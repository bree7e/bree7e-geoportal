webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>  \r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(apiService) {
        this.apiService = apiService;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.apiService.checkAuthentication();
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* APIService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.config.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppConfig; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__environments_environment__ = __webpack_require__("./src/environments/environment.ts");

var AppConfig = {
    APIPath: __WEBPACK_IMPORTED_MODULE_0__environments_environment__["a" /* environment */].APIPath,
    // Keep it actualized with the Geoportal REST API configuration
    systemTablesIdentifiers: {
        MAPS: 2,
        SYMBOLS: 3,
        ROLES: 4,
        USERS: 5,
        USER_ACTIVATION_CODES: 6,
        TABLE_ACCESS_RULES: 9,
        DATASET_TREE: 10,
        SAMPLE_THEME: 20,
        GEO_DATASET: 100,
        WPS_METHODS: 185,
        METHOD_EXAMPLES: 186,
        FILELINK: 187
    }
};


/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store_devtools__ = __webpack_require__("./node_modules/@ngrx/store-devtools/@ngrx/store-devtools.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__asymmetrik_ngx_leaflet__ = __webpack_require__("./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__asymmetrik_ngx_leaflet_draw__ = __webpack_require__("./node_modules/@asymmetrik/ngx-leaflet-draw/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ngx_pipes__ = __webpack_require__("./node_modules/ngx-pipes/ngx-pipes.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_ace_editor__ = __webpack_require__("./node_modules/ng2-ace-editor/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_routes__ = __webpack_require__("./src/app/app.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__map_map_component__ = __webpack_require__("./src/app/map/map.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__front_page_front_page_map_front_page_map_component__ = __webpack_require__("./src/app/front-page/front-page-map/front-page-map.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__authorization_header_interceptor__ = __webpack_require__("./src/app/authorization-header-interceptor.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__front_page_front_page_component__ = __webpack_require__("./src/app/front-page/front-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_manager_info_services_manager_info_component__ = __webpack_require__("./src/app/services-manager-info/services-manager-info.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__main_menu_main_menu_component__ = __webpack_require__("./src/app/main-menu/main-menu.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__datatable_datatable_component__ = __webpack_require__("./src/app/datatable/datatable.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__widgets_widget_widget_component__ = __webpack_require__("./src/app/widgets/widget/widget.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__widgets_w_edit_w_edit_component__ = __webpack_require__("./src/app/widgets/w-edit/w-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__widgets_w_password_w_password_component__ = __webpack_require__("./src/app/widgets/w-password/w-password.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__widgets_w_date_w_date_component__ = __webpack_require__("./src/app/widgets/w-date/w-date.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__widgets_w_geometry_w_geometry_component__ = __webpack_require__("./src/app/widgets/w-geometry/w-geometry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__widgets_w_point_w_point_component__ = __webpack_require__("./src/app/widgets/w-point/w-point.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__widgets_w_line_w_line_component__ = __webpack_require__("./src/app/widgets/w-line/w-line.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__widgets_w_polygon_w_polygon_component__ = __webpack_require__("./src/app/widgets/w-polygon/w-polygon.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__doc_form_table_editing_wrapper_doc_form_table_editing_wrapper_component__ = __webpack_require__("./src/app/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__widgets_w_textarea_w_textarea_component__ = __webpack_require__("./src/app/widgets/w-textarea/w-textarea.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__widgets_w_number_w_number_component__ = __webpack_require__("./src/app/widgets/w-number/w-number.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__widgets_w_boolean_w_boolean_component__ = __webpack_require__("./src/app/widgets/w-boolean/w-boolean.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__widgets_w_table_w_table_component__ = __webpack_require__("./src/app/widgets/w-table/w-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__front_page_map_service__ = __webpack_require__("./src/app/front-page/map.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__filters_filter_filter_component__ = __webpack_require__("./src/app/filters/filter/filter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__filters_f_edit_f_edit_component__ = __webpack_require__("./src/app/filters/f-edit/f-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__filters_f_number_f_number_component__ = __webpack_require__("./src/app/filters/f-number/f-number.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__filters_f_date_f_date_component__ = __webpack_require__("./src/app/filters/f-date/f-date.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__tablelist_tablelist_component__ = __webpack_require__("./src/app/tablelist/tablelist.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__modal_confirmation_modal_confirmation_component__ = __webpack_require__("./src/app/modal-confirmation/modal-confirmation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__services_and_scenarios_edit_form_services_and_scenarios_edit_form_component__ = __webpack_require__("./src/app/services-and-scenarios-edit-form/services-and-scenarios-edit-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__services_and_scenarios_run_form_services_and_scenarios_run_form_component__ = __webpack_require__("./src/app/services-and-scenarios-run-form/services-and-scenarios-run-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__services_and_scenarios_edit_form_catalog_item_parameter_catalog_item_parameter_component__ = __webpack_require__("./src/app/services-and-scenarios-edit-form/catalog-item-parameter/catalog-item-parameter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__static_page_layout_static_page_layout_component__ = __webpack_require__("./src/app/static-page-layout/static-page-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__table_dlg_table_dlg_component__ = __webpack_require__("./src/app/table-dlg/table-dlg.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__table_model_table_model_component__ = __webpack_require__("./src/app/table-model/table-model.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__widgets_w_select_w_select_component__ = __webpack_require__("./src/app/widgets/w-select/w-select.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51_ng2_completer__ = __webpack_require__("./node_modules/ng2-completer/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__widgets_w_classify_w_classify_component__ = __webpack_require__("./src/app/widgets/w-classify/w-classify.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__user_registration_user_registration_component__ = __webpack_require__("./src/app/user-registration/user-registration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__user_activation_user_activation_component__ = __webpack_require__("./src/app/user-activation/user-activation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__doc_form_doc_form_component__ = __webpack_require__("./src/app/doc-form/doc-form.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_19__main_menu_main_menu_component__["a" /* MainMenuComponent */],
                __WEBPACK_IMPORTED_MODULE_20__datatable_datatable_component__["a" /* DatatableComponent */],
                __WEBPACK_IMPORTED_MODULE_21__widgets_widget_widget_component__["a" /* WidgetComponent */],
                __WEBPACK_IMPORTED_MODULE_22__widgets_w_edit_w_edit_component__["a" /* WEditComponent */],
                __WEBPACK_IMPORTED_MODULE_23__widgets_w_password_w_password_component__["a" /* WPasswordComponent */],
                __WEBPACK_IMPORTED_MODULE_25__widgets_w_geometry_w_geometry_component__["a" /* WGeometryComponent */],
                __WEBPACK_IMPORTED_MODULE_26__widgets_w_point_w_point_component__["a" /* WPointComponent */],
                __WEBPACK_IMPORTED_MODULE_27__widgets_w_line_w_line_component__["a" /* WLineComponent */],
                __WEBPACK_IMPORTED_MODULE_28__widgets_w_polygon_w_polygon_component__["a" /* WPolygonComponent */],
                __WEBPACK_IMPORTED_MODULE_24__widgets_w_date_w_date_component__["a" /* WDateComponent */],
                __WEBPACK_IMPORTED_MODULE_29__doc_form_table_editing_wrapper_doc_form_table_editing_wrapper_component__["a" /* DocFormTableEditingWrapperComponent */],
                __WEBPACK_IMPORTED_MODULE_30__widgets_w_textarea_w_textarea_component__["a" /* WTextareaComponent */],
                __WEBPACK_IMPORTED_MODULE_31__widgets_w_number_w_number_component__["a" /* WNumberComponent */],
                __WEBPACK_IMPORTED_MODULE_32__widgets_w_boolean_w_boolean_component__["a" /* WBooleanComponent */],
                __WEBPACK_IMPORTED_MODULE_38__filters_filter_filter_component__["a" /* WFilterComponent */],
                __WEBPACK_IMPORTED_MODULE_39__filters_f_edit_f_edit_component__["a" /* FEditComponent */],
                __WEBPACK_IMPORTED_MODULE_40__filters_f_number_f_number_component__["a" /* FNumberComponent */],
                __WEBPACK_IMPORTED_MODULE_41__filters_f_date_f_date_component__["a" /* FDateComponent */],
                __WEBPACK_IMPORTED_MODULE_19__main_menu_main_menu_component__["a" /* MainMenuComponent */],
                __WEBPACK_IMPORTED_MODULE_17__front_page_front_page_component__["a" /* FrontPageComponent */],
                __WEBPACK_IMPORTED_MODULE_44__services_and_scenarios_edit_form_services_and_scenarios_edit_form_component__["a" /* ServicesAndScenariosEditFormComponent */],
                __WEBPACK_IMPORTED_MODULE_45__services_and_scenarios_run_form_services_and_scenarios_run_form_component__["a" /* ServicesAndScenariosRunFormComponent */],
                __WEBPACK_IMPORTED_MODULE_46__services_and_scenarios_edit_form_catalog_item_parameter_catalog_item_parameter_component__["a" /* CatalogItemParameterComponent */],
                __WEBPACK_IMPORTED_MODULE_47__static_page_layout_static_page_layout_component__["a" /* StaticPageLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_18__services_manager_info_services_manager_info_component__["a" /* ServicesManagerInfoComponent */],
                __WEBPACK_IMPORTED_MODULE_14__map_map_component__["a" /* MapComponent */],
                __WEBPACK_IMPORTED_MODULE_15__front_page_front_page_map_front_page_map_component__["a" /* FrontPageMapComponent */],
                __WEBPACK_IMPORTED_MODULE_42__tablelist_tablelist_component__["a" /* TablelistComponent */],
                __WEBPACK_IMPORTED_MODULE_43__modal_confirmation_modal_confirmation_component__["a" /* ModalConfirmationComponent */],
                __WEBPACK_IMPORTED_MODULE_33__widgets_w_table_w_table_component__["a" /* WTableComponent */],
                __WEBPACK_IMPORTED_MODULE_48__table_dlg_table_dlg_component__["a" /* TableDlgComponent */],
                __WEBPACK_IMPORTED_MODULE_49__table_model_table_model_component__["a" /* TableModelComponent */],
                __WEBPACK_IMPORTED_MODULE_50__widgets_w_select_w_select_component__["a" /* WSelectComponent */],
                __WEBPACK_IMPORTED_MODULE_52__widgets_w_classify_w_classify_component__["a" /* WClassifyComponent */],
                __WEBPACK_IMPORTED_MODULE_53__user_registration_user_registration_component__["a" /* UserRegistrationComponent */],
                __WEBPACK_IMPORTED_MODULE_54__user_activation_user_activation_component__["a" /* UserActivationComponent */],
                __WEBPACK_IMPORTED_MODULE_55__doc_form_doc_form_component__["a" /* DocFormComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_11_ng2_ace_editor__["a" /* AceEditorModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_router__["b" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__app_routes__["a" /* routes */], { enableTracing: false }),
                __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["i" /* StoreModule */].forRoot({ state: __WEBPACK_IMPORTED_MODULE_36__store_app_reducer__["a" /* appReducer */] }),
                __WEBPACK_IMPORTED_MODULE_5__ngrx_store_devtools__["a" /* StoreDevtoolsModule */].instrument({ maxAge: 50 }),
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_51_ng2_completer__["a" /* Ng2CompleterModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_8__asymmetrik_ngx_leaflet__["c" /* LeafletModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_9__asymmetrik_ngx_leaflet_draw__["a" /* LeafletDrawModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_10_ngx_pipes__["b" /* NgObjectPipesModule */],
                __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["c" /* NgbModule */].forRoot()
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_35__store_app_actions__["a" /* AppActions */],
                __WEBPACK_IMPORTED_MODULE_37__front_page_map_service__["a" /* MapService */],
                __WEBPACK_IMPORTED_MODULE_34__shared_api_service__["a" /* APIService */],
                {
                    provide: __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_16__authorization_header_interceptor__["a" /* AuthorizationHeaderInterceptor */],
                    multi: true
                }
            ],
            /**
             * @see https://ng-bootstrap.github.io/#/components/modal/examples
             * @see https://angular.io/guide/ngmodule-faq
             */
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_15__front_page_front_page_map_front_page_map_component__["a" /* FrontPageMapComponent */],
                __WEBPACK_IMPORTED_MODULE_29__doc_form_table_editing_wrapper_doc_form_table_editing_wrapper_component__["a" /* DocFormTableEditingWrapperComponent */],
                __WEBPACK_IMPORTED_MODULE_48__table_dlg_table_dlg_component__["a" /* TableDlgComponent */],
                __WEBPACK_IMPORTED_MODULE_49__table_model_table_model_component__["a" /* TableModelComponent */],
                __WEBPACK_IMPORTED_MODULE_43__modal_confirmation_modal_confirmation_component__["a" /* ModalConfirmationComponent */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__front_page_front_page_component__ = __webpack_require__("./src/app/front-page/front-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_registration_user_registration_component__ = __webpack_require__("./src/app/user-registration/user-registration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_activation_user_activation_component__ = __webpack_require__("./src/app/user-activation/user-activation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_and_scenarios_edit_form_services_and_scenarios_edit_form_component__ = __webpack_require__("./src/app/services-and-scenarios-edit-form/services-and-scenarios-edit-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_and_scenarios_run_form_services_and_scenarios_run_form_component__ = __webpack_require__("./src/app/services-and-scenarios-run-form/services-and-scenarios-run-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_manager_info_services_manager_info_component__ = __webpack_require__("./src/app/services-manager-info/services-manager-info.component.ts");






var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_0__front_page_front_page_component__["a" /* FrontPageComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_1__user_registration_user_registration_component__["a" /* UserRegistrationComponent */] },
    { path: 'activate-account/:code', component: __WEBPACK_IMPORTED_MODULE_2__user_activation_user_activation_component__["a" /* UserActivationComponent */] },
    { path: 'services-manager', component: __WEBPACK_IMPORTED_MODULE_5__services_manager_info_services_manager_info_component__["a" /* ServicesManagerInfoComponent */] },
    { path: 'services-manager/create', component: __WEBPACK_IMPORTED_MODULE_3__services_and_scenarios_edit_form_services_and_scenarios_edit_form_component__["a" /* ServicesAndScenariosEditFormComponent */] },
    { path: 'services-manager/edit/:id', component: __WEBPACK_IMPORTED_MODULE_3__services_and_scenarios_edit_form_services_and_scenarios_edit_form_component__["a" /* ServicesAndScenariosEditFormComponent */] },
    { path: 'services-manager/run/:id', component: __WEBPACK_IMPORTED_MODULE_4__services_and_scenarios_run_form_services_and_scenarios_run_form_component__["a" /* ServicesAndScenariosRunFormComponent */] },
];


/***/ }),

/***/ "./src/app/authorization-header-interceptor.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthorizationHeaderInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthorizationHeaderInterceptor = /** @class */ (function () {
    function AuthorizationHeaderInterceptor(store) {
        var _this = this;
        this.store = store;
        this.store.select(Object(__WEBPACK_IMPORTED_MODULE_2__store_app_reducer__["b" /* stateGetter */])('user')).subscribe(function (data) {
            _this._user = data;
        });
    }
    AuthorizationHeaderInterceptor.prototype.intercept = function (req, next) {
        if (this._user) {
            var clonedRequest = req.clone({
                headers: req.headers.set('Authorization', "Bearer " + this._user.tokens.accessToken)
            });
            return next.handle(clonedRequest);
        }
        else {
            return next.handle(req);
        }
    };
    AuthorizationHeaderInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], AuthorizationHeaderInterceptor);
    return AuthorizationHeaderInterceptor;
}());



/***/ }),

/***/ "./src/app/createAction.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = createAction;
function createAction(type, payload) {
    return { type: type, payload: payload };
}


/***/ }),

/***/ "./src/app/datatable/datatable.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"toolbars btn-toolbar justify-content-between\" role=\"toolbar\" aria-label=\"Toolbar with button groups\">\r\n  <div class=\"toolbars__main-toolbar btn-group\" *ngIf=\"mode!='readonly'\">\r\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addRow()\">\r\n      <i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-secondary\">\r\n      <i class=\"fa fa-clone\" aria-hidden=\"true\"></i>\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-secondary\">\r\n      <i class=\"fa fa-minus-square \" aria-hidden=\"true\"></i>\r\n    </button>\r\n  </div>\r\n  <div class=\"toolbars__secondary-toolbar btn-group\">\r\n    <button type=\"button\" class=\"btn\" (click)=\"toggleAggregationBarVisible()\">\r\n      <i class=\"fa fa-code-fork\" aria-hidden=\"true\"></i>\r\n      Aggregation\r\n    </button>\r\n    <button type=\"button\" class=\"btn\" (click)=\"toggleFilteringBarVisible()\">\r\n      <i class=\"fa fa-filter\" aria-hidden=\"true\"></i>\r\n      Filtering\r\n    </button>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"table-responsive\">\r\n  <table class=\"geoportal__table table table-striped table-sm table-hover\" *ngIf=\"showTable()\">\r\n    <thead class=\"thead-light\">\r\n      <ng-container *ngFor=\"let column of table.meta.columns\">\r\n        <th class=\"geoportal-table__header\" *ngIf=\"column.visible\">\r\n          {{ column.title }}\r\n          <i (click)=\"changeOrder(column)\" class=\"geoportal-table__header-label fa\" [ngClass]=\"column.sortingOrder.className\" aria-hidden=\"true\"></i>\r\n        </th>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"mode != 'readonly'\">\r\n        <th></th>\r\n        <th></th>\r\n      </ng-container>\r\n    </thead>\r\n\r\n    <thead class=\"thead-light\" *ngIf=\"showFiltering()\">\r\n      <ng-container *ngFor=\"let column of table.meta.columns\">\r\n        <th class=\"align-top\" *ngIf=\"column.visible\">\r\n          <w-filter [column]=\"column\" (filterChange)=\"onChangeFilter($event)\">\r\n          </w-filter>\r\n        </th>\r\n      </ng-container>\r\n      <th></th>\r\n      <th></th>\r\n    </thead>\r\n\r\n    <thead class=\"thead-light\" *ngIf=\"showAggregation()\">\r\n      <ng-container *ngFor=\"let column of table.meta.columns\">\r\n        <th *ngIf=\"column.visible\">f</th>\r\n      </ng-container>\r\n    </thead>\r\n    <tbody *ngIf=\"table.data !== undefined\">\r\n      <tr *ngFor=\"let row of table.data.aaData\" (click)=\"onRowClicked(row)\" [class.table-primary]=\"row === currentRow\">\r\n        <ng-container *ngFor=\"let column of table.meta.columns\">\r\n          <td *ngIf=\"column.visible\">\r\n            <widget [attr]=\"column\" [widgetValue]=\"row[column.fieldname]\" [mode]=\"'view'\">\r\n            </widget>\r\n          </td>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"mode!='readonly'\">\r\n          <td>\r\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"editRow(row)\">\r\n              <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\r\n            </button>\r\n          </td>\r\n\r\n          <td>\r\n            <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteRow(row)\">\r\n              <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\r\n            </button>\r\n          </td>\r\n        </ng-container>\r\n      </tr>\r\n    </tbody>\r\n    <tbody *ngIf=\"table.data === undefined\">\r\n      Table data is undefined.\r\n    </tbody>\r\n  </table>\r\n</div>\r\n\r\n<div class=\"table-pagination\">\r\n  <div class=\"table-pagination__limit\">\r\n    <ul class=\"pagination\">\r\n      <li class=\"page-item\" [class.active]=\"table.pageSize==5\" (click)=\"changePageSize(5)\">\r\n        <a class=\"page-link\" href=\"#\">5</a>\r\n      </li>\r\n      <li class=\"page-item\" [class.active]=\"table.pageSize==10\" (click)=\"changePageSize(10)\">\r\n        <a class=\"page-link\" href=\"#\">10</a>\r\n      </li>\r\n      <li class=\"page-item\" [class.active]=\"table.pageSize==25\" (click)=\"changePageSize(25)\">\r\n        <a class=\"page-link\" href=\"#\">25</a>\r\n      </li>\r\n      <li class=\"page-item\" [class.active]=\"table.pageSize==50\" (click)=\"changePageSize(50)\">\r\n        <a class=\"page-link\" href=\"#\">50</a>\r\n      </li>\r\n      <li class=\"page-item\" [class.active]=\"table.pageSize==100\" (click)=\"changePageSize(100)\">\r\n        <a class=\"page-link\" href=\"#\">100</a>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <div class=\"table-pagination__pages\">\r\n    <ngb-pagination [collectionSize]=\"table.collectionSize\" [page]=\"table.page\" [pageSize]=\"table.pageSize\" (pageChange)=\"onPageChanged($event)\">\r\n    </ngb-pagination>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/datatable/datatable.component.scss":
/***/ (function(module, exports) {

module.exports = ".toolbars {\n  padding: 0.75rem 0; }\n\n.table-pagination {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n\n.table-pagination__pages {\n    margin-left: 2rem; }\n\n.geoportal-table__header-label {\n  cursor: pointer; }\n\n.geoportal-table__header::first-letter {\n  text-transform: uppercase; }\n"

/***/ }),

/***/ "./src/app/datatable/datatable.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatatableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_shared_interfaces__ = __webpack_require__("./src/app/shared/interfaces.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_doc_form_table_editing_wrapper_doc_form_table_editing_wrapper_component__ = __webpack_require__("./src/app/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_app_store_app_state__ = __webpack_require__("./src/app/store/app.state.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_app_store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_app_modal_confirmation_modal_confirmation_component__ = __webpack_require__("./src/app/modal-confirmation/modal-confirmation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_app_store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/last';







var DatatableComponent = /** @class */ (function () {
    function DatatableComponent(modalService, datatableService, store, appActions) {
        this.modalService = modalService;
        this.datatableService = datatableService;
        this.store = store;
        this.appActions = appActions;
        this.mode = '';
        this.context = __WEBPACK_IMPORTED_MODULE_7_app_store_app_state__["a" /* TableContext */].ServiceTables;
        this.curenttable = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        // selectedRowKeys = []; лучше сделать массив, и в нем хранить ключи. подсмотрено у devexpress datagrid
        this.currentRow = { id: -1 };
    }
    DatatableComponent.prototype.initTableState = function () {
        this.table = {
            dataset_id: this.datasetId,
            showFilteringBar: false,
            showAggregationBar: false,
            context: this.context
        };
    };
    DatatableComponent.prototype.openModalForm = function () {
        var modalRef = this.modalService.open(__WEBPACK_IMPORTED_MODULE_6_app_doc_form_table_editing_wrapper_doc_form_table_editing_wrapper_component__["a" /* DocFormTableEditingWrapperComponent */], {
            size: 'lg',
            backdrop: 'static'
        });
        modalRef.componentInstance.meta = this.table.meta;
        modalRef.componentInstance.doc = this.currentRow;
    };
    /**
     * Еще внутрь API имеет смысл добавить ДЕЙСТВИЕ isFetching для показа спиннера загрузки
     */
    DatatableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initTableState();
        if (this.table.meta === undefined) {
            this.getMetadataFromAPIandInitSettings(this.table).then(function (initializedTable) {
                _this.appActions.setMetadataForTable(initializedTable.dataset_id, initializedTable.meta);
                _this.subscription = _this.store
                    .select(Object(__WEBPACK_IMPORTED_MODULE_10_app_store_app_reducer__["b" /* stateGetter */])('tables', _this.table.dataset_id))
                    .subscribe(function (choosedTable) {
                    _this.table = __assign({}, _this.table, Object(__WEBPACK_IMPORTED_MODULE_3_lodash__["cloneDeep"])(choosedTable));
                    _this.getDataFromAPI(_this.table).then(function (tableWithData) {
                        _this.table = tableWithData;
                        console.log('Data recieved for table =', _this.table.dataset_id);
                    });
                });
                console.log('Subsribed. Dataset ID = ', _this.table.dataset_id);
            });
        }
    };
    DatatableComponent.prototype.ngOnDestroy = function () {
        console.log('Unsubsribed. Dataset ID = ', this.table.dataset_id);
        if (this.subscription)
            this.subscription.unsubscribe();
    };
    DatatableComponent.prototype.onChangeFilter = function (filter) {
        console.log("NEW FILTER", filter);
        this.appActions.setTableFilter(this.table.dataset_id, filter);
    };
    DatatableComponent.prototype.onPageChanged = function (page) {
        if (isNaN(page) || page === 0) {
            return;
        }
        this.appActions.setPaginatorPageForTable(this.table.dataset_id, page);
    };
    DatatableComponent.prototype.changePageSize = function (pageSize) {
        this.appActions.setPaginatorPageSizeForTable(this.table.dataset_id, pageSize);
    };
    DatatableComponent.prototype.changeOrder = function (column) {
        var sortingColumn = {
            fieldname: column.fieldname,
            sorting: {
                direction: '',
                className: 'fa-sort'
            }
        };
        if (column.sortingOrder.direction === '') {
            sortingColumn.sorting.direction = 'ASC';
            sortingColumn.sorting.className = 'fa-sort-asc';
        }
        else if (column.sortingOrder.direction === 'ASC') {
            sortingColumn.sorting.direction = 'DESC';
            sortingColumn.sorting.className = 'fa-sort-desc';
        }
        else if (column.sortingOrder.direction === 'DESC') {
            // сбросить состояние сортировки
        }
        this.appActions.setSortingForTableColumn(this.datasetId, sortingColumn);
    };
    DatatableComponent.prototype.editRow = function (row) {
        this.currentRow = row;
        this.openModalForm();
    };
    DatatableComponent.prototype.addRow = function () {
        this.currentRow = { id: -1 };
        this.openModalForm();
    };
    DatatableComponent.prototype.onRowClicked = function (row) {
        this.currentRow = row;
        this.curenttable.emit(this.currentRow);
    };
    DatatableComponent.prototype.openConfirmationModalForm = function (title, body) {
        if (title === void 0) { title = 'Geoportal'; }
        var modalRef = this.modalService.open(__WEBPACK_IMPORTED_MODULE_9_app_modal_confirmation_modal_confirmation_component__["a" /* ModalConfirmationComponent */]);
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.body = body;
        return modalRef.result;
    };
    DatatableComponent.prototype.deleteRow = function (row) {
        var _this = this;
        // row: table.data.aaData
        this.currentRow = row;
        this.openConfirmationModalForm('Deleting row', 'Are you sure you want to delete this row?').then(function (result) {
            if (result === __WEBPACK_IMPORTED_MODULE_4_app_shared_interfaces__["b" /* ModalResult */].mrYes) {
                // const doc = { id: this.currentRow.id };
                // this.datatableService.delete(this.table.meta.dataset_id, doc).then(res => {
                //   console.log(res);
                //   this.appActions.updateTable(this.table.meta.dataset_id);
                // });
                console.log("\u0412\u043D\u0443\u0442\u0440\u0438 datatable \u043F\u043E\u043B\u0443\u0447\u0438\u043B\u0438 \u0414\u0430 ,\u0443\u0434\u0430\u043B\u0438\u0442\u044C " + _this.currentRow['id']);
            }
        });
    };
    DatatableComponent.prototype.showTable = function () {
        if (this.table.meta !== undefined) {
            return true;
        }
        else {
            return false;
        }
    };
    DatatableComponent.prototype.showFiltering = function () {
        if (this.table.showFilteringBar && this.table.meta !== undefined) {
            return true;
        }
        else {
            return false;
        }
    };
    DatatableComponent.prototype.showAggregation = function () {
        if (this.table.showAggregationBar && this.table.meta !== undefined) {
            return true;
        }
        else {
            return false;
        }
    };
    DatatableComponent.prototype.toggleFilteringBarVisible = function () {
        if (this.table.showFilteringBar) {
            this.appActions.setFilteringBarVisibilty(this.table.dataset_id, false);
        }
        else {
            this.appActions.setFilteringBarVisibilty(this.table.dataset_id, true);
        }
    };
    DatatableComponent.prototype.toggleAggregationBarVisible = function () {
        if (this.table.showAggregationBar) {
            this.appActions.setAggregationBarVisibilty(this.table.dataset_id, false);
        }
        else {
            this.appActions.setAggregationBarVisibilty(this.table.dataset_id, true);
        }
    };
    DatatableComponent.prototype.getDataFromAPI = function (dataSet) {
        var _this = this;
        var filtersToAPI = [];
        this.table.meta.columns.forEach(function (column) {
            var filter = {
                fieldname: column.fieldname,
                valueList: column.filterValueList
            };
            if (filter.valueList.length > 0) {
                filtersToAPI.push(filter);
            }
        });
        var sortingToAPI = [];
        this.table.meta.columns.forEach(function (column) {
            var sortingColumn = {
                fieldname: column.fieldname,
                sorting: column.sortingOrder
            };
            if (sortingColumn.sorting.direction !== '') {
                sortingToAPI.push(sortingColumn);
            }
        });
        var promise = new Promise(function (resolve, reject) {
            _this.datatableService
                .list(dataSet.dataset_id, dataSet.pageSize, dataSet.pageSize * (dataSet.page - 1), // следует ставить = 0
            filtersToAPI, sortingToAPI)
                .then(function (rows) {
                dataSet.data = rows;
                dataSet.collectionSize = rows['iTotalRecords'];
                resolve(dataSet);
            });
        });
        return promise;
    };
    DatatableComponent.prototype.getMetadataFromAPIandInitSettings = function (dataSet) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.datatableService
                .meta(dataSet.dataset_id)
                .then(function (serviceMetadata) {
                dataSet.meta = serviceMetadata;
                dataSet.meta.columns.forEach(function (column) {
                    column.sortingOrder = {
                        className: 'fa-sort',
                        direction: ''
                    };
                    column.filterValueList = [];
                });
                dataSet.page = 1; // pagination from first page
                dataSet.pageSize = 5; // pagination Size for first page
                dataSet.collectionSize = 0;
                dataSet.showFilteringBar = false;
                dataSet.showAggregationBar = false;
                resolve(dataSet);
            });
        });
        return promise;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], DatatableComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Number)
    ], DatatableComponent.prototype, "datasetId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Number)
    ], DatatableComponent.prototype, "context", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], DatatableComponent.prototype, "curenttable", void 0);
    DatatableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'datatable',
            template: __webpack_require__("./src/app/datatable/datatable.component.html"),
            styles: [__webpack_require__("./src/app/datatable/datatable.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */],
            __WEBPACK_IMPORTED_MODULE_5_app_shared_api_service__["a" /* APIService */],
            __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_8_app_store_app_actions__["a" /* AppActions */]])
    ], DatatableComponent);
    return DatatableComponent;
}());



/***/ }),

/***/ "./src/app/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component.html":
/***/ (function(module, exports) {

module.exports = "  <div class=\"modal-header\">\r\n    <h4 class=\"modal-title\">Record editing </h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"docCancel()\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"modal-body\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <!-- Старый компонент begin -->\r\n          <form>  \r\n            <ng-container *ngFor=\"let attr of meta.columns\">\r\n              <div class=\"form-group\" *ngIf=\"attr.visible\">\r\n                <label for=\"{{attr.fieldname}}\">{{attr.title}}</label>\r\n                <widget [attr]=\"attr\" [(widgetValue)]=\"this.doc[attr.fieldname]\" [mode]=\"'forminput'\"></widget>\r\n              </div>\r\n            </ng-container>\r\n            <!-- Старый компонент end -->  \r\n          </form>\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <span #mapPlacement></span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"docCancel()\">Close</button>\r\n    <button type=\"button\" class=\"btn btn-primary\" (click)=\"docSubmit()\">Save changes</button>    \r\n     <!-- оригинальная -->\r\n    <!-- <button type=\"submit\" (click)=\"docSubmit()\" class=\"btn btn-success\">Submit</button>  -->\r\n  </div>"

/***/ }),

/***/ "./src/app/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Document */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocFormTableEditingWrapperComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__widgets_widget_widget_component__ = __webpack_require__("./src/app/widgets/widget/widget.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Document = /** @class */ (function () {
    function Document(id) {
        this.id = id;
    }
    return Document;
}());

/*
@todo Use the doc-form component and clean the component up
*/
var DocFormTableEditingWrapperComponent = /** @class */ (function () {
    function DocFormTableEditingWrapperComponent(activeModal, store, DocService, appActions) {
        this.activeModal = activeModal;
        this.store = store;
        this.DocService = DocService;
        this.appActions = appActions;
        this.model = new Document(-1);
        this.history = [];
        this.formData = {};
        // TODO Should add interface
        this.meta = {};
        this.doc = {};
    }
    DocFormTableEditingWrapperComponent.prototype.docSubmit = function () {
        var _this = this;
        this.childrenWidgets.forEach(function (item) {
            console.log(item.attr.fieldname, item.getValue());
            _this.doc[item.attr.fieldname] = item.getValue();
        });
        console.log('ok');
        if ('id' in this.doc && this.doc['id'] != -1) {
            this.DocService.update(this.meta.dataset_id, this.doc).then(function (res) {
                console.log(res);
                _this.appActions.updateTableData(_this.meta.dataset_id);
            });
        }
        else {
            this.DocService.add(this.meta.dataset_id, this.doc).then(function (res) {
                console.log(res);
                _this.appActions.updateTableData(_this.meta.dataset_id);
            });
        }
        this.docCancel();
    };
    DocFormTableEditingWrapperComponent.prototype.docCancel = function () {
        this.mapPlacementContainer.detach(this.mapPlacementContainer.indexOf(this.mapRef.hostView));
        this.appActions.setFrontPageMapHost({ host: 'regular' });
        this.activeModal.close('Close click');
    };
    DocFormTableEditingWrapperComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appActions.setFrontPageMapHost({ host: 'embedded' });
        this.store.select(Object(__WEBPACK_IMPORTED_MODULE_6__store_app_reducer__["b" /* stateGetter */])('frontPageMap', 'ref')).take(1).subscribe(function (data) {
            _this.mapRef = data;
            _this.mapPlacementContainer.insert(data.hostView);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('mapPlacement', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewContainerRef */] }),
        __metadata("design:type", Object)
    ], DocFormTableEditingWrapperComponent.prototype, "mapPlacementContainer", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], DocFormTableEditingWrapperComponent.prototype, "meta", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], DocFormTableEditingWrapperComponent.prototype, "doc", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChildren */])(__WEBPACK_IMPORTED_MODULE_2__widgets_widget_widget_component__["a" /* WidgetComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* QueryList */])
    ], DocFormTableEditingWrapperComponent.prototype, "childrenWidgets", void 0);
    DocFormTableEditingWrapperComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'doc-form',
            template: __webpack_require__("./src/app/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component.html"),
            styles: [__webpack_require__("./src/app/doc-form-table-editing-wrapper/doc-form-table-editing-wrapper.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__shared_api_service__["a" /* APIService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbActiveModal */], __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_3__shared_api_service__["a" /* APIService */], __WEBPACK_IMPORTED_MODULE_4__store_app_actions__["a" /* AppActions */]])
    ], DocFormTableEditingWrapperComponent);
    return DocFormTableEditingWrapperComponent;
}());



/***/ }),

/***/ "./src/app/doc-form/doc-form.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"loading-overlay-container\" *ngIf=\"loading\"></div>\r\n  <div class=\"row\" *ngIf=\"serverError !== false\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"alert alert-danger\" role=\"alert\">{{ serverError }}</div>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <h1>Geoportal registration</h1>\r\n      <p>Please fill all fields and submit the form. You will receive the <strong>email</strong> with the activation link.</p>\r\n      <hr/>\r\n      <form>\r\n        <ng-container *ngFor=\"let attr of meta.columns\">\r\n          <div class=\"form-group\" *ngIf=\"attr.visible\">\r\n            <label for=\"{{attr.fieldname}}\">{{attr.title}}</label>\r\n            <widget [attr]=\"attr\" [(widgetValue)]=\"this.doc[attr.fieldname]\" [mode]=\"'forminput'\"></widget>\r\n          </div>\r\n        </ng-container>\r\n      </form>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-6\" >\r\n      <button  type=\"button\" class=\"btn btn-primary\" (click)=\"docSubmit($event)\">\r\n        <i class=\"fa fa-save\" ></i> <span *ngIf=\"mode === modes.ADD\">Save</span><span *ngIf=\"mode === modes.UPDATE\">Update</span>\r\n      </button>\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <button *ngIf=\"mode === modes.UPDATE\" type=\"button\" class=\"btn\"><i class=\"fa fa-remove\" aria-hidden=\"true\"></i> Clear form</button>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\" *ngIf=\"formDataIsComplete === false\">\r\n    <div class=\"col-md-12\">\r\n      <span class=\"error-message\">Form data is incomplete</span>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/doc-form/doc-form.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/doc-form/doc-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__widgets_widget_widget_component__ = __webpack_require__("./src/app/widgets/widget/widget.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DocFormComponent = /** @class */ (function () {
    function DocFormComponent(store, apiService, appActions) {
        this.store = store;
        this.apiService = apiService;
        this.appActions = appActions;
        this.modes = {
            ADD: 1,
            UPDATE: 2
        };
        this.meta = {};
        this.doc = {};
        this.onSuccessfullSubmition = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.loading = false;
        this.formDataIsComplete = true;
        this.serverError = false;
        if ('id' in this.doc && this.doc['id'] != -1) {
            this.mode = this.modes.UPDATE;
        }
        else {
            this.mode = this.modes.ADD;
        }
    }
    DocFormComponent.prototype.errorHandler = function (errorResponse) {
        this.loading = false;
        var error = errorResponse.error;
        if (error && error.errorId) {
            if (error.errorId === 1002) {
                this.serverError = "Specified " + error.description.field + " already exists";
            }
            else {
                this.serverError = "Error occured: " + error.message;
            }
        }
        else {
            throw new Error('Error occured at DocForm update');
        }
    };
    DocFormComponent.prototype.docSubmit = function (event) {
        var _this = this;
        this.loading = true;
        this.formDataIsComplete = true;
        this.serverError = false;
        this.childrenWidgets.forEach(function (item) {
            var widgetValue = item.getValue();
            if (item.attr && item.attr.required && item.attr.required === false) {
                _this.doc[item.attr.fieldname] = widgetValue;
            }
            else {
                if (widgetValue) {
                    _this.doc[item.attr.fieldname] = widgetValue;
                }
                else {
                    _this.formDataIsComplete = false;
                }
            }
        });
        if (this.formDataIsComplete) {
            if ('id' in this.doc && this.doc['id'] != -1) {
                this.apiService.update(this.meta.dataset_id, this.doc).then(function (res) {
                    _this.loading = false;
                    _this.onSuccessfullSubmition.emit(res);
                }).catch(function (errorResponse) {
                    _this.errorHandler(errorResponse);
                });
            }
            else {
                this.apiService.add(this.meta.dataset_id, this.doc).then(function (res) {
                    _this.loading = false;
                    _this.onSuccessfullSubmition.emit(res);
                }).catch(function (errorResponse) {
                    _this.errorHandler(errorResponse);
                });
            }
        }
        else {
            this.loading = false;
        }
    };
    DocFormComponent.prototype.ngOnInit = function () { };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], DocFormComponent.prototype, "meta", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], DocFormComponent.prototype, "doc", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], DocFormComponent.prototype, "onSuccessfullSubmition", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChildren */])(__WEBPACK_IMPORTED_MODULE_1__widgets_widget_widget_component__["a" /* WidgetComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* QueryList */])
    ], DocFormComponent.prototype, "childrenWidgets", void 0);
    DocFormComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-doc-form',
            template: __webpack_require__("./src/app/doc-form/doc-form.component.html"),
            styles: [__webpack_require__("./src/app/doc-form/doc-form.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_2__shared_api_service__["a" /* APIService */], __WEBPACK_IMPORTED_MODULE_3__store_app_actions__["a" /* AppActions */]])
    ], DocFormComponent);
    return DocFormComponent;
}());



/***/ }),

/***/ "./src/app/doc-form/doc_service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export fieldval */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var fieldval = /** @class */ (function () {
    function fieldval(fieldname, value) {
        this.fieldname = fieldname;
        this.value = value;
    }
    return fieldval;
}());

var DocService = /** @class */ (function () {
    function DocService() {
        // Observable string sources
        this.widgets = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["a" /* Subject */]();
        this.form = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["a" /* Subject */]();
        // Observable string streams
        this.widgets$ = this.widgets.asObservable();
        this.formSource$ = this.form.asObservable();
    }
    // Service message commands
    DocService.prototype.confirmWidget = function (val) {
        this.widgets.next(val);
    };
    DocService.prototype.confirmForm = function (value) {
        // alert(value.fieldname+'form catch');
        this.form.next(value);
    };
    DocService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])()
    ], DocService);
    return DocService;
}());



/***/ }),

/***/ "./src/app/filters/f-date/f-date.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/filters/f-date/f-date.component.html":
/***/ (function(module, exports) {

module.exports = "<w-date [attr]=\"attr\" [widgetValue]=\"curentval1\"  [mode]=\"mode\" (widgetValueChange)=\"changeval1($event)\"></w-date>-\r\n<w-date [attr]=\"attr\" [widgetValue]=\"curentval2\"  [mode]=\"mode\" (widgetValueChange)=\"changeval2($event)\"></w-date>\r\n<button type=\"button\" class=\"btn\" (click)=\"addcondition()\">+</button>\r\n<div>\r\n  <ng-container *ngFor=\"let val of widgetValue\">\r\n      <div (click)=\"deleteitem(val)\">\r\n        {{val.v1  | date:'dd.MM.yyyy'}} - {{val.v2  | date:'dd.MM.yyyy'}}\r\n      </div>\r\n  </ng-container>\r\n</div>"

/***/ }),

/***/ "./src/app/filters/f-date/f-date.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FDateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FDateComponent = /** @class */ (function () {
    function FDateComponent() {
        this.attr = {};
        this.widgetValue = [];
        this.mode = {};
        this.curentval1 = '';
        this.curentval2 = '';
        this.valueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    FDateComponent.prototype.ngOnInit = function () {
    };
    FDateComponent.prototype.changeval1 = function (mas) {
        this.curentval1 = mas[1];
    };
    FDateComponent.prototype.changeval2 = function (mas) {
        this.curentval2 = mas[1];
    };
    FDateComponent.prototype.addcondition = function () {
        console.log(this.widgetValue);
        if (!this.widgetValue) {
            this.widgetValue = [];
        }
        var item2 = { v1: this.curentval1, v2: this.curentval2 };
        this.widgetValue.push(item2);
        //mas[2].widgetValue = '';
    };
    FDateComponent.prototype.deleteitem = function (val) {
        if (!this.widgetValue) {
            return;
        }
        var index = this.widgetValue.indexOf(val, 0);
        if (index > -1) {
            this.widgetValue.splice(index, 1);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FDateComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Array)
    ], FDateComponent.prototype, "widgetValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FDateComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], FDateComponent.prototype, "valueChange", void 0);
    FDateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'f-date',
            template: __webpack_require__("./src/app/filters/f-date/f-date.component.html"),
            styles: [__webpack_require__("./src/app/filters/f-date/f-date.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FDateComponent);
    return FDateComponent;
}());



/***/ }),

/***/ "./src/app/filters/f-edit/f-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<w-edit \r\n  [column]=\"column\" \r\n  [mode]=\"mode\" \r\n  [widgetValue]=\"currentVal\"  \r\n  (widgetValueChange)=\"addItem($event)\">\r\n</w-edit>\r\n<div class=\"f-edit__item-list\">\r\n  <ng-container *ngFor=\"let value of column.filterValueList\">\r\n      <div class=\"f-edit__item\" (click)=\"removeItem(value)\">\r\n        {{value}}        \r\n      </div>\r\n  </ng-container>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/filters/f-edit/f-edit.component.scss":
/***/ (function(module, exports) {

module.exports = ".f-edit__item:hover {\n  text-decoration: line-through;\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/filters/f-edit/f-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_widgets_w_edit_w_edit_component__ = __webpack_require__("./src/app/widgets/w-edit/w-edit.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FEditComponent = /** @class */ (function () {
    function FEditComponent() {
        this.mode = {};
        this.widgetValue = { fieldname: '', valueList: [] };
        // @see https://angular.io/guide/styleguide#dont-prefix-output-properties
        this.valueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */](); // объект генерит события
        this.currentVal = '';
    }
    FEditComponent.prototype.notifyParent = function () {
        /*
        const filter: MetadataFilter = {
          fieldname: this.attr.fieldname,
          fieldlist: this.widgetValue
        };*/
        this.widgetValue.fieldname = this.column.fieldname;
        this.widgetValue.valueList = this.column.filterValueList;
        this.valueChange.emit(this.widgetValue);
    };
    FEditComponent.prototype.addItem = function (item) {
        if (this.column.filterValueList.indexOf(item) === -1) {
            this.column.filterValueList.push(item);
        }
        this.widget.widgetValue = '';
        this.notifyParent();
    };
    FEditComponent.prototype.removeItem = function (item) {
        var index = this.column.filterValueList.indexOf(item, 0);
        if (index > -1) {
            this.column.filterValueList.splice(index, 1);
        }
        this.notifyParent();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FEditComponent.prototype, "column", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FEditComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FEditComponent.prototype, "widgetValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", Object)
    ], FEditComponent.prototype, "valueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_app_widgets_w_edit_w_edit_component__["a" /* WEditComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_app_widgets_w_edit_w_edit_component__["a" /* WEditComponent */])
    ], FEditComponent.prototype, "widget", void 0);
    FEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'f-edit',
            template: __webpack_require__("./src/app/filters/f-edit/f-edit.component.html"),
            styles: [__webpack_require__("./src/app/filters/f-edit/f-edit.component.scss")]
        })
    ], FEditComponent);
    return FEditComponent;
}());



/***/ }),

/***/ "./src/app/filters/f-number/f-number.component.html":
/***/ (function(module, exports) {

module.exports = "<select class=\"f-number__logic-element form-control col-2\" *ngIf=\"showLogicElement()\" #logicElement>\r\n  <option value=\"{{logicElements.Or}}\">OR</option>\r\n  <option value=\"{{logicElements.And}}\">AND</option>\r\n</select>\r\n\r\n<select class=\"f-number__option form-control col-2\" #signOption>\r\n  <option value=\"{{signOptions.Equals}}\">=</option>\r\n  <option value=\"{{signOptions.NotEqualTo}}\">≠</option>\r\n  <option value=\"{{signOptions.GreaterThan}}\">&gt;</option>\r\n  <option value=\"{{signOptions.LessThan}}\">&lt;</option>\r\n  <option value=\"{{signOptions.GreaterThanOrEqualTo}}\">≥</option>\r\n  <option value=\"{{signOptions.LessThanOrEqualTo}}\">≤</option>\r\n</select>\r\n\r\n<w-number \r\n  class=\"col-8\"\r\n  [column]=\"column\" \r\n  [mode]=\"mode\" \r\n  [widgetValue]=\"currentVal\" \r\n  (widgetValueChange)=\"addItem($event);\">\r\n</w-number>\r\n<div class=\"f-number__item-list\">\r\n  <ng-container *ngFor=\"let filter of column.filterValueList\">\r\n    <div class=\"f-number__item\" (click)=\"removeItem(filter)\" [ngSwitch]=\"filter.sign\">\r\n        <ng-container *ngIf=\"filter.logicElement != logicElements.Empty\">\r\n          {{ filter.logicElement}}\r\n        </ng-container>\r\n        <ng-template [ngSwitchCase]=\"signOptions.Equals\">=</ng-template>\r\n        <ng-template [ngSwitchCase]=\"signOptions.NotEqualTo\">≠</ng-template>\r\n        <ng-template [ngSwitchCase]=\"signOptions.GreaterThan\">&gt;</ng-template>\r\n        <ng-template [ngSwitchCase]=\"signOptions.LessThan\">&lt;</ng-template>\r\n        <ng-template [ngSwitchCase]=\"signOptions.GreaterThanOrEqualTo\">≥</ng-template>\r\n        <ng-template [ngSwitchCase]=\"signOptions.LessThanOrEqualTo\">≤</ng-template>\r\n        {{ filter.value }}\r\n    </div>\r\n  </ng-container>\r\n</div>"

/***/ }),

/***/ "./src/app/filters/f-number/f-number.component.scss":
/***/ (function(module, exports) {

module.exports = ".f-number__item:hover {\n  text-decoration: line-through;\n  cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/filters/f-number/f-number.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FNumberComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_shared_interfaces__ = __webpack_require__("./src/app/shared/interfaces.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_widgets_w_number_w_number_component__ = __webpack_require__("./src/app/widgets/w-number/w-number.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FNumberComponent = /** @class */ (function () {
    function FNumberComponent() {
        this.mode = {};
        this.widgetValue = { fieldname: '', valueList: [] };
        this.filterValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */](); // объект генерит события
        this.currentVal = '';
        this.signOptions = __WEBPACK_IMPORTED_MODULE_1_app_shared_interfaces__["c" /* SignOptions */];
        this.logicElements = __WEBPACK_IMPORTED_MODULE_1_app_shared_interfaces__["a" /* LogicElements */];
    }
    FNumberComponent.prototype.notifyParent = function () {
        this.widgetValue.fieldname = this.column.fieldname;
        this.widgetValue.valueList = this.column.filterValueList;
        this.filterValueChange.emit(this.widgetValue);
    };
    FNumberComponent.prototype.addItem = function (item) {
        var logicElement = __WEBPACK_IMPORTED_MODULE_1_app_shared_interfaces__["a" /* LogicElements */].Empty;
        if ((this.logicElement) && (this.logicElement.nativeElement)) {
            logicElement = this.logicElement.nativeElement.value;
        }
        var filter = {
            value: item,
            sign: this.signOption.nativeElement.value,
            logicElement: logicElement
        };
        if (this.column.filterValueList.indexOf(filter) === -1) {
            this.column.filterValueList.push(filter);
        }
        console.log("Item " + filter.logicElement + " " + item + " was added to f-number component");
        this.widget.widgetValue = '';
        this.notifyParent();
    };
    FNumberComponent.prototype.removeItem = function (item) {
        var index = this.column.filterValueList.indexOf(item, 0);
        if (index > -1) {
            this.column.filterValueList.splice(index, 1);
        }
        console.log("Item " + item + " was removed from f-number component");
        this.notifyParent();
    };
    FNumberComponent.prototype.showLogicElement = function () {
        if (this.column.filterValueList === undefined) {
            return false;
        }
        return this.column.filterValueList.length > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FNumberComponent.prototype, "column", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FNumberComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FNumberComponent.prototype, "widgetValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", Object)
    ], FNumberComponent.prototype, "filterValueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_app_widgets_w_number_w_number_component__["a" /* WNumberComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_app_widgets_w_number_w_number_component__["a" /* WNumberComponent */])
    ], FNumberComponent.prototype, "widget", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('logicElement'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], FNumberComponent.prototype, "logicElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('signOption'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], FNumberComponent.prototype, "signOption", void 0);
    FNumberComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'f-number',
            template: __webpack_require__("./src/app/filters/f-number/f-number.component.html"),
            styles: [__webpack_require__("./src/app/filters/f-number/f-number.component.scss")]
        })
    ], FNumberComponent);
    return FNumberComponent;
}());



/***/ }),

/***/ "./src/app/filters/filter/filter.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/filters/filter/filter.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"column.widget.name\">\r\n  <f-edit class=\"f-edit\" \r\n    *ngSwitchCase=\"'edit'\"  \r\n    [column]=\"column\" \r\n    [mode]=\"mode\" \r\n    [widgetValue]=\"filterValue\"  \r\n    (valueChange)=\"passFilter($event)\">\r\n  </f-edit>\r\n  <f-edit class=\"f-edit\" \r\n    *ngSwitchCase=\"'textarea'\"  \r\n    [column]=\"column\" \r\n    [mode]=\"mode\" \r\n    [widgetValue]=\"filterValue\" \r\n    (valueChange)=\"passFilter($event)\">\r\n  </f-edit>\r\n  <f-number class=\"form-row\"\r\n    *ngSwitchCase=\"'number'\"  \r\n    [column]=\"column\" \r\n    [mode]=\"mode\" \r\n    [widgetValue]=\"filterValue\"  \r\n    (filterValueChange)=\"passFilter($event)\">\r\n  </f-number>\r\n  <f-date \r\n    *ngSwitchCase=\"'date'\"  \r\n    [attr]=\"column\" \r\n    [mode]=\"mode\" \r\n    [widgetValue]=\"filterValue\" \r\n    (valueChange)=\"changeVal($event)\">\r\n  </f-date>\r\n  <w-boolean *ngSwitchCase=\"'boolean'\"  \r\n    [attr]=\"column\" \r\n    [widgetValue]=\"filterValue\"  \r\n    [mode]=\"mode\" \r\n    (valueChange)=\"changeVal($event)\">\r\n  </w-boolean>\r\n  <span *ngSwitchDefault></span>\r\n</ng-container>"

/***/ }),

/***/ "./src/app/filters/filter/filter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WFilterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WFilterComponent = /** @class */ (function () {
    function WFilterComponent() {
        this.mode = 'forminput';
        this.filterValue = {};
        this.filterChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    WFilterComponent.prototype.changeVal = function (newValue) {
        console.log('w-filter newValue = ', newValue);
        // надо получить название столбца, например Authors
        // this.filterObject.fieldName = this.attr.fieldname;
        // this.filterObject.filterValues = newValue;
        // this.filterChange.emit([this.filterObject]);
    };
    WFilterComponent.prototype.passFilter = function (filter) {
        this.filterChange.emit(filter);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WFilterComponent.prototype, "column", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WFilterComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WFilterComponent.prototype, "filterValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WFilterComponent.prototype, "filterChange", void 0);
    WFilterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'w-filter',
            template: __webpack_require__("./src/app/filters/filter/filter.component.html"),
            styles: [__webpack_require__("./src/app/filters/filter/filter.component.css")]
        })
    ], WFilterComponent);
    return WFilterComponent;
}());



/***/ }),

/***/ "./src/app/front-page/front-page-map/front-page-map.component.html":
/***/ (function(module, exports) {

module.exports = "<div leaflet\r\n  id=\"maps_front-page-map\"\r\n  [ngStyle]=\"{'height': '100%'}\"\r\n  [leafletOptions]=\"options\"\r\n  (leafletMapReady)=\"onMapReady($event)\">\r\n  <div *ngIf=\"drawingIsEnabled\"\r\n    leafletDraw\r\n    featureGroup=\"featureGroup\"\r\n    [leafletDrawOptions]=\"drawOptions\"></div>\r\n</div>"

/***/ }),

/***/ "./src/app/front-page/front-page-map/front-page-map.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FrontPageMapComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__map_map_component__ = __webpack_require__("./src/app/map/map.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map_service__ = __webpack_require__("./src/app/front-page/map.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FrontPageMapComponent = /** @class */ (function (_super) {
    __extends(FrontPageMapComponent, _super);
    function FrontPageMapComponent(mapService, store, actions) {
        var _this = _super.call(this, mapService, store, actions) || this;
        _this.mapService = mapService;
        _this.store = store;
        _this.actions = actions;
        _this.mapId = 'maps_front-page-map';
        return _this;
    }
    FrontPageMapComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'front-page-map',
            template: __webpack_require__("./src/app/front-page/front-page-map/front-page-map.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__map_service__["a" /* MapService */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_4__store_app_actions__["a" /* AppActions */]])
    ], FrontPageMapComponent);
    return FrontPageMapComponent;
}(__WEBPACK_IMPORTED_MODULE_1__map_map_component__["a" /* MapComponent */]));



/***/ }),

/***/ "./src/app/front-page/front-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"layout__menu\" main-menu></div>\r\n<div class=\"layout__map\">\r\n    <div style=\"width: 100%; height: 100%; z-index: -1000; background-image: url('/assets/map-is-temporary-disabled.png'); position: absolute; top: 0px; left: 0px;\"></div>\r\n    <div style=\"width: 100%; height: 100%;\">\r\n        <span #mapPlacement></span>\r\n    </div>\r\n</div>\r\n<div class=\"layout__content\">   \r\n    <app-tablelist></app-tablelist>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/front-page/front-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FrontPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__front_page_map_front_page_map_component__ = __webpack_require__("./src/app/front-page/front-page-map/front-page-map.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FrontPageComponent = /** @class */ (function () {
    function FrontPageComponent(store, appActions, componentFactoryResolver, renderer, el) {
        this.store = store;
        this.appActions = appActions;
        this.componentFactoryResolver = componentFactoryResolver;
        this.renderer = renderer;
        this.el = el;
    }
    FrontPageComponent.prototype.ngOnInit = function () {
        var _this = this;
        var mapFactory = this.componentFactoryResolver.resolveComponentFactory(__WEBPACK_IMPORTED_MODULE_4__front_page_map_front_page_map_component__["a" /* FrontPageMapComponent */]);
        var componentRef = this.mapPlacementContainer.createComponent(mapFactory);
        this.store.select(Object(__WEBPACK_IMPORTED_MODULE_2__store_app_reducer__["b" /* stateGetter */])('frontPageMap', 'host')).subscribe(function (data) {
            if (data === 'regular') {
                _this.store
                    .select(Object(__WEBPACK_IMPORTED_MODULE_2__store_app_reducer__["b" /* stateGetter */])('frontPageMap', 'ref'))
                    .take(1)
                    .subscribe(function (data) {
                    _this.mapPlacementContainer.insert(data.hostView);
                });
            }
            else {
                _this.mapPlacementContainer.detach(_this.mapPlacementContainer.indexOf(componentRef.hostView));
                _this.appActions.setFrontPageMapRef({ ref: componentRef });
            }
        });
        this.user$ = this.store.select(Object(__WEBPACK_IMPORTED_MODULE_2__store_app_reducer__["b" /* stateGetter */])('user'));
        this.appActions.setFrontPageMapHost({ host: 'regular' });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('mapPlacement', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewContainerRef */] }),
        __metadata("design:type", Object)
    ], FrontPageComponent.prototype, "mapPlacementContainer", void 0);
    FrontPageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/front-page/front-page.component.html"),
            styles: [
                '.layout__map { width: 100%; height: 40%; position: relative; }',
                '.layout__content { margin: 0.75rem; }',
                '.layout__menu { z-index: 1000; width: 100%; }',
                '.mapcontainer { position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; }'
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__store_app_actions__["a" /* AppActions */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* ComponentFactoryResolver */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["Y" /* Renderer2 */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]])
    ], FrontPageComponent);
    return FrontPageComponent;
}());



/***/ }),

/***/ "./src/app/front-page/map.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MapService = /** @class */ (function () {
    function MapService(actions) {
        this.actions = actions;
        this._maps = [];
    }
    MapService.prototype.disableMouseEvent = function (event) { };
    MapService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__store_app_actions__["a" /* AppActions */]])
    ], MapService);
    return MapService;
}());



/***/ }),

/***/ "./src/app/main-menu/main-menu.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"menu-container\">\r\n  <nav class=\"navbar navbar-expand-sm navbar-dark bg-primary\">\r\n    <a href=\"/\" class=\"navbar-brand\">Geoportal</a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbar6\">\r\n          <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div class=\"navbar-collapse collapse\" id=\"navbar6\">\r\n          <ul class=\"navbar-nav\">\r\n              <li class=\"nav-item active\">\r\n                  <a class=\"nav-link\" routerLink=\"/\">Home</a>\r\n              </li>\r\n              <li class=\"nav-item\">\r\n                <div ngbDropdown class=\"d-inline-block\">\r\n                  <a class=\"nav-link\" id=\"dropdownBasic1\" ngbDropdownToggle>Services manager</a>\r\n                  <div ngbDropdownMenu aria-labelledby=\"dropdownBasic1\">\r\n                    <a class=\"dropdown-item\" routerLink=\"/services-manager\">Browse</a>\r\n                    <a class=\"dropdown-item\" routerLink=\"/services-manager/create\">Register service or scenario</a>\r\n                  </div>\r\n                </div>\r\n              </li>\r\n          </ul>\r\n          <ul class=\"nav navbar-nav flex-row justify-content-between ml-auto\">\r\n              <li class=\"nav-item\">\r\n                  <a *ngIf=\"!currentUser\" class=\"btn btn-primary\" routerLink=\"/register\">Register</a>\r\n                  <button *ngIf=\"!currentUser\" type=\"button\" class=\"btn btn-outline-primary\" (click)=\"open(content)\">Log in</button>\r\n                  <form *ngIf=\"currentUser\" class=\"form-inline\">\r\n                    <label>Hello, {{ currentUser.name }}</label>\r\n                    <button type=\"button\" class=\"btn btn-outline-primary\" (click)=\"logout()\">Log out</button>\r\n                  </form>\r\n              </li>\r\n          </ul>\r\n      </div>\r\n  </nav>\r\n  <ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n    <div class=\"modal-header\">\r\n      <h4 class=\"modal-title\">Login to Geoportal</h4>\r\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d()\">\r\n        <span aria-hidden=\"true\">&times;</span>\r\n      </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <form #f=\"ngForm\" novalidate>\r\n        <div class=\"form-group row\">\r\n          <label class=\"col-sm-4 col-form-label\">Login</label>\r\n          <div class=\"col-sm-8\">\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"mylogin\"\r\n              [(ngModel)]=\"model.login\" name=\"login\" [ngClass]=\"{'is-invalid': error}\" required>\r\n          </div>\r\n        </div>\r\n        <div class=\"form-group row\">\r\n          <label class=\"col-sm-4 col-form-label\">Password</label>\r\n          <div class=\"col-sm-8\">\r\n            <input type=\"password\" class=\"form-control\" placeholder=\"abc123!\"\r\n              [(ngModel)]=\"model.password\" name=\"password\" [ngClass]=\"{'is-invalid': error}\" required>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12 text-center\" style=\"padding-bottom: 10px;\">\r\n            <a href=\"#\">Forgot password?</a>\r\n          </div>\r\n        </div>\r\n        <div>\r\n          <button type=\"button\" [disabled]=\"!model.login || model.login.length === 0 || !model.password || model.password.length < 6 || loading\"\r\n            class=\"btn btn-primary\" style=\"width: 100%;\" (click)=\"login()\">\r\n            <span *ngIf=\"loading\">Loading...</span>\r\n            <span *ngIf=\"!loading\">Login</span>\r\n          </button>\r\n        </div>\r\n      </form>\r\n    </div>\r\n  </ng-template>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/main-menu/main-menu.component.scss":
/***/ (function(module, exports) {

module.exports = ".menu-container {\n  width: 100%;\n  background-color: pink; }\n\nnav .btn {\n  color: #fff;\n  border-color: #fff;\n  cursor: pointer; }\n\nnav .btn:hover {\n  background-color: #3399ff; }\n\nnav label {\n  color: white;\n  padding-right: 10px; }\n\n.dropdown-toggle::after {\n  content: none !important; }\n"

/***/ }),

/***/ "./src/app/main-menu/main-menu.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MainMenuComponent = /** @class */ (function () {
    function MainMenuComponent(modalService, apiService, store) {
        var _this = this;
        this.modalService = modalService;
        this.apiService = apiService;
        this.store = store;
        this.model = {};
        this.loading = false;
        this.error = false;
        this.store.select(Object(__WEBPACK_IMPORTED_MODULE_4__store_app_reducer__["b" /* stateGetter */])('user')).subscribe(function (value) {
            _this.currentUser = value;
        });
    }
    MainMenuComponent.prototype.ngOnInit = function () { };
    MainMenuComponent.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.error = false;
        this.apiService.login(this.model.login, this.model.password).subscribe(function (data) {
            _this.loading = false;
            _this.error = true;
            _this.modalReference.close();
        }, function (error) {
            _this.loading = false;
            _this.error = true;
        });
    };
    MainMenuComponent.prototype.logout = function () {
        this.loading = false;
        this.error = false;
        this.apiService.logout();
    };
    MainMenuComponent.prototype.open = function (content) {
        this.modalReference = this.modalService.open(content);
    };
    MainMenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: '[main-menu]',
            template: __webpack_require__("./src/app/main-menu/main-menu.component.html"),
            styles: [__webpack_require__("./src/app/main-menu/main-menu.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */], __WEBPACK_IMPORTED_MODULE_3__shared_api_service__["a" /* APIService */], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], MainMenuComponent);
    return MainMenuComponent;
}());



/***/ }),

/***/ "./src/app/map/map.component.html":
/***/ (function(module, exports) {

module.exports = "<div leaflet style=\"height: 400px;\" [leafletOptions]=\"options\">\r\n  <div\r\n    *ngIf=\"drawingIsEnabled\"\r\n    leafletDraw\r\n    [leafletDrawOptions]=\"drawOptions\"></div>\r\n</div>"

/***/ }),

/***/ "./src/app/map/map.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__front_page_map_service__ = __webpack_require__("./src/app/front-page/map.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_wellknown__ = __webpack_require__("./node_modules/wellknown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_wellknown___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_wellknown__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_timers__ = __webpack_require__("./node_modules/timers-browserify/main.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_timers___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_timers__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var noDrawingControlSetting = {
    marker: false,
    polyline: false,
    polygon: false,
    circle: false,
    rectangle: false,
    circlemarker: false
};
var MapComponent = /** @class */ (function () {
    function MapComponent(mapService, store, actions) {
        this.mapService = mapService;
        this.store = store;
        this.actions = actions;
        this.drawingMode = 0;
        this.drawingIsEnabled = false;
        this.mapId = '';
        this.featureGroup = L.featureGroup();
        this.lastLayer = false;
        this.options = {
            zoomControl: true,
            center: L.latLng(52.54963, 104.4580),
            zoom: 6,
            minZoom: 4,
            maxZoom: 19
        };
        this.baseMaps = {
            OpenStreetMap: L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'),
            Esri: L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'),
            CartoDB: L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png')
        };
        this.drawOptions = {
            position: 'topright',
            draw: {}
        };
    }
    MapComponent.prototype.ngOnInit = function () { };
    MapComponent.prototype.clearFeatures = function () {
        var _this = this;
        if (this.featureGroup) {
            this.featureGroup.eachLayer(function (layer) {
                _this.map.removeLayer(layer);
            });
        }
    };
    MapComponent.prototype.clearDrawnFeatures = function () {
        var _this = this;
        Object(__WEBPACK_IMPORTED_MODULE_6_timers__["setTimeout"])(function () {
            if (_this.drawOptions.edit) {
                _this.drawOptions.edit.featureGroup.removeLayer(_this.lastLayer);
            }
        }, 100);
    };
    MapComponent.prototype.onMapReady = function (event) {
        var _this = this;
        this.map = event;
        this.map.on('draw:created', function (data) {
            _this.lastLayer = data.layer;
            var wktRepresentation = '';
            switch (_this.drawingMode) {
                case _this.constructor['MODE_POINT']:
                    _this.actions.setLastPointForMap({
                        mapId: _this.mapId,
                        wkt: __WEBPACK_IMPORTED_MODULE_5_wellknown__["stringify"](data.layer.toGeoJSON())
                    });
                    break;
                case _this.constructor['MODE_LINE']:
                    _this.actions.setLastLineForMap({
                        mapId: _this.mapId,
                        wkt: __WEBPACK_IMPORTED_MODULE_5_wellknown__["stringify"](data.layer.toGeoJSON())
                    });
                    break;
                case _this.constructor['MODE_POLYGON']:
                    _this.actions.setLastPolygonForMap({
                        mapId: _this.mapId,
                        wkt: __WEBPACK_IMPORTED_MODULE_5_wellknown__["stringify"](data.layer.toGeoJSON())
                    });
                    break;
            }
            _this.drawingIsEnabled = false;
        });
        L.control.layers(this.baseMaps, {}).addTo(this.map);
        this.map.addLayer(L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'));
        this.actions.addMap({ mapId: this.mapId });
        this.store.select(Object(__WEBPACK_IMPORTED_MODULE_4__store_app_reducer__["b" /* stateGetter */])(this.mapId + '')).subscribe(function (value) {
            _this.clearFeatures();
            _this.clearDrawnFeatures();
            if (_this.drawingIsEnabled !== value['drawingIsEnabled']) {
                var newDrawOptions = Object.assign({}, _this.drawOptions);
                newDrawOptions.draw = Object.assign({}, noDrawingControlSetting);
                if (value['existingFeature']) {
                    var parsedFeature = __WEBPACK_IMPORTED_MODULE_5_wellknown__["parse"](value['existingFeature']);
                    var newLayer = L.geoJSON(parsedFeature, {
                        pointToLayer: function (feature, latlng) {
                            return L.marker(latlng, {
                                icon: L.icon({
                                    iconSize: [25, 41],
                                    iconAnchor: [13, 41],
                                    iconUrl: 'assets/marker-icon-red.png',
                                    shadowUrl: 'assets/marker-shadow.png'
                                })
                            });
                        },
                        style: function () {
                            return {
                                weight: 3,
                                opacity: 1,
                                color: '#FF3333',
                                fillOpacity: 0.3,
                                fillColor: '#FF3333'
                            };
                        }
                    });
                    _this.featureGroup.addLayer(newLayer);
                    _this.featureGroup.addTo(_this.map);
                }
                switch (value['drawingMode']) {
                    case _this.constructor['MODE_POINT']:
                        newDrawOptions.draw.marker = {
                            icon: L.icon({
                                iconSize: [25, 41],
                                iconAnchor: [13, 41],
                                iconUrl: 'assets/marker-icon.png',
                                shadowUrl: 'assets/marker-shadow.png'
                            })
                        };
                        break;
                    case _this.constructor['MODE_LINE']:
                        newDrawOptions.draw.polyline = true;
                        break;
                    case _this.constructor['MODE_POLYGON']:
                        newDrawOptions.draw.polygon = true;
                        break;
                }
                _this.drawingMode = value['drawingMode'];
                _this.drawOptions = newDrawOptions;
                _this.drawingIsEnabled = value['drawingIsEnabled'];
            }
        });
    };
    MapComponent.MODE_POINT = 0;
    MapComponent.MODE_LINE = 1;
    MapComponent.MODE_POLYGON = 2;
    MapComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/map/map.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__front_page_map_service__["a" /* MapService */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_3__store_app_actions__["a" /* AppActions */]])
    ], MapComponent);
    return MapComponent;
}());



/***/ }),

/***/ "./src/app/modal-confirmation/modal-confirmation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title\">{{ title }}</h4>\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss(ModalResult.mrCancel)\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <p>{{ body }}</p>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"yesAnswer()\">Yes</button>\r\n  <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"noAnswer()\">No</button>\r\n  <!-- <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"noAnswer()\">No</button> -->\r\n</div>"

/***/ }),

/***/ "./src/app/modal-confirmation/modal-confirmation.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modal-confirmation/modal-confirmation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalConfirmationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_shared_interfaces__ = __webpack_require__("./src/app/shared/interfaces.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModalConfirmationComponent = /** @class */ (function () {
    function ModalConfirmationComponent(activeModal) {
        this.activeModal = activeModal;
    }
    ModalConfirmationComponent.prototype.ngOnInit = function () {
    };
    ModalConfirmationComponent.prototype.yesAnswer = function () {
        this.activeModal.close(__WEBPACK_IMPORTED_MODULE_2_app_shared_interfaces__["b" /* ModalResult */].mrYes);
    };
    ModalConfirmationComponent.prototype.noAnswer = function () {
        this.activeModal.close(__WEBPACK_IMPORTED_MODULE_2_app_shared_interfaces__["b" /* ModalResult */].mrNo);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], ModalConfirmationComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], ModalConfirmationComponent.prototype, "body", void 0);
    ModalConfirmationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-modal-confirmation',
            template: __webpack_require__("./src/app/modal-confirmation/modal-confirmation.component.html"),
            styles: [__webpack_require__("./src/app/modal-confirmation/modal-confirmation.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbActiveModal */]])
    ], ModalConfirmationComponent);
    return ModalConfirmationComponent;
}());



/***/ }),

/***/ "./src/app/services-and-scenarios-edit-form/catalog-item-parameter/catalog-item-parameter.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"border-top: 1px solid #eaebed; padding: 10px;\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <div class=\"form-group row\">\r\n            <label class=\"col-sm-3\">Identifier</label>\r\n            <input class=\"col-sm-9 form-control\" type=\"text\" (blur)=\"onModelChange($event)\" name=\"identifier\" [(ngModel)]=\"identifier\" [readonly]=\"type === 'service_wps'\"/>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label class=\"col-sm-3\">Title</label>\r\n            <input class=\"col-sm-9 form-control\" type=\"text\" (blur)=\"onModelChange($event)\" name=\"title\" [(ngModel)]=\"title\" />\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <label class=\"col-sm-3\">Description</label>\r\n            <textarea class=\"col-sm-9 form-control\" name=\"description\" (blur)=\"onModelChange($event)\" [(ngModel)]=\"description\"></textarea>\r\n          </div>\r\n        </div>\r\n        <div class=\"col\">\r\n          <div *ngIf=\"type === 'scenario'\" style=\"float: right;\">\r\n            <button type=\"button\" class=\"btn btn-sm btn-danger\" (click)=\"onDelete($event)\">\r\n              <i class=\"fa fa-remove\" aria-hidden=\"true\"></i> Delete parameter\r\n            </button>\r\n          </div>\r\n          <label>Widget</label>\r\n          <div class=\"alert alert-danger\" role=\"alert\">\r\n            <strong>@todo</strong> Not implemented yet\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/services-and-scenarios-edit-form/catalog-item-parameter/catalog-item-parameter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatalogItemParameterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

;
var CatalogItemParameterComponent = /** @class */ (function () {
    function CatalogItemParameterComponent() {
        this.onParameterChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.onParameterDelete = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    /**
     * Triggered on @Input() change
     */
    CatalogItemParameterComponent.prototype.ngOnChanges = function () {
        this.identifier = this.parameter.identifier;
        this.title = this.parameter.title;
        this.description = this.parameter.description;
    };
    /**
     * Triggered on model change
     */
    CatalogItemParameterComponent.prototype.onModelChange = function () {
        this.onParameterChange.emit({
            identifier: this.identifier,
            title: this.title,
            description: this.description,
            widget: ''
        });
    };
    /**
     * Deletes parameter
     */
    CatalogItemParameterComponent.prototype.onDelete = function (event) {
        this.onParameterDelete.emit();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], CatalogItemParameterComponent.prototype, "type", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], CatalogItemParameterComponent.prototype, "parameter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", Object)
    ], CatalogItemParameterComponent.prototype, "onParameterChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", Object)
    ], CatalogItemParameterComponent.prototype, "onParameterDelete", void 0);
    CatalogItemParameterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'catalog-item-parameter',
            template: __webpack_require__("./src/app/services-and-scenarios-edit-form/catalog-item-parameter/catalog-item-parameter.component.html"),
            styles: []
        })
    ], CatalogItemParameterComponent);
    return CatalogItemParameterComponent;
}());



/***/ }),

/***/ "./src/app/services-and-scenarios-edit-form/services-and-scenarios-edit-form.component.html":
/***/ (function(module, exports) {

module.exports = "<div static-page-layout>\r\n  <form>\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <h3 *ngIf=\"id <= 0\">Create Services and scenarios catalog item</h3>\r\n          <h3 *ngIf=\"id > 0\">Update {{ name }} <button routerLink=\"/services-manager/run/{{ id }}\" type=\"button\" class=\"btn btn-sm\"><i class=\"fa fa-play\"></i> Run</button>\r\n            <span class=\"badge badge-info\" *ngIf=\"type === 'service_wps'\">WPS service</span>\r\n            <span class=\"badge badge-info\" *ngIf=\"type === 'scenario'\">JavaScript scenario</span>\r\n          </h3>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n            <div class=\"form-group\">\r\n              <label>Name</label>\r\n              <input type=\"text\" [(ngModel)]=\"name\" (ngModelChange)=\"generateTextualIdentifier($event)\" name=\"name\" class=\"form-control\" placeholder=\"My service\">\r\n              <input type=\"text\" [ngModel]=\"textualIdentifier\" name=\"textualIdentifier\" readonly class=\"form-control\" placeholder=\"myservice\">\r\n              <small class=\"form-text text-muted\">The disabled field will be used as an internal identifier</small>\r\n            </div>\r\n        </div>\r\n        <div class=\"col\">\r\n          <div class=\"form-group\">\r\n            <label>Description</label>\r\n            <textarea [(ngModel)]=\"description\" name=\"description\" class=\"form-control\" placeholder=\"Description\" style=\"height: 95px;\"></textarea>\r\n          </div>\r\n        </div>\r\n        <div class=\"col\">\r\n          <div class=\"form-group\">\r\n            <label>MapReduce specification</label>\r\n            <textarea [(ngModel)]=\"mapReduceSpecification\" name=\"mapReduceSpecification\" class=\"form-control\" placeholder=\"MapReduce specification\" style=\"height: 95px;\"></textarea>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\" *ngIf=\"id === -1\">\r\n        <div class=\"col\">\r\n          <label>Type of registered Catalog item </label>\r\n        </div>\r\n        <div class=\"col\">\r\n          <div class=\"form-check form-check-inline\" *ngFor=\"let localType of types\">\r\n            <label class=\"form-check-label\">\r\n              <input class=\"form-check-input\" (change)=\"onTypeChange($event)\" type=\"radio\" name=\"type\" value=\"{{ localType.id }}\">{{ localType.title }}\r\n            </label>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\" *ngIf=\"!type\">\r\n        <div class=\"col text-center\">\r\n          <div class=\"alert alert-info\" role=\"alert\">\r\n            The Catalog item editing form will be available after selectiing the Type of item\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- JavaScript scenario editing -->\r\n      <div class=\"row scenario-container\" *ngIf=\"type === 'scenario'\">\r\n        <div class=\"col\">\r\n          <div class=\"container section-scenario\">\r\n            <div class=\"row\" style=\"padding-top: 10px;\">\r\n              <div class=\"col\">\r\n                  <h4>Input parameters <button class=\"btn btn-primary btn-sm\" type=\"button\" (click)=\"addSampleParameter($event, 'input')\">\r\n                    <i class=\"fa fa-plus\" aria-hidden=\"true\"></i> Add parameter\r\n                  </button></h4>\r\n                  <catalog-item-parameter *ngFor=\"let parameter of inputParameters; index as i;\"\r\n                    [parameter]=\"parameter\"\r\n                    (onParameterDelete)=\"onParameterDelete($event, 'input', i)\"\r\n                    (onParameterChange)=\"onParameterChange($event, 'input', i)\"\r\n                    [type]=\"type\"></catalog-item-parameter>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" style=\"padding-top: 10px;\">\r\n              <div class=\"col\">\r\n                  <h4>Output parameters <button class=\"btn btn-primary btn-sm\" type=\"button\" (click)=\"addSampleParameter($event, 'output')\">\r\n                    <i class=\"fa fa-plus\" aria-hidden=\"true\"></i> Add parameter\r\n                  </button></h4>\r\n                  <catalog-item-parameter *ngFor=\"let parameter of outputParameters; index as i;\"\r\n                    [parameter]=\"parameter\"\r\n                    (onParameterDelete)=\"onParameterDelete($event, 'output', i)\"\r\n                    (onParameterChange)=\"onParameterChange($event, 'output', i)\"\r\n                    [type]=\"type\"></catalog-item-parameter>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\">\r\n              <div class=\"col\">\r\n                <label>Enter the scenario code</label>\r\n                <div ace-editor [(text)]=\"scenarioCode\" [theme]=\"'eclipse'\" [mode]=\"'javascript'\" style=\"min-height: 200px; width:100%; overflow: auto;\"></div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"parametersAreFilledUp()\" style=\"padding-top: 10px;\">\r\n              <div class=\"col\" *ngIf=\"id === -1\">\r\n                <button type=\"button\"\r\n                  [disabled]=\"!(textualIdentifier.length > 0 && description.length > 0 && parametersAreFilledUp())\"\r\n                  (click)=\"register($event)\" class=\"btn btn-primary btn-block\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i> Save JavaScript scenario</button>\r\n              </div>\r\n              <div class=\"col\" *ngIf=\"id > 0\">\r\n                <button type=\"button\"\r\n                  [disabled]=\"!(textualIdentifier.length > 0 && description.length > 0 && parametersAreFilledUp())\"\r\n                  (click)=\"update($event)\" class=\"btn btn-primary btn-block\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i> Update JavaScript scenario</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- WPS service editing -->\r\n      <div class=\"row service_wps-container\" *ngIf=\"type === 'service_wps'\">\r\n        <div class=\"col\">\r\n          <div class=\"container section-service_wps\">\r\n            <div class=\"row\">\r\n              <div class=\"col\">\r\n                <div class=\"form-inline\">\r\n                  <label class=\"mr-sm-2\">Add servers with service</label>               \r\n                  <label class=\"mr-sm-2\">\r\n                    <input type=\"text\" class=\"form-control form-control-sm\" [(ngModel)]=\"host\" name=\"host\" placeholder=\"Host (myhostname.com)\">\r\n                  </label>\r\n                  <label class=\"mr-sm-1\">\r\n                      <input type=\"text\" class=\"form-control form-control-sm\" [(ngModel)]=\"port\" name=\"port\" placeholder=\"Port (8080)\">\r\n                  </label>\r\n                  <label class=\"mr-sm-2\">\r\n                    <div class=\"input-group\">\r\n                      <input type=\"text\" class=\"form-control form-control-sm\" [(ngModel)]=\"path\" name=\"path\" placeholder=\"WPS server path\">\r\n                      <span class=\"input-group-btn\">\r\n                        <button class=\"btn btn-secondary btn-sm\" type=\"button\" placement=\"top\" ngbPopover=\"Provide the path to WPS server, for example, '/cgi-bin/wps/zoo_loader.cgi?'\" popoverTitle=\"WPS server path\">?</button>\r\n                      </span>\r\n                    </div>\r\n                  </label>\r\n                  <label class=\"mr-sm-1\">\r\n                    <button type=\"submit\" class=\"btn btn-primary btn-sm\" [disabled]=\"!(host && port && path && path !== 0)\" (click)=\"addServiceServer($event)\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i> Add</button>\r\n                  </label>\r\n                  <label class=\"mr-sm-1\">\r\n                    <button type=\"button\" *ngIf=\"id === -1\" class=\"btn btn-primary btn-sm\" [disabled]=\"servers.length === 0\" (click)=\"fetchServices($event)\">\r\n                      <span *ngIf=\"servers.length === 0\">Enter at least one services server</span>\r\n                      <span *ngIf=\"servers.length > 0\"><i class=\"fa fa-refresh\" aria-hidden=\"true\"></i> Fetch the list of available services</span>\r\n                    </button>\r\n                  </label>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row services_server-container\">\r\n              <div class=\"col\" *ngIf=\"servers.length !== 0\">                \r\n                <div class=\"services_server-container__item\" *ngFor=\"let server of servers\">\r\n                  {{server.host + ':' + server.port + server.path}}\r\n                  <button type=\"button\" class=\"btn btn-outline-primary btn-sm\" (click)=\"deleteServicesServer($event, index)\">\r\n                    <i class=\"fa fa-times\"></i>\r\n                  </button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" style=\"padding: 5px;\" *ngIf=\"id === -1\">\r\n              <div class=\"col text-center\">\r\n                <p *ngIf=\"availableServices.length === 0\">The list of WPS services will be fetched from the <strong>first</strong> available WPS server defined above</p>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"availableServices.length > 0\">\r\n              <div class=\"col\">\r\n                <div class=\"form-inline\">\r\n                  <label class=\"mr-sm-2\">  \r\n                    <label>Available services</label>\r\n                  </label>\r\n                  <label class=\"mr-sm-2\">\r\n                    <select class=\"form-control\" (change)=\"fetchServiceParameter($event.target.value)\">\r\n                      <option value=\"null\">Choose one of the available services</option>\r\n                      <option *ngFor=\"let service of availableServices\" value=\"{{service.identifier}}\">{{ service.title }}</option>\r\n                    </select>\r\n                  </label>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"inputParameters.length > 0\" style=\"padding-top: 10px;\">\r\n              <div class=\"col\">\r\n                  <h4>Input parameters</h4>\r\n                  <catalog-item-parameter *ngFor=\"let parameter of inputParameters; index as i;\"\r\n                    [parameter]=\"parameter\"\r\n                    (onParameterDelete)=\"onParameterDelete($event, 'input', i)\"\r\n                    (onParameterChange)=\"onParameterChange($event, 'input', i)\"\r\n                    [type]=\"type\"></catalog-item-parameter>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"outputParameters.length > 0\">\r\n              <div class=\"col\">\r\n                <h4>Output parameters</h4>\r\n                <catalog-item-parameter *ngFor=\"let parameter of outputParameters; index as i;\"\r\n                  [parameter]=\"parameter\"\r\n                  (onParameterDelete)=\"onParameterDelete($event, 'output', i)\"\r\n                  (onParameterChange)=\"onParameterChange($event, 'output', i)\"\r\n                  [type]=\"type\"></catalog-item-parameter>\r\n              </div>\r\n            </div>\r\n            <div class=\"row\" *ngIf=\"parametersAreFilledUp()\">\r\n              <div class=\"col\" *ngIf=\"id === -1\">\r\n                <button type=\"button\"\r\n                  [disabled]=\"!(textualIdentifier.length > 0 && description.length > 0 && servers.length > 0 && parametersAreFilledUp())\"\r\n                  (click)=\"register($event)\" class=\"btn btn-primary btn-block\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i> Save WPS service</button>\r\n              </div>\r\n              <div class=\"col\" *ngIf=\"id > 0\">\r\n                <button type=\"button\"\r\n                  [disabled]=\"!(textualIdentifier.length > 0 && description.length > 0 && servers.length > 0 && parametersAreFilledUp())\"\r\n                  (click)=\"update($event)\" class=\"btn btn-primary btn-block\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i> Update WPS service</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </form>\r\n  <ng-template #content let-d=\"dismiss\">\r\n    <div class=\"modal-header\">\r\n      <h4 class=\"modal-title\">Catalog was updated</h4>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <a class=\"btn btn-primary btn-block\" routerLink=\"/services-manager/run/{{ (this.retrievedId === -1) ? this.id : this.retrievedId }}\">Launch \r\n        <span *ngIf=\"this.type === 'service_wps'\">service</span>\r\n        <span *ngIf=\"this.type === 'scenario'\">scenario</span>\r\n      </a>\r\n      <a *ngIf=\"this.retrievedId !== -1\" class=\"btn btn-primary btn-block\" routerLink=\"/services-manager/edit/{{ this.retrievedId }}\">Back to editing</a>\r\n      <a *ngIf=\"this.retrievedId === -1\" class=\"btn btn-primary btn-block\" (click)=\"d()\" style=\"color: white;\">Back to editing</a>\r\n    </div>\r\n  </ng-template>\r\n</div>"

/***/ }),

/***/ "./src/app/services-and-scenarios-edit-form/services-and-scenarios-edit-form.component.scss":
/***/ (function(module, exports) {

module.exports = ".catalog-item-container, .service_wps-container, .scenario-container {\n  border-radius: 5px;\n  padding-top: 15px;\n  padding-bottom: 15px; }\n\n.service_wps-container {\n  border: 1px solid #66ccff; }\n\n.scenario-container {\n  border: 1px solid #3e4ec9; }\n\n.services_server-container {\n  padding-top: 5px; }\n\n.services_server-container__item {\n  border: 1px solid #66ccff;\n  padding: 5px;\n  border-radius: 5px;\n  float: left;\n  margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/services-and-scenarios-edit-form/services-and-scenarios-edit-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesAndScenariosEditFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_xml2js__ = __webpack_require__("./node_modules/xml2js/lib/xml2js.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_xml2js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_xml2js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





;
;
;
var SERVICE_WPS = 'service_wps';
var SCENARIO = 'scenario';
var DEFAULT_SERVICES_SERVER_PORT = 80;
var ServicesAndScenariosEditFormComponent = /** @class */ (function () {
    /**
     * The instaniation of the component
     *
     * @param api {APIService} Reruired to talk with the backend
     */
    function ServicesAndScenariosEditFormComponent(api, modalService, route) {
        var _this = this;
        this.api = api;
        this.modalService = modalService;
        this.route = route;
        this.id = -1;
        this.retrievedId = -1;
        this.name = '';
        this.textualIdentifier = '';
        this.description = '';
        this.serviceIdentifier = '';
        this.mapReduceSpecification = '';
        this.servers = [];
        this.availableServices = [];
        this.inputParameters = [];
        this.outputParameters = [];
        this.scenarioCode = '';
        this.scenarioEditorOptions = { maxLines: 1000, printMargin: false };
        this.types = [{
                id: SERVICE_WPS,
                title: 'WPS service'
            }, {
                id: SCENARIO,
                title: 'JavaScript scenario'
            }];
        this.host = '';
        this.port = DEFAULT_SERVICES_SERVER_PORT;
        this.path = '';
        this.servers = [{
                host: '84.237.16.46',
                port: 8800,
                path: '/cgi-bin/wps/zoo_loader.cgi?'
            }];
        route.params.map(function (p) { return p.id; }).subscribe(function (id) {
            if (_this.modalRef)
                _this.modalRef.close();
            if (parseInt(id) > 0) {
                _this.api.getCatalogItem(id).then(function (result) {
                    var data = result['data'];
                    _this.name = data['name'];
                    _this.textualIdentifier = data['name'];
                    _this.description = data['description'];
                    _this.type = ((data['type'] === 'wps') ? 'service_wps' : 'scenario');
                    _this.serviceIdentifier = ((data['type'] === 'wps') ? data['wpsmethod'] : '');
                    _this.mapReduceSpecification = data['map_reduce_specification'];
                    var wpsServers = [];
                    if (data['wpsservers']) {
                        data['wpsservers'].map(function (item) {
                            wpsServers.push({
                                host: item.host,
                                port: item.port,
                                path: item.path
                            });
                        });
                    }
                    _this.servers = wpsServers;
                    var scenarioCode = '';
                    if (data['js_body']) {
                        scenarioCode = data['js_body'].replace(/XXNEWLINEXX/g, '\n').replace(/XXQUOTEXX/g, '"');
                    }
                    _this.scenarioCode = scenarioCode;
                    var inputParameters = [];
                    data['params'].map(function (item) {
                        inputParameters.push({
                            identifier: item.fieldname,
                            title: item.title,
                            description: item.description
                        });
                    });
                    _this.inputParameters = inputParameters;
                    var outputParameters = [];
                    data['output_params'].map(function (item) {
                        outputParameters.push({
                            identifier: item.fieldname,
                            title: item.title,
                            description: item.description
                        });
                    });
                    _this.outputParameters = outputParameters;
                    _this.id = data['id'];
                }).catch(function (error) {
                    throw error;
                });
            }
        }, function (e) { return console.log('onError: %s', e); }, function () { return console.log('onCompleted'); });
    }
    /**
     * Checks if input parameters are all filled up
     */
    ServicesAndScenariosEditFormComponent.prototype.parametersAreFilledUp = function () {
        if (this.inputParameters.length > 0 && this.outputParameters.length > 0) {
            var parametersAreFilledUp_1 = true;
            var combinedParametersArray = this.inputParameters.concat(this.outputParameters);
            combinedParametersArray.map(function (item) {
                if (!item.title || item.title.length === 0) {
                    parametersAreFilledUp_1 = false;
                    return false;
                }
            });
            return parametersAreFilledUp_1;
        }
        else {
            return false;
        }
    };
    /**
     * Clears input and output parameters on type change
     *
     * @param event {Object} Event object
     */
    ServicesAndScenariosEditFormComponent.prototype.onTypeChange = function (event) {
        this.inputParameters = [];
        this.outputParameters = [];
        this.type = event.target.value;
    };
    /**
     * Closing the modal
     */
    ServicesAndScenariosEditFormComponent.prototype.ngOnDestroy = function () {
        if (this.modalRef)
            this.modalRef.close();
    };
    /**
     * Fetches and displays parameters for the specific service.
     *
     * @param serviceIdentifier {String} Service identifier
     */
    ServicesAndScenariosEditFormComponent.prototype.fetchServiceParameter = function (serviceIdentifier) {
        var _this = this;
        if (this.servers.length > 0) {
            this.serviceIdentifier = serviceIdentifier;
            var path = this.servers[0].path;
            var requestParameters = ['request=DescribeProcess', 'service=WPS', "identifier=" + serviceIdentifier, "version=1"];
            path += requestParameters.join('&');
            var protocol = '';
            if (this.servers[0].host.indexOf('://') === -1) {
                protocol = 'http://';
            }
            this.api.proxifyRequest('GET', "" + protocol + this.servers[0].host + ":" + this.servers[0].port + path).then(function (data) {
                __WEBPACK_IMPORTED_MODULE_4_xml2js___default.a.parseString(data, function (err, result) {
                    var fillParameters = function (item, container) {
                        container.push({
                            identifier: item['ows:Identifier'][0],
                            title: item['ows:Title'][0],
                            description: '',
                            widget: ''
                        });
                    };
                    var inputParameters = [];
                    result["wps:ProcessDescriptions"].ProcessDescription["0"].DataInputs[0].Input.map(function (item) { return fillParameters(item, inputParameters); });
                    _this.inputParameters = inputParameters;
                    var outputParameters = [];
                    result["wps:ProcessDescriptions"].ProcessDescription["0"].ProcessOutputs[0].Output.map(function (item) { return fillParameters(item, outputParameters); });
                    _this.outputParameters = outputParameters;
                });
            });
        }
        else {
            throw new Error('Unable to fetch services');
        }
    };
    /**
     * Form the service or scenario description object according to the entered data
     *
     * @return {Object}
     */
    ServicesAndScenariosEditFormComponent.prototype.gatherData = function () {
        var serviceDescription = {
            name: this.textualIdentifier,
            description: this.description,
            type: ((this.type === SERVICE_WPS) ? 'wps' : 'js'),
            map_reduce_specification: this.mapReduceSpecification,
            params: [],
            output_params: []
        };
        if (this.type === SERVICE_WPS) {
            serviceDescription['servers'] = this.servers;
            serviceDescription['method'] = this.serviceIdentifier;
        }
        else if (this.type === SCENARIO) {
            serviceDescription['funcbody'] = this.scenarioCode;
        }
        else {
            throw new Error("Unable to detect type");
        }
        this.inputParameters.map(function (item) {
            serviceDescription.params.push({
                fieldname: item.identifier,
                title: item.title,
                description: item.description,
                visible: true,
                widget: {
                    name: "edit",
                    properties: {
                        tsvector: ""
                    }
                }
            });
        });
        this.outputParameters.map(function (item) {
            serviceDescription.output_params.push({
                fieldname: item.identifier,
                title: item.title,
                description: item.description,
                visible: true,
                widget: {
                    name: "edit",
                    properties: {
                        tsvector: ""
                    }
                }
            });
        });
        return serviceDescription;
    };
    /**
     * Registers catalog item
     *
     * @param event {Object} Occured event
     */
    ServicesAndScenariosEditFormComponent.prototype.register = function (event) {
        var _this = this;
        var description = this.gatherData();
        this.api.sendCatalogItem(description).then(function (data) {
            _this.retrievedId = data['id'];
            _this.modalRef = _this.modalService.open(_this.content, { backdrop: 'static' });
        }).catch(function (error) {
            throw error;
        });
    };
    /**
     * Updates catalog item
     *
     * @param event {Object} Occured event
     */
    ServicesAndScenariosEditFormComponent.prototype.update = function (event) {
        var _this = this;
        var description = this.gatherData();
        description['previd'] = this.id;
        this.api.sendCatalogItem(description).then(function (data) {
            _this.modalRef = _this.modalService.open(_this.content);
        }).catch(function (error) {
            throw error;
        });
    };
    /**
     * Fetches list of services for the first available services server.
     */
    ServicesAndScenariosEditFormComponent.prototype.fetchServices = function () {
        var _this = this;
        if (this.servers.length > 0) {
            var path = this.servers[0].path;
            var requestParameters = ['request=GetCapabilities', 'service=WPS'];
            path += requestParameters.join('&');
            var protocol = '';
            if (this.servers[0].host.indexOf('://') === -1) {
                protocol = 'http://';
            }
            this.api.proxifyRequest('GET', "" + protocol + this.servers[0].host + ":" + this.servers[0].port + path).then(function (data) {
                __WEBPACK_IMPORTED_MODULE_4_xml2js___default.a.parseString(data, function (err, result) {
                    var services = [];
                    result['wps:Capabilities']['wps:ProcessOfferings']['0']['wps:Process'].map(function (item) {
                        services.push({
                            title: item['ows:Title'][0],
                            identifier: item['ows:Identifier'][0]
                        });
                    });
                    services.sort(function (a, b) {
                        if (a.title < b.title)
                            return -1;
                        if (a.title > b.title)
                            return 1;
                        return 0;
                    });
                    _this.availableServices = services;
                });
            });
        }
        else {
            throw new Error('Unable to fetch services');
        }
    };
    /**
     * Deletes services server
     *
     * @param event {Object} Event
     * @param index {Number} Index of the deleted services server
     */
    ServicesAndScenariosEditFormComponent.prototype.deleteServicesServer = function (event, index) {
        this.servers.splice(index, 1);
    };
    /**
     * Updating specified parameter
     *
     * @param data {Object} The parameter description
     * @param type {String} The type specifier
     * @param index {Number} The index of the parameter
     */
    ServicesAndScenariosEditFormComponent.prototype.onParameterChange = function (data, type, index) {
        if (type === 'input') {
            this.inputParameters[index] = data;
        }
        else if (type === 'output') {
            this.outputParameters[index] = data;
        }
        else {
            throw new Error("Invalid type was provided");
        }
    };
    /**
     * Deleting specified parameter
     *
     * @param data {Object} The parameter description
     * @param type {String} The type specifier
     * @param index {Number} The index of the parameter
     */
    ServicesAndScenariosEditFormComponent.prototype.onParameterDelete = function (data, type, index) {
        if (type === 'input') {
            this.inputParameters.splice(index, 1);
        }
        else if (type === 'output') {
            this.outputParameters.splice(index, 1);
        }
        else {
            throw new Error("Invalid type was provided");
        }
    };
    /**
     * Adds sample parameter to input or output parameters
     *
     * @param event {Object} Event object
     * @param type {String} The parameter type specifier
     */
    ServicesAndScenariosEditFormComponent.prototype.addSampleParameter = function (event, type) {
        var sampleParameter = {
            identifier: '',
            title: '',
            description: '',
            widget: ''
        };
        if (type === 'input') {
            this.inputParameters.push(sampleParameter);
        }
        else if (type === 'output') {
            this.outputParameters.push(sampleParameter);
        }
        else {
            throw new Error("Ivalid type was provided");
        }
    };
    /**
     * Adds services server
     */
    ServicesAndScenariosEditFormComponent.prototype.addServiceServer = function () {
        var newServer = {
            host: this.host,
            port: this.port,
            path: this.path
        };
        this.host = '';
        this.port = DEFAULT_SERVICES_SERVER_PORT;
        this.path = '';
        this.servers.push(newServer);
    };
    /**
     * Transliterates word (cyryllic to latin)
     *
     * @param word {String} Transliterated word
     */
    ServicesAndScenariosEditFormComponent.prototype.transliterate = function (word) {
        var symbolSet = { "Ё": "YO", "Й": "I", "Ц": "TS", "У": "U", "К": "K", "Е": "E", "Н": "N", "Г": "G", "Ш": "SH",
            "Щ": "SCH", "З": "Z", "Х": "H", "Ъ": "'", "ё": "yo", "й": "i", "ц": "ts", "у": "u", "к": "k", "е": "e", "н": "n",
            "г": "g", "ш": "sh", "щ": "sch", "з": "z", "х": "h", "ъ": "'", "Ф": "F", "Ы": "I", "В": "V", "А": "a", "П": "P", "Р": "R",
            "О": "O", "Л": "L", "Д": "D", "Ж": "ZH", "Э": "E", "ф": "f", "ы": "i", "в": "v", "а": "a", "п": "p", "р": "r", "о": "o",
            "л": "l", "д": "d", "ж": "zh", "э": "e", "Я": "Ya", "Ч": "CH", "С": "S", "М": "M", "И": "I", "Т": "T", "Ь": "'", "Б": "B",
            "Ю": "YU", "я": "ya", "ч": "ch", "с": "s", "м": "m", "и": "i", "т": "t", "ь": "'", "б": "b", "ю": "yu" };
        return word.split('').map(function (char) {
            return (symbolSet[char] || char);
        }).join('');
    };
    /**
     * Generates and updates internal textual identifier
     *
     * @param value {String} Current human-readable name value
     */
    ServicesAndScenariosEditFormComponent.prototype.generateTextualIdentifier = function (value) {
        var identifier = this.transliterate(value).toLowerCase().replace(/[^\w\s]/g, '').replace(/[\s]/g, '_');
        this.textualIdentifier = identifier;
        if (this.scenarioCode.split('\n').length <= 3) {
            this.scenarioCode = "function " + identifier + "(input, mapping) {\n  var abc = 123;\n}";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], ServicesAndScenariosEditFormComponent.prototype, "content", void 0);
    ServicesAndScenariosEditFormComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-services-and-scenarios-edit-form',
            template: __webpack_require__("./src/app/services-and-scenarios-edit-form/services-and-scenarios-edit-form.component.html"),
            styles: [__webpack_require__("./src/app/services-and-scenarios-edit-form/services-and-scenarios-edit-form.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* APIService */], __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]])
    ], ServicesAndScenariosEditFormComponent);
    return ServicesAndScenariosEditFormComponent;
}());



/***/ }),

/***/ "./src/app/services-and-scenarios-run-form/services-and-scenarios-run-form.component.html":
/***/ (function(module, exports) {

module.exports = "<div static-page-layout>\r\n  <form *ngIf=\"catalogItem\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <h3>Run the {{ catalogItem.name }} <button routerLink=\"/services-manager/edit/{{ catalogItem.id }}\" type=\"button\" class=\"btn btn-sm\"><i class=\"fa fa-pencil\"></i> Edit</button></h3>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col\">\r\n          <div class=\"container\">\r\n            <form>\r\n              <div *ngFor=\"let item of catalogItem.inputParameters; let i = index\" class=\"form-group row\">\r\n                <label class=\"col-sm-2 col-form-label label-required\">{{ item.title }}</label>\r\n                <div class=\"col-sm-10\">\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"inputValues[i]\" [readonly]=\"running\" name=\"input_field_{{i}}\" placeholder=\"Enter the {{ item.title }} value\">\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n              <div *ngFor=\"let item of catalogItem.outputParameters; let i = index\" class=\"form-group row\">\r\n                <label class=\"col-sm-2 col-form-label\">{{ item.title }}</label>\r\n                <div class=\"col-sm-10\">\r\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"outputValues[i]\" [readonly]=\"running\" name=\"output_field_{{i}}\" placeholder=\"Enter the {{ item.title }} value\">\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n              <div class=\"form-group row\">\r\n                <div class=\"col-sm-4\">\r\n                  <button type=\"button\" class=\"btn btn-block btn-primary\" (click)=\"run($event)\" [disabled]=\"running || !inputParametersAreReady()\">\r\n                    <i class=\"fa fa-play\"></i> Run\r\n                  </button>\r\n                </div>\r\n                <div class=\"col-sm-4\">\r\n                  <button type=\"button\" class=\"btn btn-block btn-primary\" (click)=\"stop($event)\" [disabled]=\"!running\">\r\n                    <i class=\"fa fa-stop\"></i> Stop\r\n                  </button>\r\n                </div>\r\n                <div class=\"col-sm-4\">\r\n                    <button type=\"button\" class=\"btn btn-block btn-primary\" (click)=\"clear($event)\" [disabled]=\"running\">\r\n                      <i class=\"fa fa-remove\"></i> Clear\r\n                    </button>\r\n                  </div>\r\n              </div>\r\n              <div class=\"form-group row\" *ngIf=\"catalogItemStatus !== 'RUN_STATUS_NULL'\">\r\n                <div class=\"col-sm-12\">\r\n                  <div class=\"alert alert-success\" role=\"alert\" *ngIf=\"catalogItemStatus === 'RUN_STATUS_SUCCEEDED'\" style=\"overflow: scroll;\">\r\n                    {{ catalogItemRunResult }}\r\n                  </div>\r\n                  <div class=\"alert alert-primary\" role=\"alert\" *ngIf=\"catalogItemRunSTDOUT.length > 0\" style=\"overflow: scroll;\">\r\n                    <p *ngFor=\"let item of catalogItemRunSTDOUT\">{{ item.datetime | date:'mediumTime' }}: {{ item.content }} </p>\r\n                  </div>\r\n                  <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"catalogItemStatus === 'RUN_STATUS_FAILED'\" style=\"overflow: scroll;\">\r\n                    <p *ngFor=\"let item of catalogItemRunSTDERR\">{{ item.datetime | date:'mediumTime' }}: {{ item.content }}</p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/services-and-scenarios-run-form/services-and-scenarios-run-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesAndScenariosRunFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var consoleOutputTimeout = 3000;
var ServicesAndScenariosRunFormComponent = /** @class */ (function () {
    function ServicesAndScenariosRunFormComponent(api, route) {
        var _this = this;
        this.api = api;
        this.route = route;
        this.id = -1;
        this.catalogItemRunId = -1;
        this.catalogItemRunSTDOUT = [];
        this.catalogItemRunSTDERR = [];
        this.catalogItemRunResult = false;
        this.catalogItemStatus = 'RUN_STATUS_NULL';
        this.intervalHandle = false;
        this.inputValues = [];
        this.outputValues = [];
        this.running = false;
        route.params.map(function (p) { return p.id; }).subscribe(function (id) {
            if (parseInt(id) > 0) {
                _this.api.getCatalogItem(id).then(function (result) {
                    var serviceData = result['data'];
                    for (var i = 0; i < serviceData.params.length; i++) {
                        _this.inputValues[i] = '';
                    }
                    for (var i = 0; i < serviceData.output_params.length; i++) {
                        _this.outputValues[i] = '';
                    }
                    _this.catalogItem = {
                        id: serviceData.id,
                        name: serviceData.name,
                        description: serviceData.description,
                        inputParameters: serviceData.params,
                        outputParameters: serviceData.output_params
                    };
                    _this.id = id;
                });
            }
        });
    }
    /**
     * Checks if input parameters were entered
     *
     * @return {Boolean}
     */
    ServicesAndScenariosRunFormComponent.prototype.inputParametersAreReady = function () {
        var inputParametersAreFilledUp = true;
        this.inputValues.map(function (item) {
            if (item === '') {
                inputParametersAreFilledUp = false;
                return false;
            }
        });
        return inputParametersAreFilledUp;
    };
    /**
     * Detects and returns new console lines that came from server
     *
     * @param existingLines {Array} Existing console lines
     * @param newLines {String}
     */
    ServicesAndScenariosRunFormComponent.prototype.retrieveNewConsoleLines = function (existingLines, newLines) {
        var newConsoleLines = newLines.split('\n').map(function (item) {
            return {
                datetime: new Date(),
                content: item
            };
        });
        newConsoleLines.splice(0, existingLines.length);
        return newConsoleLines;
    };
    /**
     * Populates the STDOUT console output object
     *
     * @param data {String} Raw console output
     */
    ServicesAndScenariosRunFormComponent.prototype.populateSTDOUT = function (data) {
        var newConsoleLines = this.retrieveNewConsoleLines(this.catalogItemRunSTDOUT, data);
        this.catalogItemRunSTDOUT = this.catalogItemRunSTDOUT.concat(newConsoleLines);
    };
    /**
     * Populates the STDERR console output object
     *
     * @param data {String} Raw console output
     */
    ServicesAndScenariosRunFormComponent.prototype.populateSTDERR = function (data) {
        var newConsoleLines = this.retrieveNewConsoleLines(this.catalogItemRunSTDERR, data);
        this.catalogItemRunSTDERR = this.catalogItemRunSTDERR.concat(newConsoleLines);
    };
    /**
     * Starts the interval polling of the catalog item run's console output
     */
    ServicesAndScenariosRunFormComponent.prototype.startIntervalRequest = function () {
        var _this = this;
        this.intervalHandle = setInterval(function () {
            _this.api.getCatalogItemRunConsoleOutput(_this.catalogItemRunId).then(function (result) {
                console.log(result['data'].lines[0].status);
                switch (result['data'].lines[0].status) {
                    case 'METHOD_EXAMPLE_STARTED':
                    case 'METHOD_EXAMPLE_CREATED':
                        _this.catalogItemStatus = 'RUN_STATUS_CREATED';
                        _this.populateSTDOUT(result['data'].lines[0].console_output);
                        break;
                    case 'METHOD_EXAMPLE_SUCCEEDED':
                        _this.catalogItemStatus = 'RUN_STATUS_SUCCEEDED';
                        _this.catalogItemRunResult = (result['data'].lines[0].result);
                        _this.populateSTDOUT(result['data'].lines[0].console_output);
                        _this.stop();
                        break;
                    case 'METHOD_EXAMPLE_FAILED':
                        _this.catalogItemStatus = 'RUN_STATUS_FAILED';
                        _this.populateSTDOUT(result['data'].lines[0].console_output);
                        _this.populateSTDERR(result['data'].lines[0].error_output);
                        _this.stop();
                        break;
                }
            }, function (error) {
                throw error;
            });
        }, consoleOutputTimeout);
    };
    /**
     * Stops the interval polling of the catalog item run's console output
     */
    ServicesAndScenariosRunFormComponent.prototype.stopIntervalRequest = function () {
        clearInterval(this.intervalHandle);
        this.intervalHandle = false;
    };
    /**
     * Runs the catalog item
     *
     * @param event {Object} Event object
     */
    ServicesAndScenariosRunFormComponent.prototype.run = function (event) {
        var _this = this;
        this.running = true;
        var inputValuesObject = {};
        for (var i = 0; i < this.catalogItem['inputParameters'].length; i++) {
            inputValuesObject[this.catalogItem['inputParameters'][i].fieldname] = this.inputValues[i];
        }
        var outputValuesObject = {};
        for (var i = 0; i < this.catalogItem['outputParameters'].length; i++) {
            outputValuesObject[this.catalogItem['outputParameters'][i].fieldname] = this.outputValues[i];
        }
        this.api.runCatalogItem(this.id, inputValuesObject, outputValuesObject).then(function (result) {
            _this.catalogItemRunId = result['data'];
            _this.startIntervalRequest();
        }).catch(function (error) {
            throw error;
        });
    };
    /**
     * Stops the execution of the catalog item
     *
     * @param event {Object} Event object
     */
    ServicesAndScenariosRunFormComponent.prototype.stop = function (event) {
        this.running = false;
        this.stopIntervalRequest();
    };
    /**
     * Clears the input form
     *
     * @param event {Object} Event object
     */
    ServicesAndScenariosRunFormComponent.prototype.clear = function (event) {
        for (var i = 0; i < this.catalogItem['inputParameters'].length; i++) {
            this.inputValues[i] = '';
        }
        for (var i = 0; i < this.catalogItem['outputParameters'].length; i++) {
            this.outputValues[i] = '';
        }
        this.catalogItemRunSTDOUT = [];
        this.catalogItemRunSTDERR = [];
        this.catalogItemRunResult = '';
        this.catalogItemStatus = 'RUN_STATUS_NULL';
    };
    ServicesAndScenariosRunFormComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-services-and-scenarios-run-form',
            template: __webpack_require__("./src/app/services-and-scenarios-run-form/services-and-scenarios-run-form.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* APIService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]])
    ], ServicesAndScenariosRunFormComponent);
    return ServicesAndScenariosRunFormComponent;
}());



/***/ }),

/***/ "./src/app/services-manager-info/services-manager-info.component.html":
/***/ (function(module, exports) {

module.exports = "<div static-page-layout>\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <h1>Services manager</h1>\r\n        <div></div>\r\n        <div>\r\n          <table class=\"table table-striped\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th scope=\"col\">Name</th>\r\n                <th scope=\"col\">Description</th>\r\n                <th scope=\"col\">Actions</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let item of servicesAndScenarios\">\r\n                <th>{{ item.id }}</th>\r\n                <td>{{ item.name }}</td>\r\n                <td>{{ item.description }}</td>\r\n                <td>\r\n                  <div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">\r\n                    <button title=\"Edit the catalog item\" routerLink=\"/services-manager/edit/{{ item.id }}\" type=\"button\" class=\"btn\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i> Edit</button>\r\n                    <button title=\"Run the catalog item\" routerLink=\"/services-manager/run/{{ item.id }}\" type=\"button\" class=\"btn\"><i class=\"fa fa-play\" aria-hidden=\"true\"></i> Run</button>\r\n                  </div>\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/services-manager-info/services-manager-info.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesManagerInfoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ServicesManagerInfoComponent = /** @class */ (function () {
    function ServicesManagerInfoComponent(api) {
        this.api = api;
        this.servicesAndScenarios = [];
    }
    ServicesManagerInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.list(185, 100, 0).then(function (result) {
            _this.servicesAndScenarios = result['aaData'];
        }).catch(function (error) {
            throw error;
        });
    };
    ServicesManagerInfoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/services-manager-info/services-manager-info.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* APIService */]])
    ], ServicesManagerInfoComponent);
    return ServicesManagerInfoComponent;
}());



/***/ }),

/***/ "./src/app/shared/api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APIService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_config__ = __webpack_require__("./src/app/app.config.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var APIService = /** @class */ (function () {
    function APIService(http, appActions) {
        this.http = http;
        this.appActions = appActions;
    }
    APIService.prototype.checkAuthentication = function () {
        var tokens = localStorage.getItem('geoportal_tokens');
        if (tokens && tokens !== 'null') {
            this.appActions.setUser(JSON.parse(tokens));
        }
    };
    APIService.prototype.login = function (username, password) {
        var _this = this;
        var res = this.http.post(__WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + "/auth/login", {
            username: username,
            password: password
        });
        res.subscribe(function (data) {
            var userData = {
                accessToken: data['accessToken'],
                refreshToken: data['refreshToken']
            };
            localStorage.setItem('geoportal_tokens', JSON.stringify(data));
            _this.appActions.setUser(userData);
        });
        return res;
    };
    APIService.prototype.logout = function () {
        localStorage.setItem('geoportal_tokens', null);
        this.appActions.unsetUser();
    };
    /**
     * Returns metainformation for the specified table.
     *
     * @param dataTableId Table identifier
     */
    APIService.prototype.meta = function (dataTableId) {
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpParams */]().set('f', String(100)).set('f_id', String(dataTableId));
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + "/geoThemes/dataset/list", { params: params }).toPromise().then(function (res) {
            var result = new Promise(function (resolve, reject) {
                if (res['aaData'] && res['aaData'].length === 1) {
                    var s = res['aaData'][0]['JSON'];
                    resolve(JSON.parse(s));
                }
                else {
                    reject('Unable to retrieve the table data');
                }
            });
            return result;
        });
    };
    /**
     * @param dataTableId  table identifier
     * @param limit row count
     * @param offset for pagination
     * @param filter Если не null, то формируем фильтр и запрашиваем данные
     * @see https://www.typescriptlang.org/docs/handbook/declaration-files/do-s-and-don-ts.html
     */
    APIService.prototype.list = function (dataTableId, limit, offset, filters, sorting) {
        var url = __WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + '/geoThemes/dataset/list';
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpParams */]().set('f', String(dataTableId));
        if (filters) {
            filters.forEach(function (filter) {
                params = params.set('f_' + filter.fieldname, filter.valueList.join(';'));
            });
        }
        if (sorting) {
            var strangeSortingArray_1 = [];
            sorting.forEach(function (sortingColumn) {
                strangeSortingArray_1.push({
                    fieldname: sortingColumn.fieldname,
                    dir: sortingColumn.sorting.direction
                });
            });
            var jsonSorting = JSON.stringify(strangeSortingArray_1);
            params = params.set('sort', jsonSorting);
        }
        params = params.set('count_rows', 'true');
        params = params.set('iDisplayStart', String(offset));
        params = params.set('iDisplayLength', String(limit));
        return this.http.get(url, { params: params }).toPromise();
    };
    APIService.prototype.listObservable = function (dataTableId, limit, offset, filters, sort) {
        var url = __WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + '/geoThemes/dataset/list';
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpParams */]().set('f', String(dataTableId));
        if (filters) {
            for (var _i = 0, filters_1 = filters; _i < filters_1.length; _i++) {
                var filter = filters_1[_i];
                params = params.set('f_' + filter['fieldname'], filter['valueList'].join(';'));
            }
        }
        params = params.set('count_rows', 'true');
        if (sort) {
            var sortjson = JSON.stringify(sort);
            params = params.set('sort', sortjson);
        }
        params = params.set('iDisplayStart', String(offset));
        params = params.set('iDisplayLength', String(limit));
        return this.http.get(url, { params: params });
    };
    /**
     * Adding row in dataset
     *
     * @param dataTableId {number} dataset identifier
     *
     * @param document {object} new data
     *
     * @return {Promise}
     */
    APIService.prototype.add = function (datasetId, document) {
        var url = __WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + '/geoThemes/dataset/add?f=' + String(datasetId);
        document.dataset_id = datasetId;
        var documentjson = JSON.stringify(document);
        return this.http
            .post(url, { f: datasetId, document: documentjson })
            .toPromise();
    };
    /**
     * Updating row in dataset
     *
     * @param dataTableId {number} dataset identifier
     *
     * @param document {object} updated data
     *
     * @return {Promise}
     */
    APIService.prototype.update = function (dataTableId, document) {
        var url = __WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + '/geoThemes/dataset/update?f=' + String(dataTableId);
        document.dataset_id = dataTableId;
        document.f_id = document.id;
        var documentjson = JSON.stringify(document);
        return this.http
            .post(url, { f_id: document.id, document: documentjson })
            .toPromise();
    };
    /**
     * Deleting row in dataset
     *
     * @param dataTableId {number} dataset identifier
     *
     * @param document {object} deleted data
     *
     * @return {Promise}
     */
    APIService.prototype.delete = function (dataTableId, document) {
        var url = __WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + '/geoThemes/dataset/delete?f=' + String(dataTableId);
        document.dataset_id = dataTableId;
        document.f_id = document.id;
        var documentjson = JSON.stringify(document);
        return this.http
            .post(url, { f_id: document.id, document: documentjson })
            .toPromise();
    };
    /**
   * Creating dataset
   *
   *
   * @param document {object} deleted data
   *
   * @return {Promise}
   */
    APIService.prototype.createdatatable = function (meta) {
        var url = __WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + '/geoThemes/processtheme';
        //let params = new HttpParams()
        //  .set('f', String(dataTableId));
        var tablejson = JSON.stringify(meta);
        //params = params.set('document', documentjson);
        return this.http
            .post(url, { tablejson: tablejson })
            .toPromise();
    };
    /**
     * Registers or updates the service or scenarion in the catalog
     *
     * @param serviceInformation {Object} Item information
     *
     * @return {Promise}
     */
    APIService.prototype.sendCatalogItem = function (serviceInformation) {
        return this.http
            .post(__WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + "/servicesManager/create", {
            data: JSON.stringify(serviceInformation)
        })
            .toPromise();
    };
    /**
     * Fetches the specified catalog item
     *
     * @param id {Number} Catalog item identifier
     */
    APIService.prototype.getCatalogItem = function (id) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + "/servicesManager/getinfo?id=" + id)
            .toPromise();
    };
    /**
     * Fetches the console output for the specific catalog item run
     *
     * @param runId {Number} Identifier of the catalog item run
     */
    APIService.prototype.getCatalogItemRunConsoleOutput = function (runId) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + "/servicesManager/consoleoutput?id=" + runId)
            .toPromise();
    };
    /**
     * Runs the catalog item with specified parameters
     *
     * @param id               {Number} Identifier of the catalog item
     * @param inputParameters  {Object} Input parameters values
     * @param outputParameters {Object} Output parameters values
     */
    APIService.prototype.runCatalogItem = function (id, inputParameters, outputParameters) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + "/servicesManager/execute?id=" + id + "&inputparams=" + encodeURIComponent(JSON.stringify(inputParameters)) + "&outputparams=" + encodeURIComponent(JSON.stringify(outputParameters)))
            .toPromise();
    };
    /**
     * Proxifies the request to the external server (avoiding CORS issues).
     *
     * @param requestType {String} The request type ('GET')
     * @param url         {String} Requested URL address
     *
     * @return {Promise}
     */
    APIService.prototype.proxifyRequest = function (requestType, url) {
        var completeProxyUrl = __WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + "/servicesManager/proxy?url=" + encodeURIComponent(url);
        if (['GET'].indexOf(requestType) === -1) {
            throw new Error("Invalid parameter");
        }
        return this.http
            .get(completeProxyUrl, { responseType: 'text' })
            .toPromise();
    };
    /**
     * Activates user account
     *
     * @param code {String} Activation code
     */
    APIService.prototype.activateUser = function (code) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_3__app_config__["a" /* AppConfig */].APIPath + "/users/activate?code=" + code).toPromise();
    };
    APIService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__store_app_actions__["a" /* AppActions */]])
    ], APIService);
    return APIService;
}());



/***/ }),

/***/ "./src/app/shared/conversions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export addzero */
/* unused harmony export addsec */
/* unused harmony export Dec2minute_sec */
/* unused harmony export minute_sec2Dec */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return wkt2decimal; });
/* unused harmony export wkt2minute_sec */
/* unused harmony export ToDec */
/* unused harmony export FromDec */
/* unused harmony export translit */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getcorrectobjname; });
var addzero = function (v, cnt) {
    v = v + '';
    while (cnt > v.length) {
        v = '0' + v;
    }
    return v;
};
var addsec = function (v) {
    v = v + '';
    var ps = v.split('.');
    for (var i = ps[0].length; i < 2; i++) {
        v = '0' + v;
    }
    while (5 > v.length) {
        if (v.length == 2)
            v = v + '.';
        else
            v = v + '0';
    }
    return v;
};
var Dec2minute_sec = function (val) {
    if (val < 0)
        val = -val;
    var lon_gr = addzero(Math.floor(val), 3);
    var lon_mn = addzero(Math.floor((val - lon_gr) * 60), 2);
    var lon_sec = addsec(Math.round((((val - lon_gr) * 60 - lon_mn) * 60) * 100) / 100);
    var value = lon_gr + "°" + lon_mn + "'" + lon_sec + "''";
    return value;
};
var minute_sec2Dec = function (val) {
    if (val == undefined || val == '')
        return 0;
    var sign = val.toUpperCase().charAt(0);
    sign = sign == 'E' || sign == 'N';
    val = val.substr(1);
    var arr = val.split(/[\D]/);
    var sec = parseFloat(arr[2] + '.' + arr[3]);
    sec = sec / 3600;
    var mn = arr[1] / 60;
    var g = parseInt(arr[0], 10) + mn + sec;
    if (!sign)
        g = -g;
    return g;
};
var wkt2decimal = function (val) {
    var re = /(MULTI){0,}POINT[\s]{0,1}\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/;
    var arr = re.exec(val);
    var result = { lat: '0', lon: '0' };
    if (arr) {
        if (arr.length === 3) {
            result = { lon: arr[1], lat: arr[2] };
        }
        else if (arr.length === 4) {
            result = { lon: arr[2], lat: arr[3] };
        }
        else {
            throw 'Invalid case';
        }
    }
    return result;
};
var wkt2minute_sec = function (val) {
    var re = /MULTIPOINT\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/;
    var arr = re.exec(val);
    if (arr == null || arr.length < 3)
        return { lat: 0, lon: 0 };
    var lat = Dec2minute_sec(arr[2]);
    var lon = Dec2minute_sec(arr[1]);
    if (parseFloat(arr[2]) < 0)
        lat = 'S' + lat;
    else
        lat = 'N' + lat;
    if (parseFloat(arr[1]) < 0)
        lon = 'W' + lon;
    else
        lon = 'E' + lon;
    return { lat: lat, lon: lon };
};
var ToDec = function (string) {
    var in_arr = string.split(' ');
    var lon_vl = in_arr[1];
    var lon_sign = lon_vl.toUpperCase().charAt(0);
    if (lon_sign == 'E' || lon_sign == 'W') {
        lon_vl = lon_vl.substr(1);
    }
    else
        lon_sign = 'E';
    var lat_vl = in_arr[0];
    var lat_sign = lat_vl.toUpperCase().charAt(0);
    if (lat_sign == 'N' || lat_sign == 'S') {
        lat_vl = lat_vl.substr(1);
    }
    else
        lat_sign = 'N';
    var arr = lon_vl.split(/[\D]/);
    var sec = arr[2] + '.' + arr[3];
    var lon_sec = parseFloat(sec) / 3600;
    var lon_mn = arr[1] / 60;
    var lon = parseInt(arr[0], 10) + lon_mn + lon_sec;
    if (lon_sign != 'E')
        lon = -lon;
    arr = lat_vl.split(/[\D]/);
    var sec = arr[2] + '.' + arr[3];
    var lan_sec = parseFloat(sec) / 3600;
    var lan_mn = arr[1] / 60;
    var lat = parseInt(arr[0], 10) + lan_mn + lan_sec;
    if (lat_sign != 'N')
        lat = -lat;
    var value = lon + " " + lat;
    return value;
};
var FromDec = function (string) {
    var arr;
    if (string == '')
        arr = { 0: '0', 1: '0' };
    else
        arr = string.split(' ');
    var lan_sign = 'N';
    if (arr[1] + 0 < 0)
        lan_sign = 'S';
    var lon_sign = 'E';
    if (arr[0] + 0 < 0)
        lon_sign = 'W';
    var lon_gr = addzero(Math.floor(arr[0]), 3);
    var lon_mn = addzero(Math.floor((arr[0] - lon_gr) * 60), 2);
    var lon_sec = addsec(Math.round((((arr[0] - lon_gr) * 60 - lon_mn) * 60) * 100) / 100);
    var lan_gr = addzero(Math.floor(arr[1]), 3);
    var lan_mn = addzero(Math.floor((arr[1] - lan_gr) * 60), 2);
    var lan_sec = addsec(Math.round((((arr[1] - lan_gr) * 60 - lan_mn) * 60) * 100) / 100);
    var value = lan_sign + lan_gr + "°" + lan_mn + "'" + lan_sec + "''" + " " + lon_sign + lon_gr + "°" + lon_mn + "'" + lon_sec + "''";
    return value;
};
/**
* tranlsit - trnforming cyrillic to latin string
*/
var translit = function (str) {
    if (str === undefined) {
        str = 'notdef';
    }
    ;
    if (str == undefined) {
        str = 'notdef';
    }
    ;
    var transl = new Array();
    transl['А'] = 'A';
    transl['а'] = 'a';
    transl['Б'] = 'B';
    transl['б'] = 'b';
    transl['В'] = 'V';
    transl['в'] = 'v';
    transl['Г'] = 'G';
    transl['г'] = 'g';
    transl['Д'] = 'D';
    transl['д'] = 'd';
    transl['Е'] = 'E';
    transl['е'] = 'e';
    transl['Ё'] = 'Yo';
    transl['ё'] = 'yo';
    transl['Ж'] = 'Zh';
    transl['ж'] = 'zh';
    transl['З'] = 'Z';
    transl['з'] = 'z';
    transl['И'] = 'I';
    transl['и'] = 'i';
    transl['Й'] = 'J';
    transl['й'] = 'j';
    transl['К'] = 'K';
    transl['к'] = 'k';
    transl['Л'] = 'L';
    transl['л'] = 'l';
    transl['М'] = 'M';
    transl['м'] = 'm';
    transl['Н'] = 'N';
    transl['н'] = 'n';
    transl['О'] = 'O';
    transl['о'] = 'o';
    transl['П'] = 'P';
    transl['п'] = 'p';
    transl['Р'] = 'R';
    transl['р'] = 'r';
    transl['С'] = 'S';
    transl['с'] = 's';
    transl['Т'] = 'T';
    transl['т'] = 't';
    transl['У'] = 'U';
    transl['у'] = 'u';
    transl['Ф'] = 'F';
    transl['ф'] = 'f';
    transl['Х'] = 'X';
    transl['х'] = 'x';
    transl['Ц'] = 'C';
    transl['ц'] = 'c';
    transl['Ч'] = 'Ch';
    transl['ч'] = 'ch';
    transl['Ш'] = 'Sh';
    transl['ш'] = 'sh';
    transl['Щ'] = 'Shh';
    transl['щ'] = 'shh';
    transl['Ъ'] = '';
    transl['ъ'] = '';
    transl['Ы'] = 'Y';
    transl['ы'] = 'y';
    transl['Ь'] = '';
    transl['ь'] = '';
    transl['Э'] = 'E';
    transl['э'] = 'e';
    transl['Ю'] = 'Yu';
    transl['ю'] = 'yu';
    transl['Я'] = 'Ya';
    transl['я'] = 'ya';
    transl['_'] = '_';
    transl[' '] = '_';
    var result = '';
    for (var i = 0; i < str.length; i++) {
        if (transl[str[i]] != undefined) {
            result += transl[str[i]];
        }
        else {
            result += str[i];
        }
    }
    return result;
};
/**
*	It is an util function for obtaining correct object name
*
*	@param name - input name to make correct
*	@return correct name
*/
var getcorrectobjname = function (name) {
    name = name.toLowerCase();
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    var digits = '0123456789';
    var res = '';
    name = translit(name);
    for (var i = 0; i < name.length; i++) {
        if (res.length == 0 && digits.indexOf(name.charAt(i)) != -1)
            continue;
        if (chars.indexOf(name.charAt(i)) != -1)
            res = res + name.charAt(i);
        if (res.length > 15)
            break;
    }
    return res;
};


/***/ }),

/***/ "./src/app/shared/interfaces.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ModalResult; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SignOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogicElements; });
var ModalResult;
(function (ModalResult) {
    ModalResult[ModalResult["mrNone"] = 0] = "mrNone";
    ModalResult[ModalResult["mrOk"] = 1] = "mrOk";
    ModalResult[ModalResult["mrCancel"] = 2] = "mrCancel";
    ModalResult[ModalResult["mrAbort"] = 3] = "mrAbort";
    ModalResult[ModalResult["mrRetry"] = 4] = "mrRetry";
    ModalResult[ModalResult["mrIgnore"] = 5] = "mrIgnore";
    ModalResult[ModalResult["mrYes"] = 6] = "mrYes";
    ModalResult[ModalResult["mrNo"] = 7] = "mrNo";
})(ModalResult || (ModalResult = {}));
var SignOptions;
(function (SignOptions) {
    SignOptions["Equals"] = "==";
    SignOptions["NotEqualTo"] = "!=";
    SignOptions["GreaterThan"] = ">";
    SignOptions["LessThan"] = "<";
    SignOptions["GreaterThanOrEqualTo"] = ">=";
    SignOptions["LessThanOrEqualTo"] = "<=";
})(SignOptions || (SignOptions = {}));
var LogicElements;
(function (LogicElements) {
    LogicElements["Empty"] = "EMPTY";
    LogicElements["And"] = "AND";
    LogicElements["Or"] = "OR";
    LogicElements["Not"] = "NOT";
})(LogicElements || (LogicElements = {}));


/***/ }),

/***/ "./src/app/static-page-layout/static-page-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"layout__menu\" main-menu></div>\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col\">\r\n      <ng-content></ng-content>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/static-page-layout/static-page-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StaticPageLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StaticPageLayoutComponent = /** @class */ (function () {
    function StaticPageLayoutComponent() {
    }
    StaticPageLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: '[static-page-layout]',
            template: __webpack_require__("./src/app/static-page-layout/static-page-layout.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], StaticPageLayoutComponent);
    return StaticPageLayoutComponent;
}());



/***/ }),

/***/ "./src/app/store/app.actions.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppActions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_createAction__ = __webpack_require__("./src/app/createAction.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppActions = /** @class */ (function () {
    function AppActions(store) {
        this.store = store;
    }
    AppActions_1 = AppActions;
    AppActions.prototype.setFrontPageMapHost = function (data) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_FRONT_PAGE_MAP_HOST, data));
    };
    AppActions.prototype.setFrontPageMapRef = function (data) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_FRONT_PAGE_MAP_REF, data));
    };
    AppActions.prototype.enableDrawingForMap = function (data) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.ENABLE_DRAWING_FOR_MAP, data));
    };
    AppActions.prototype.cancelDrawing = function (data) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.CANCEL_DRAWING, data));
    };
    AppActions.prototype.setLastPointForMap = function (data) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_LAST_POINT_FOR_MAP, data));
    };
    AppActions.prototype.setLastLineForMap = function (data) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_LAST_LINE_FOR_MAP, data));
    };
    AppActions.prototype.setLastPolygonForMap = function (data) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_LAST_POLYGON_FOR_MAP, data));
    };
    AppActions.prototype.addMap = function (map) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.ADD_MAP, map));
    };
    AppActions.prototype.setUser = function (tokens) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_USER, tokens));
    };
    AppActions.prototype.unsetUser = function () {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.UNSET_USER));
    };
    AppActions.prototype.showTable = function (datasetId, context) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SHOW_TABLE, {
            datasetId: datasetId,
            context: context
        }));
    };
    AppActions.prototype.setTableFilter = function (datasetId, filter) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_FILTER_FOR_TABLE, {
            datasetId: datasetId,
            filter: filter
        }));
    };
    AppActions.prototype.setSortingForTableColumn = function (datasetId, sorting) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_SORTING_DIRECTION_FOR_TABLE_COLUMN, {
            datasetId: datasetId,
            sorting: sorting
        }));
    };
    AppActions.prototype.setMetadataForTable = function (datasetId, metadata) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_METADATA_FOR_TABLE, {
            datasetId: datasetId,
            metadata: metadata
        }));
    };
    AppActions.prototype.setPaginatorPageForTable = function (datasetId, page) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_PAGE_FOR_TABLE, {
            datasetId: datasetId,
            page: page
        }));
    };
    AppActions.prototype.setPaginatorPageSizeForTable = function (datasetId, pageSize) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_PAGE_SIZE_FOR_TABLE, {
            datasetId: datasetId,
            pageSize: pageSize
        }));
    };
    AppActions.prototype.setCollectionSizeForTable = function (datasetId, collectionSize) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_COLLECTION_SIZE_FOR_TABLE, {
            datasetId: datasetId,
            collectionSize: collectionSize
        }));
    };
    AppActions.prototype.setFilteringBarVisibilty = function (datasetId, visibility) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_TABLE_FILTERING_BAR_VISIBILITY, {
            datasetId: datasetId,
            visibility: visibility
        }));
    };
    AppActions.prototype.setAggregationBarVisibilty = function (datasetId, visibility) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.SET_TABLE_AGGREGATION_BAR_VISIBILITY, {
            datasetId: datasetId,
            visibility: visibility
        }));
    };
    AppActions.prototype.updateTableData = function (datasetId) {
        this.store.dispatch(Object(__WEBPACK_IMPORTED_MODULE_2_app_createAction__["a" /* createAction */])(AppActions_1.TABLE_DATA_UPDATED, datasetId));
    };
    AppActions.SET_FRONT_PAGE_MAP_REF = 'SET_FRONT_PAGE_MAP_REF';
    AppActions.SET_FRONT_PAGE_MAP_HOST = 'SET_FRONT_PAGE_MAP_HOST';
    AppActions.SET_USER = 'SET_USER';
    AppActions.UNSET_USER = 'UNSET_USER';
    AppActions.SHOW_TABLE = 'SHOW_TABLE';
    AppActions.ADD_MAP = 'ADD_MAP';
    AppActions.ENABLE_DRAWING_FOR_MAP = 'ENABLE_DRAWING_FOR_MAP';
    AppActions.CANCEL_DRAWING = 'CANCEL_DRAWING';
    AppActions.SET_LAST_POINT_FOR_MAP = 'SET_LAST_POINT_FOR_MAP';
    AppActions.SET_LAST_LINE_FOR_MAP = 'SET_LAST_LINE_FOR_MAP';
    AppActions.SET_LAST_POLYGON_FOR_MAP = 'SET_LAST_POLYGON_FOR_MAP';
    AppActions.TABLE_DATA_UPDATED = '[Tables] Table Data Updated';
    AppActions.SET_FILTER_FOR_TABLE = '[Tables] Set Filter For Table';
    AppActions.CLEAR_TABLE_FILTERS = '[Tables] Clear Table Filters'; // Not implemented yet
    AppActions.SET_METADATA_FOR_TABLE = '[Tables] Set Metadata For Table';
    AppActions.SET_PAGE_FOR_TABLE = '[Tables] Set Page For Table';
    AppActions.SET_PAGE_SIZE_FOR_TABLE = '[Tables] Set Page Size For Table';
    AppActions.SET_COLLECTION_SIZE_FOR_TABLE = '[Tables]  Set Collection Size For Table';
    AppActions.SET_TABLE_FILTERING_BAR_VISIBILITY = '[Tables] Set Visibilty of Table Filtering Bar';
    AppActions.SET_TABLE_AGGREGATION_BAR_VISIBILITY = '[Tables] Set Visibilty of Table Aggregation Bar';
    AppActions.SET_SORTING_DIRECTION_FOR_TABLE_COLUMN = '[Tables] Set Sorting Direction For Table Column';
    AppActions = AppActions_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], AppActions);
    return AppActions;
    var AppActions_1;
}());



/***/ }),

/***/ "./src/app/store/app.reducer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = appReducer;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return stateGetter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_state__ = __webpack_require__("./src/app/store/app.state.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



var initialState = {
    user: false,
    frontPageMap: {
        ref: false,
        host: false
    },
    tables: [],
    filters: []
};
function appReducer(state, action) {
    if (state === void 0) { state = initialState; }
    console.warn('REDUCER SAY: ', action);
    var newMap = {};
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_FRONT_PAGE_MAP_HOST:
            return Object.assign({}, state, {
                frontPageMap: {
                    ref: state.frontPageMap.ref,
                    host: action.payload.host
                }
            });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_FRONT_PAGE_MAP_REF:
            return Object.assign({}, state, {
                frontPageMap: {
                    ref: action.payload.ref,
                    host: state.frontPageMap.host
                }
            });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_LAST_POINT_FOR_MAP:
            newMap[action.payload.mapId] = {
                drawingIsEnabled: false,
                lastPoint: action.payload.wkt
            };
            return Object.assign({}, state, newMap);
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_LAST_LINE_FOR_MAP:
            newMap[action.payload.mapId] = {
                drawingIsEnabled: false,
                lastLine: action.payload.wkt
            };
            return Object.assign({}, state, newMap);
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_LAST_POLYGON_FOR_MAP:
            newMap[action.payload.mapId] = {
                drawingIsEnabled: false,
                lastPolygon: action.payload.wkt
            };
            return Object.assign({}, state, newMap);
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].CANCEL_DRAWING:
            newMap[action.payload.mapId] = { drawingIsEnabled: false };
            return Object.assign({}, state, newMap);
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].ENABLE_DRAWING_FOR_MAP:
            newMap[action.payload.mapId] = {
                drawingIsEnabled: true,
                drawingMode: action.payload.mode,
                existingFeature: action.payload.existingFeature
                    ? action.payload.existingFeature
                    : false,
                lastPoint: false
            };
            return Object.assign({}, state, newMap);
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].ADD_MAP:
            if (state[action.payload.mapId]) {
                throw Error("Map with specified id already exists");
            }
            newMap[action.payload.mapId] = { drawingIsEnabled: false };
            return Object.assign({}, state, newMap);
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_USER:
            var splittedToken = action.payload.accessToken.split('.');
            var user = JSON.parse(atob(splittedToken[1]));
            return Object.assign({}, state, {
                user: {
                    id: user.id,
                    name: user.username,
                    tokens: action.payload
                }
            });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].UNSET_USER:
            return Object.assign({}, state, { user: false });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SHOW_TABLE:
            return __assign({}, state, { tables: __assign({}, state.tables, (_a = {}, _a[action.payload.datasetId] = {
                    dataset_id: action.payload.datasetId,
                    sort: [],
                    lastUpdated: Date.now(),
                    context: __WEBPACK_IMPORTED_MODULE_1__app_state__["a" /* TableContext */].TableList
                }, _a)) });
        // следует добавить action ДОБАВИТЬ_ТАБЛИЦУ_К_СПИСКУ_ТАБЛИЦ
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_METADATA_FOR_TABLE:
            var newContext = state.tables[action.payload.datasetId]
                ? state.tables[action.payload.datasetId].context
                : __WEBPACK_IMPORTED_MODULE_1__app_state__["a" /* TableContext */].ServiceTables;
            return __assign({}, state, { tables: __assign({}, state.tables, (_b = {}, _b[action.payload.datasetId] = __assign({}, state.tables[action.payload.datasetId], { dataset_id: action.payload.datasetId, meta: action.payload.metadata, lastUpdated: Date.now(), context: newContext }), _b)) });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_PAGE_FOR_TABLE:
            return __assign({}, state, { tables: __assign({}, state.tables, (_c = {}, _c[action.payload.datasetId] = __assign({}, state.tables[action.payload.datasetId], { page: action.payload.page, lastUpdated: Date.now() }), _c)) });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_PAGE_SIZE_FOR_TABLE:
            return __assign({}, state, { tables: __assign({}, state.tables, (_d = {}, _d[action.payload.datasetId] = __assign({}, state.tables[action.payload.datasetId], { pageSize: action.payload.pageSize, lastUpdated: Date.now() }), _d)) });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_COLLECTION_SIZE_FOR_TABLE:
            return __assign({}, state, { tables: __assign({}, state.tables, (_e = {}, _e[action.payload.datasetId] = __assign({}, state.tables[action.payload.datasetId], { collectionSize: action.payload.collectionSize, lastUpdated: Date.now() }), _e)) });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_TABLE_FILTERING_BAR_VISIBILITY:
            return __assign({}, state, { tables: __assign({}, state.tables, (_f = {}, _f[action.payload.datasetId] = __assign({}, state.tables[action.payload.datasetId], { showFilteringBar: action.payload.visibility, lastUpdated: Date.now() }), _f)) });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_TABLE_AGGREGATION_BAR_VISIBILITY:
            return __assign({}, state, { tables: __assign({}, state.tables, (_g = {}, _g[action.payload.datasetId] = __assign({}, state.tables[action.payload.datasetId], { showAggregationBar: action.payload.visibility, lastUpdated: Date.now() }), _g)) });
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_FILTER_FOR_TABLE: {
            var choosedColumn_1 = __assign({}, Object(__WEBPACK_IMPORTED_MODULE_2_lodash__["find"])(state.tables[action.payload.datasetId].meta.columns, { 'fieldname': action.payload.filter.fieldname }));
            choosedColumn_1.filterValueList = action.payload.filter.valueList;
            var newColumns = state.tables[action.payload.datasetId].meta.columns.map(function (column) {
                if (column.fieldname === action.payload.filter.fieldname) {
                    return choosedColumn_1;
                }
                return column;
            });
            return __assign({}, state, { tables: __assign({}, state.tables, (_h = {}, _h[action.payload.datasetId] = __assign({}, state.tables[action.payload.datasetId], { meta: __assign({}, state.tables[action.payload.datasetId].meta, { columns: newColumns }), lastUpdated: Date.now() }), _h)) });
        }
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].SET_SORTING_DIRECTION_FOR_TABLE_COLUMN: {
            var choosedColumn_2 = __assign({}, Object(__WEBPACK_IMPORTED_MODULE_2_lodash__["find"])(state.tables[action.payload.datasetId].meta.columns, { 'fieldname': action.payload.sorting.fieldname }));
            choosedColumn_2.sortingOrder = action.payload.sorting.sorting;
            var newColumns = state.tables[action.payload.datasetId].meta.columns.map(function (column) {
                if (column.fieldname === action.payload.sorting.fieldname) {
                    return choosedColumn_2;
                }
                return column;
            });
            return __assign({}, state, { tables: __assign({}, state.tables, (_j = {}, _j[action.payload.datasetId] = __assign({}, state.tables[action.payload.datasetId], { meta: __assign({}, state.tables[action.payload.datasetId].meta, { columns: newColumns }), lastUpdated: Date.now() }), _j)) });
        }
        case __WEBPACK_IMPORTED_MODULE_0__app_actions__["a" /* AppActions */].TABLE_DATA_UPDATED:
            return __assign({}, state, { tables: __assign({}, state.tables, (_k = {}, _k[action.payload] = __assign({}, state.tables[action.payload], { lastUpdated: Date.now() }), _k)) });
        default:
            console.warn("Unprocessed action " + action.type);
            return state;
    }
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
}
var getUnderlyingProperty = function (currentStateLevel, properties) {
    if (properties.length === 0) {
        throw Error('Unable to get the underlying property');
    }
    else if (properties.length === 1) {
        var key = properties.shift();
        return currentStateLevel[key];
    }
    else {
        var key = properties.shift();
        return getUnderlyingProperty(currentStateLevel[key], properties);
    }
};
var stateGetter = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    return function (state) {
        var argsCopy = args.slice();
        return getUnderlyingProperty(state['state'], argsCopy);
    };
};


/***/ }),

/***/ "./src/app/store/app.state.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableContext; });
var TableContext;
(function (TableContext) {
    TableContext[TableContext["TableList"] = 0] = "TableList";
    TableContext[TableContext["ServiceTables"] = 1] = "ServiceTables";
})(TableContext || (TableContext = {}));


/***/ }),

/***/ "./src/app/table-dlg/table-dlg.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title\">Table list </h4>\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <form>     \r\n    <w-table [mode]=\"'forminput'\" (widgetValueChange)=\"changecurentval($event)\" ></w-table>  \r\n  </form>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-secondary\" (click)=\"activeModal.close('Close click')\">Close</button>\r\n  <button type=\"button\" class=\"btn btn-primary\" (click)=\"docSubmit()\">Open table</button>\r\n  <!-- <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"yesAnswer()\">Yes</button>\r\n  <button type=\"button\" class=\"btn btn-outline-dark\" (click)=\"noAnswer()\">No</button>   -->\r\n</div>"

/***/ }),

/***/ "./src/app/table-dlg/table-dlg.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/table-dlg/table-dlg.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableDlgComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_shared_interfaces__ = __webpack_require__("./src/app/shared/interfaces.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TableDlgComponent = /** @class */ (function () {
    function TableDlgComponent(activeModal) {
        this.activeModal = activeModal;
    }
    TableDlgComponent.prototype.ngOnInit = function () { };
    TableDlgComponent.prototype.docSubmit = function () {
        this.activeModal.close({ choosed: __WEBPACK_IMPORTED_MODULE_2_app_shared_interfaces__["b" /* ModalResult */].mrOk, dataset_id: this.currentTableId });
    };
    TableDlgComponent.prototype.changecurentval = function (tableId) {
        this.currentTableId = Number(tableId);
    };
    TableDlgComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-table-dlg',
            template: __webpack_require__("./src/app/table-dlg/table-dlg.component.html"),
            styles: [__webpack_require__("./src/app/table-dlg/table-dlg.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbActiveModal */]])
    ], TableDlgComponent);
    return TableDlgComponent;
}());



/***/ }),

/***/ "./src/app/table-model/table-model.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title\">Table model </h4>\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <form>\r\n    <span [ngClass]=\"{'trailactive': step==1}\">Initial</span> &gt;&gt;\r\n    <span [ngClass]=\"{'trailactive': step==2}\">General</span> &gt;&gt;\r\n    <span [ngClass]=\"{'trailactive': step==3}\">Columns</span> &gt;&gt;\r\n    <span [ngClass]=\"{'trailactive': step==4}\">Rights</span>\r\n\r\n    <div class=\"alert alert-danger\" *ngIf=\"errorMsg!=''\">{{errorMsg}}</div>\r\n    <ng-container [ngSwitch]=\"step\">\r\n      <ng-container *ngSwitchCase=\"'1'\">\r\n        <div class=\"form-group\">\r\n          <label>\r\n            <input name=\"newtable\" type=\"radio\" [(ngModel)]=\"newtable\" [value]=\"true\"> New table\r\n          </label>\r\n          <br/>\r\n          <label>\r\n            <input name=\"newtable\" type=\"radio\" [(ngModel)]=\"newtable\" [value]=\"false\"> Existing table\r\n          </label>\r\n        </div>\r\n        <div class=\"alert alert-danger\" *ngIf=\"tryselectingtable && !this.newtable && !this.selectedtable\">You must select a table.</div>\r\n        \r\n        <w-table *ngIf=\"!newtable\" [mode]=\"'forminput'\" (widgetValueChange)=\"changetab($event)\"></w-table>\r\n      </ng-container>\r\n      <ng-container *ngSwitchCase=\"'2'\">\r\n        <div class=\"form-group\">\r\n            <div class=\"alert alert-danger\" *ngIf=\"validating && (!meta.title || !meta.title.length>0)\">You must specify Table name.</div>\r\n          <label for=\"tablename\">Table name</label>\r\n          <input type=\"text\" class=\"form-control\" name=\"tablename\" [(ngModel)]=\"meta.title\" required minlength=\"5\" maxlength=\"30\"/>\r\n        </div>\r\n        <div class=\"form-group\">\r\n          <label for=\"description\">Description</label>\r\n          <textarea type=\"text\" class=\"form-control\" name=\"description\" [(ngModel)]=\"meta.description\" rows=\"3\" cols=\"100\"></textarea>\r\n        </div>\r\n      </ng-container>\r\n      <div *ngSwitchCase=\"'3'\">\r\n        <div class=\"alert alert-danger\" *ngIf=\"validating && (!meta.columns || !meta.columns.length>0)\">You must add one or more columns.</div>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"addcolumn()\">\r\n          <i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>\r\n        </button>\r\n        <ul class=\"geoportal__table table table-striped table-sm table-hover table-responsive\">\r\n\r\n          <ng-container *ngFor=\"let column of meta.columns\">\r\n            <li class=\"geoportal-table__header\" *ngIf=\"column.title\">\r\n              <div class=\"row\">\r\n                <div class=\"col-4\">\r\n                  <a href=\"#\" *ngIf=\"!column.edit\" (click)=\"editcolumn(column)\">{{ column.title }} </a>\r\n                  <input *ngIf=\"column.edit\" type=\"text\" class=\"form-control\" name=\"column.title\" value=\"{{column.title}}\" (blur)=\"savetitlecolumn($event)\"\r\n                    (change)=\"savetitlecolumn($event)\" />\r\n                </div>\r\n                <div class=\"col-4\">\r\n                  <label for=\"widgetSelect\"> Widget:</label>\r\n                  <select name=\"widgetSelect\" (change)=\"savewidgetcolumn($event, column )\">\r\n                    <ng-container *ngFor=\"let w of widgets\">\r\n                      <option *ngIf=\"w.id==column.widget.name\" selected value=\"w.id\"> {{w.name}} </option>\r\n                      <option *ngIf=\"w.id!=column.widget.name\" [value]=\"w.id\"> {{w.name}} </option>\r\n                    </ng-container>\r\n                  </select>\r\n                  <i class=\"fa fa-asterisk\" aria-hidden=\"true\" (click)=\"detail(column)\"></i>\r\n                </div>\r\n                <div class=\"col-2\">\r\n                  <button type=\"button\" class=\"btn btn-danger\" (click)=\"deletecolumn(column)\">\r\n                    <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\r\n                  </button>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"column.propertyediting\" class=\"row detail\">\r\n                <div class=\"form-group\">\r\n                  <label>\r\n                      <input type=\"checkbox\" name=\"required\" [(ngModel)]=\"column.required\">\r\n                      Is required\r\n                  </label>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label>\r\n                        <input type=\"checkbox\" name=\"required\" [(ngModel)]=\"column.visible\">\r\n                        Is visible\r\n                    </label>\r\n                  </div>\r\n                  <widget [attr]=\"column\" [mode]=\"'properites'\"></widget>\r\n              </div>\r\n            </li>\r\n          </ng-container>\r\n        </ul>\r\n      </div>\r\n      <div *ngSwitchCase=\"'4'\">\r\n          <div *ngFor=\"let rightholder of meta.userlist\">\r\n              userid: {{rightholder.userid}} <br/>accesstype: {{rightholder.accesstype}}\r\n          </div>\r\n      </div>\r\n    </ng-container>\r\n  </form>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button *ngIf=\"step>1\" type=\"button\" class=\"btn btn-primary\" (click)=\"prev()\">Prev</button>\r\n  <button type=\"button\" class=\"btn btn-primary\" (click)=\"next()\">Next</button>\r\n  <button type=\"button\" class=\"btn btn-secondary\" (click)=\"activeModal.close('Close click')\">Close</button>\r\n  <button type=\"submit\" class=\"btn btn-secondary\" (click)=\"modelSubmit()\" >Save model</button>\r\n</div>"

/***/ }),

/***/ "./src/app/table-model/table-model.component.scss":
/***/ (function(module, exports) {

module.exports = ".detail {\n  display: block; }\n\n.trailactive {\n  color: #007bff; }\n"

/***/ }),

/***/ "./src/app/table-model/table-model.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableModelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_shared_interfaces__ = __webpack_require__("./src/app/shared/interfaces.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_shared_conversions__ = __webpack_require__("./src/app/shared/conversions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_modal_confirmation_modal_confirmation_component__ = __webpack_require__("./src/app/modal-confirmation/modal-confirmation.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TableModelComponent = /** @class */ (function () {
    function TableModelComponent(activeModal, appActions, datatableService, modalService) {
        this.activeModal = activeModal;
        this.appActions = appActions;
        this.datatableService = datatableService;
        this.modalService = modalService;
        this.step = 1;
        this.newtable = true;
        this.selectedtable = false;
        this.tryselectingtable = false;
        this.validating = false;
        this.meta = {};
        this.errorMsg = '';
        this.widgets = [
            { id: 'edit', name: 'edit' },
            { id: 'number', name: 'number' },
            { id: 'polygon', name: 'polygon' },
            { id: 'line', name: 'line' },
            { id: 'select', name: 'select' },
            { id: 'classify', name: 'classify' }
        ];
    }
    TableModelComponent.prototype.ngOnInit = function () { };
    TableModelComponent.prototype.modelSubmit = function () {
        var _this = this;
        this.validating = true;
        //this.appActions.showTable({ dataset_id: this.dataset_id });
        if (!this.meta.title) {
            this.step = 2;
            return;
        }
        if (!this.meta.columns || this.meta.columns.length == 0) {
            this.step = 3;
            return;
        }
        if (!this.meta.tablename || this.meta.tablename == '')
            this.meta.tablename = Object(__WEBPACK_IMPORTED_MODULE_5_app_shared_conversions__["a" /* getcorrectobjname */])(this.meta.title);
        this.meta.columns.forEach(function (element) {
            if (!element.fieldname || element.fieldname == '')
                element.fieldname = Object(__WEBPACK_IMPORTED_MODULE_5_app_shared_conversions__["a" /* getcorrectobjname */])(element.title);
            if (!element.sqlname || element.sqlname == '')
                element.sqlname = element.fieldname;
        });
        var documentjson = JSON.stringify(this.meta);
        this.datatableService.createdatatable(this.meta).then(function (res) {
            console.log(res);
            _this.activeModal.close('Save click');
            //this.appActions.updateTable( this.meta.dataset_id );
        }).catch(function (msg) {
            return _this.errorMsg = msg.error.error;
        });
        //alert(documentjson);
        console.log(this.dataset_id);
    };
    TableModelComponent.prototype.changetab = function (dataset_id) {
        var _this = this;
        this.dataset_id = dataset_id;
        this.selectedtable = true;
        this.datatableService.meta(this.dataset_id).then(function (serviceMeta) {
            _this.meta = serviceMeta;
            _this.meta.columns.forEach(function (element) {
                if (!element.widget)
                    element.widget = { name: '' };
                if (!element.widget.properties)
                    element.widget.properties = {};
            });
        });
    };
    TableModelComponent.prototype.next = function () {
        if (this.step == 1 && !this.newtable && !this.selectedtable) {
            this.tryselectingtable = true;
            return;
        }
        else if (this.step == 1 && this.newtable && this.meta.dataset_id) {
            this.meta = {};
        }
        this.step++;
    };
    TableModelComponent.prototype.prev = function () {
        this.step--;
    };
    TableModelComponent.prototype.addcolumn = function () {
        var column = {
            title: 'new column',
            visible: true,
            fieldname: '',
            tablename: '',
            sqltablename: '',
            sqlname: '',
            widget: { name: 'edit', properties: {} }
        };
        if (!this.meta.columns)
            this.meta.columns = [];
        this.meta.columns.push(column);
    };
    TableModelComponent.prototype.editcolumn = function (column) {
        column.edit = true;
        console.log('ok');
        this.cur_column = column;
    };
    TableModelComponent.prototype.savetitlecolumn = function ($event) {
        this.cur_column.edit = false;
        this.cur_column.title = $event.target.value;
    };
    TableModelComponent.prototype.savewidgetcolumn = function ($event, column) {
        column.widget.name = $event.target.value;
    };
    TableModelComponent.prototype.openConfirmationModalForm = function (title, body) {
        if (title === void 0) { title = 'Geoportal'; }
        var modalRef = this.modalService.open(__WEBPACK_IMPORTED_MODULE_6_app_modal_confirmation_modal_confirmation_component__["a" /* ModalConfirmationComponent */]);
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.body = body;
        return modalRef.result;
    };
    TableModelComponent.prototype.deletecolumn = function (column) {
        var _this = this;
        this.openConfirmationModalForm('Deleting column', 'Are you sure you want to delete this column?').then(function (result) {
            if (result === __WEBPACK_IMPORTED_MODULE_3_app_shared_interfaces__["b" /* ModalResult */].mrYes) {
                var ind = _this.meta.columns.indexOf(column);
                if (ind != -1)
                    _this.meta.columns.splice(ind, 1);
            }
        });
    };
    TableModelComponent.prototype.detail = function (column) {
        if (column.propertyediting)
            column.propertyediting = false;
        else
            column.propertyediting = true;
    };
    TableModelComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-table-model',
            template: __webpack_require__("./src/app/table-model/table-model.component.html"),
            styles: [__webpack_require__("./src/app/table-model/table-model.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* NgbActiveModal */],
            __WEBPACK_IMPORTED_MODULE_2__store_app_actions__["a" /* AppActions */],
            __WEBPACK_IMPORTED_MODULE_4_app_shared_api_service__["a" /* APIService */],
            __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]])
    ], TableModelComponent);
    return TableModelComponent;
}());



/***/ }),

/***/ "./src/app/tablelist/tablelist.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-header\">  \r\n    <div class=\"btn-toolbar justify-content-end\">\r\n        <div class=\"btn-group mr-2\">\r\n            <button type=\"button\" class=\"btn\" (click)=\"opentab()\">\r\n                <i class=\"fa fa-folder-open-o\" aria-hidden=\"true\"></i>\r\n                Open\r\n            </button>\r\n            <button type=\"button\" class=\"btn\">\r\n                <i class=\"fa fa-arrow-down\" aria-hidden=\"true\"></i>\r\n                Load\r\n            </button>\r\n            <button type=\"button\" class=\"btn\">\r\n                <i class=\"fa fa-print\" aria-hidden=\"true\"></i>\r\n                Print\r\n            </button>\r\n            <button type=\"button\" class=\"btn\" (click)=\"openmodel()\">\r\n                <i class=\"fa fa-list\" aria-hidden=\"true\"></i>\r\n                Structure\r\n            </button>\r\n            <button type=\"button\" class=\"btn\">\r\n                CSV/PDF\r\n            </button>\r\n        </div>\r\n    \r\n        <div class=\"btn-group btn-group-sm\">\r\n            <button type=\"button\" class=\"btn \">\r\n                <i class=\"fa fa-window-minimize\" aria-hidden=\"true\"></i>\r\n            </button>            \r\n            <button type=\"button\" class=\"btn \">\r\n                <i class=\"fa fa-window-restore\" aria-hidden=\"true\"></i>\r\n            </button>            \r\n            <button type=\"button\" class=\"btn \">\r\n                <i class=\"fa fa-window-maximize \" aria-hidden=\"true\"></i>\r\n            </button>            \r\n        </div>\r\n    </div>        \r\n</div>\r\n\r\n<ngb-tabset (tabChange)=\"onTabChange($event)\" [destroyOnHide]=\"false\">       \r\n  <!-- <ng-container *ngFor=\"let table of tableList$|async|values\">       -->\r\n  <!-- <ng-container *ngFor=\"let table of datasetList|values\">       -->\r\n  <ng-container *ngFor=\"let datasetId of datasetList\">      \r\n        <!-- <ngb-tab title=\"Dynamic Table {{table.dataset_id}}\" id=\"tab-datagrid-{{table.dataset_id}}\"> -->\r\n        <ngb-tab title=\"Dynamic Table {{datasetId}}\" id=\"tab-datagrid-{{datasetId}}\">\r\n            <ng-template ngbTabContent>\r\n                <div class=\"content-table\">\r\n                    <!-- <datatable [dataSetId]=\"table.dataset_id\"></datatable> -->\r\n                    <datatable [datasetId]=\"datasetId\"></datatable>\r\n                </div>\r\n            </ng-template>\r\n        </ngb-tab>\r\n    </ng-container>       \r\n</ngb-tabset>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/tablelist/tablelist.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/tablelist/tablelist.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TablelistComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_filter__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_pipes__ = __webpack_require__("./node_modules/ngx-pipes/ngx-pipes.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__store_app_state__ = __webpack_require__("./src/app/store/app.state.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_app_table_dlg_table_dlg_component__ = __webpack_require__("./src/app/table-dlg/table-dlg.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_app_table_model_table_model_component__ = __webpack_require__("./src/app/table-model/table-model.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_app_shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_app_shared_interfaces__ = __webpack_require__("./src/app/shared/interfaces.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var TablelistComponent = /** @class */ (function () {
    function TablelistComponent(store, appActions, datatableService, modalService) {
        this.store = store;
        this.appActions = appActions;
        this.datatableService = datatableService;
        this.modalService = modalService;
        // private tableList$: Observable<DataTableState[]>;
        // private tableList: DataTableState[];
        this.datasetList = [];
    }
    TablelistComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.tableList$ = this.store
        //   .select(stateGetter('tables'))
        //   .map(tables => filter(tables, table => table.context === TableContext.TableList));
        this.subscription = this.store.select(Object(__WEBPACK_IMPORTED_MODULE_8__store_app_reducer__["b" /* stateGetter */])('tables'))
            .map(function (tables) { return Object(__WEBPACK_IMPORTED_MODULE_5_lodash__["filter"])(tables, function (table) { return table.context === __WEBPACK_IMPORTED_MODULE_6__store_app_state__["a" /* TableContext */].TableList; }); })
            .subscribe(function (filteredTablesArray) {
            filteredTablesArray.forEach(function (table) {
                if (!_this.datasetList.includes(table.dataset_id)) {
                    _this.datasetList.push(table.dataset_id);
                }
            });
            // this.tableList = filteredTables;
            // console.log('New table List recived', this.datasetList);
        });
        console.log('Subsribed. Table List');
        this.appActions.showTable(194, 1);
    };
    TablelistComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
        console.log('Unsubsribed. Table List');
    };
    /**
     * Открытие диалога добавления новой таблицы
     * TODO получить из диалога данные и уже здесь сделать this.appActions.showTable
     */
    TablelistComponent.prototype.opentab = function () {
        var _this = this;
        var modalRef = this.modalService.open(__WEBPACK_IMPORTED_MODULE_9_app_table_dlg_table_dlg_component__["a" /* TableDlgComponent */]);
        modalRef.result.then(function (result) {
            if (result.choosed === __WEBPACK_IMPORTED_MODULE_12_app_shared_interfaces__["b" /* ModalResult */].mrOk) {
                _this.appActions.showTable(result.dataset_id, __WEBPACK_IMPORTED_MODULE_6__store_app_state__["a" /* TableContext */].TableList);
            }
        });
    };
    TablelistComponent.prototype.openmodel = function () {
        var modalRef = this.modalService.open(__WEBPACK_IMPORTED_MODULE_10_app_table_model_table_model_component__["a" /* TableModelComponent */], { size: 'lg' });
    };
    TablelistComponent.prototype.onTabChange = function ($event) {
        console.log("\u0412\u044B\u0431\u0440\u0430\u043D\u0430 \u0432\u043A\u043B\u0430\u0434\u043A\u0430 id = " + $event.nextId);
        if ($event.nextId === 'tab-preventchange2') {
            $event.preventDefault();
        }
    };
    TablelistComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-tablelist',
            template: __webpack_require__("./src/app/tablelist/tablelist.component.html"),
            styles: [__webpack_require__("./src/app/tablelist/tablelist.component.scss")],
            styles: ['.header-buttons { margin-left: auto; }'],
            providers: [__WEBPACK_IMPORTED_MODULE_4_ngx_pipes__["a" /* KeysPipe */], __WEBPACK_IMPORTED_MODULE_4_ngx_pipes__["c" /* ValuesPipe */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_7__store_app_actions__["a" /* AppActions */],
            __WEBPACK_IMPORTED_MODULE_11_app_shared_api_service__["a" /* APIService */],
            __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]])
    ], TablelistComponent);
    return TablelistComponent;
}());



/***/ }),

/***/ "./src/app/user-activation/user-activation.component.html":
/***/ (function(module, exports) {

module.exports = "<div static-page-layout>\r\n  <div class=\"container\">\r\n    <div class=\"loading-overlay-container\" *ngIf=\"activationStatusReceived === false\"></div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\" *ngIf=\"activationStatusReceived\">\r\n        <div style=\"text-align: center; padding: 40px;\">\r\n            <h2 *ngIf=\"successfullActivation\">\r\n              <i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i> User account was activated, please log in\r\n            </h2>\r\n          <h2 *ngIf=\"successfullActivation === false\">\r\n            <i class=\"fa fa-remove\" aria-hidden=\"true\" style=\"color: red;\"></i> Activation link is invalid\r\n          </h2>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12\" *ngIf=\"activationStatusReceived === false\">\r\n          <div style=\"text-align: center; padding: 40px;\">\r\n            <h2><i class=\"fa fa-refresh\" aria-hidden=\"true\" style=\"color: orange;\"></i> Activating user account</h2>\r\n          </div>\r\n        </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/user-activation/user-activation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserActivationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserActivationComponent = /** @class */ (function () {
    function UserActivationComponent(activatedRoute, apiService) {
        this.activatedRoute = activatedRoute;
        this.apiService = apiService;
        this.activationStatusReceived = false;
        this.successfullActivation = false;
    }
    UserActivationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.apiService.activateUser(params.code).then(function (result) {
                _this.activationStatusReceived = true;
                _this.successfullActivation = true;
            }).catch(function (errorResponse) {
                _this.activationStatusReceived = true;
            });
        });
    };
    UserActivationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'gp-user-activation',
            template: __webpack_require__("./src/app/user-activation/user-activation.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* APIService */]])
    ], UserActivationComponent);
    return UserActivationComponent;
}());



/***/ }),

/***/ "./src/app/user-registration/user-registration.component.html":
/***/ (function(module, exports) {

module.exports = "<div static-page-layout>\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\" *ngIf=\"registered\">\r\n        <div style=\"text-align: center; padding: 40px;\">\r\n          <h2><i class=\"fa fa-check\" aria-hidden=\"true\" style=\"color: green;\"></i> Email with activation link was sent to specified email</h2>\r\n          <p>Please click the link in email. If you do not see the email in your inbox, please check the Spam folder.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12\" *ngIf=\"registered === false\">\r\n        <app-doc-form\r\n          [meta]=\"userTableMeta\"\r\n          (onSuccessfullSubmition)=\"onSuccessfulRegistration($event)\"\r\n          ></app-doc-form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/user-registration/user-registration.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user-registration/user-registration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRegistrationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__("./src/app/app.config.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserRegistrationComponent = /** @class */ (function () {
    function UserRegistrationComponent(apiService) {
        var _this = this;
        this.apiService = apiService;
        this.userTableMeta = false;
        this.registered = false;
        this.apiService.meta(__WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* AppConfig */].systemTablesIdentifiers['USERS']).then(function (result) {
            _this.userTableMeta = result;
        });
    }
    UserRegistrationComponent.prototype.onSuccessfulRegistration = function () {
        this.registered = true;
    };
    UserRegistrationComponent.prototype.ngOnInit = function () { };
    UserRegistrationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-user-registration',
            template: __webpack_require__("./src/app/user-registration/user-registration.component.html"),
            styles: [__webpack_require__("./src/app/user-registration/user-registration.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_api_service__["a" /* APIService */]])
    ], UserRegistrationComponent);
    return UserRegistrationComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-boolean/w-boolean.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-boolean/w-boolean.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">    \r\n  <div *ngSwitchCase=\"'forminput'\">\r\n      <select class=\"form-control\" id=\"{{attr.fieldname}}\" type=\"text\" name=\"{{attr.fieldname}}\" [(ngModel)]=\"widgetValue\" #t (change)=\"change(t.value)\">\r\n        <option *ngFor=\"let obj of values\" [value]=\"obj.value\">{{obj.name}}</option>\r\n      </select>\r\n      </div>\r\n  <div *ngSwitchCase=\"'properites'\">{{getvalue()}}</div>  \r\n  <div *ngSwitchDefault>{{getvalue()}}</div>\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-boolean/w-boolean.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WBooleanComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WBooleanComponent = /** @class */ (function () {
    function WBooleanComponent() {
        this.attr = {};
        this.mode = "input";
        this.values = [{ name: 'Yes', value: 'true' }, { name: 'No', value: 'false' }, { name: 'Unknown' }];
        //@Input() onChange;
        this.widgetValue = '';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    WBooleanComponent.prototype.getvalue = function () {
        return (this.widgetValue);
    };
    WBooleanComponent.prototype.change = function (val) {
        this.widgetValue = val;
        this.widgetValueChange.emit([null, this.widgetValue, this]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WBooleanComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WBooleanComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WBooleanComponent.prototype, "widgetValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WBooleanComponent.prototype, "widgetValueChange", void 0);
    WBooleanComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-boolean',
            template: __webpack_require__("./src/app/widgets/w-boolean/w-boolean.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-boolean/w-boolean.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WBooleanComponent);
    return WBooleanComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-classify/w-classify.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">\r\n  <div *ngSwitchCase=\"'forminput'\">111113333\r\n    <ng2-completer [(ngModel)]=\"strwidgetValue\" [datasource]=\"customData\" [minSearchLength]=\"3\" [inputClass]=\"'form-control'\" [placeholder]=\"''\"\r\n      (selected)=\"widgetValueSubmitted($event)\" [textSearching]=\"'Please wait...'\">\r\n    </ng2-completer>\r\n\r\n  </div>\r\n  <div *ngSwitchCase=\"'properites'\">\r\n      {{attr.widget.properties.reftablename}}\r\n    <w-table [mode]=\"'forminput'\" [widgetValue]=\"attr.widget.properties.dataset_id\" (widgetValueChange)=\"changedataset($event)\"></w-table>\r\n\r\n    <div class=\"form-group\">\r\n      <label for=\"tablename\">Widget type</label>\r\n      <w-select [attr]=\"{fieldname:'control_type',widget:{properties:{options:'autoselect:autocomplete;select:select;radio:radio'}}}\"\r\n        [widgetValue]=\"attr.widget.properties.control_type\" [mode]=\"'forminput'\" (widgetValueChange)=\"setcontrol_type($event)\"></w-select>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n      <ng-container *ngFor=\"let col of refMeta.columns\" >        \r\n        <input type=\"checkbox\" *ngIf=\"col.title && col.title!=''\" name=\"tc\" [ngModel]=\"col.checked\" (change)=\"changeRefColumn(col)\">{{col.title}}\r\n      </ng-container>\r\n    </div>\r\n\r\n  </div>\r\n  <div *ngSwitchDefault>{{strwidgetValue}}</div>\r\n\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-classify/w-classify.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-classify/w-classify.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WClassifyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_widgets_w_edit_CompleterSearch__ = __webpack_require__("./src/app/widgets/w-edit/CompleterSearch.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WClassifyComponent = /** @class */ (function () {
    function WClassifyComponent(datatableService) {
        this.datatableService = datatableService;
        this.mode = 'input';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this._widgetIdValue = '';
        this.strwidgetValue = '';
        this.refMeta = {};
        this._objectValue = {};
    }
    Object.defineProperty(WClassifyComponent.prototype, "widgetValue", {
        get: function () {
            return this._widgetIdValue;
        },
        set: function (value) {
            this._objectValue = value;
        },
        enumerable: true,
        configurable: true
    });
    WClassifyComponent.prototype.getValue = function () {
        return this._widgetIdValue;
    };
    WClassifyComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.attr)
            this.attr = {
                fieldname: '',
                tablename: '',
                sqltablename: '',
                sqlname: '',
                widget: { name: '', properties: {} }
            };
        if (!this.attr.widget)
            this.attr.widget = { name: '', properties: {} };
        if (this.attr.widget.properties.dataset_id) {
            this.customData = new __WEBPACK_IMPORTED_MODULE_2_app_widgets_w_edit_CompleterSearch__["a" /* CompleterSearch */](this.datatableService, this.attr.widget.properties.dataset_id, this.attr.widget.properties.db_field.join(','));
        }
        if (!this.attr.widget.properties.db_field)
            this.attr.widget.properties.db_field = [];
        this.changedataset(this.attr.widget.properties.dataset_id);
        this.attr.widget.properties.reftablename =
            'r_' +
                Math.random()
                    .toString(36)
                    .substring(2, 10);
        if (typeof this._objectValue === 'object') {
            this._widgetIdValue = this._objectValue['id'];
            this.strwidgetValue = '';
            this.attr.widget.properties.db_field.forEach(function (element) {
                _this.strwidgetValue += ' ' + _this._objectValue[element];
            });
        }
        else
            this._widgetIdValue = this._objectValue;
        if (this.strwidgetValue == '' && this._widgetIdValue !== '') {
            var filters = {
                fieldname: 'id',
                valueList: [this._widgetIdValue]
            };
            this.datatableService
                .list(this.attr.widget.properties.dataset_id, 1, 0, [filters])
                .then(function (rows) {
                _this.strwidgetValue = '';
                _this.attr.widget.properties.db_field.forEach(function (element) {
                    _this.strwidgetValue += ' ' + rows['aaData'][0][element];
                });
                // this.collectionSize=100;
            });
        }
    };
    WClassifyComponent.prototype.widgetValueSubmitted = function (event) {
        //if (this.widgetValue) {
        this._widgetIdValue = event.originalObject;
        this.widgetValueChange.emit(event.originalObject);
        //}
    };
    WClassifyComponent.prototype.onChange = function ($event) {
        this.widgetValueSubmitted(event);
    };
    WClassifyComponent.prototype.setcontrol_type = function (value) {
        this.attr.widget.properties.control_type = value;
    };
    WClassifyComponent.prototype.changedataset = function (dataset_id) {
        var _this = this;
        if (!dataset_id)
            return;
        console.log(dataset_id);
        this.attr.widget.properties.reftablename =
            'r_' +
                Math.random()
                    .toString(36)
                    .substring(2, 10);
        this.attr.widget.properties.dataset_id = dataset_id;
        this.datatableService.meta(dataset_id).then(function (serviceMeta) {
            _this.refMeta = serviceMeta;
            if (!_this.attr.widget.properties.db_field) {
                _this.attr.widget.properties.db_field = [];
            }
            _this.refMeta.columns.forEach(function (element) {
                element['checked'] =
                    _this.attr.widget.properties.db_field.indexOf(element.fieldname) !==
                        -1;
            });
        });
    };
    WClassifyComponent.prototype.changeRefColumn = function (col) {
        var index = this.attr.widget.properties.db_field.indexOf(col.fieldname);
        if (col.checked) {
            this.attr.widget.properties.db_field.splice(index);
        }
        else {
            this.attr.widget.properties.db_field.push(col.fieldname);
        }
        col.checked = !col.checked;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WClassifyComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WClassifyComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WClassifyComponent.prototype, "widgetValueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], WClassifyComponent.prototype, "widgetValue", null);
    WClassifyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-classify',
            template: __webpack_require__("./src/app/widgets/w-classify/w-classify.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-classify/w-classify.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_app_shared_api_service__["a" /* APIService */]])
    ], WClassifyComponent);
    return WClassifyComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-date/w-date.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-date/w-date.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">    \r\n  <div *ngSwitchCase=\"'forminput'\">\r\n    <input [ngModel]=\"widgetValue | date:'yyyy-MM-dd'\"  type=\"date\" (ngModelChange)=\"onChange($event)\"/>\r\n  </div>\r\n  <div *ngSwitchCase=\"'properites'\">{{getValue()}}</div>  \r\n  <div *ngSwitchDefault>{{widgetValue | date:'dd.MM.yyyy'}}</div>\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-date/w-date.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WDateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WDateComponent = /** @class */ (function () {
    //subscription: Subscription;
    function WDateComponent() {
        this.attr = {};
        this.widgetValue = '';
        this.mode = "input";
        this.val_field = '';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    WDateComponent.prototype.ngOnInit = function () {
    };
    WDateComponent.prototype.getValue = function () {
        return (this.widgetValue);
    };
    WDateComponent.prototype.ngOnDestroy = function () {
        // prevent memory leak when component destroyed
        //this.subscription.unsubscribe();
    };
    WDateComponent.prototype.onChange = function ($event) {
        this.widgetValue = $event;
        this.widgetValueChange.emit([$event, this.widgetValue, this]);
        //console.log('qqqqqqqqqq',this.widgetValue, this);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WDateComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WDateComponent.prototype, "widgetValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WDateComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WDateComponent.prototype, "widgetValueChange", void 0);
    WDateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-date',
            template: __webpack_require__("./src/app/widgets/w-date/w-date.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-date/w-date.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WDateComponent);
    return WDateComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-edit/CompleterSearch.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompleterSearch; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/_esm5/Subject.js");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var CompleterSearch = /** @class */ (function (_super) {
    __extends(CompleterSearch, _super);
    function CompleterSearch(datatableService, dataset_id, fieldname) {
        var _this = _super.call(this) || this;
        _this.datatableService = datatableService;
        _this.dataset_id = dataset_id;
        _this.fieldname = fieldname;
        return _this;
    }
    CompleterSearch.prototype.search = function (term) {
        var _this = this;
        var filter = {
            fieldname: 'name',
            valueList: [term]
        };
        this.datatableService.listObservable(this.dataset_id, 5, 0, [filter])
            .map(function (res) {
            // Convert the result to CompleterItem[]
            var data = res;
            var matches = data.aaData.map(function (row) { return _this.convertToItem(row); });
            _this.next(matches);
        })
            .subscribe();
    };
    CompleterSearch.prototype.cancel = function () {
        // Handle cancel
    };
    CompleterSearch.prototype.convertToItem = function (data) {
        if (!data) {
            return null;
        }
        // data will be string if an initial value is set
        return {
            title: data[this.fieldname],
            originalObject: data.id
        };
    };
    return CompleterSearch;
}(__WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__["a" /* Subject */]));



/***/ }),

/***/ "./src/app/widgets/w-edit/w-edit.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-edit/w-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">\r\n  <div *ngSwitchCase=\"'forminput'\">\r\n    <ng2-completer *ngIf=\"column.widget.properties.autocomplete\" [(ngModel)]=\"widgetValue\" [datasource]=\"customData\" [minSearchLength]=\"3\"\r\n      [inputClass]=\"'form-control'\" (selected)=\"widgetValueSubmitted()\" (blur)=\"widgetValueSubmitted($event)\"\r\n      (keyup)=\"keyup($event)\" [textSearching]=\"'Please wait...'\">\r\n    </ng2-completer>\r\n\r\n    <input class=\"form-control\" *ngIf=\"!column.widget.properties.autocomplete\" ([ngModel])=\"widgetValue\" type=\"text\" (blur)=\"widgetValueSubmitted($event)\" (keyup.enter)=\"widgetValueSubmitted($event)\"/>\r\n\r\n  </div>\r\n  <div *ngSwitchCase=\"'properites'\">\r\n    <div class=\"form-group\">\r\n      <input type=\"checkbox\" [checked]=\"column.widget.properties.autocomplete\" (change)=\"column.widget.properties.autocomplete = !column.widget.properties.autocomplete\">Autocomplete {{column.widget.properties.autocomplete}}\r\n    </div>\r\n    <ng-container *ngIf=\"column.widget.properties.autocomplete\">\r\n      {{column.widget.properties.dataset_id}}\r\n      <w-table *ngIf=\"column.widget.properties.autocomplete\" [mode]=\"'forminput'\" (widgetValueChange)=\"changeautocompletedataset($event)\"></w-table>\r\n      <ng-container *ngFor=\"let col of refcolumns\" >        \r\n          <input *ngIf=\"col.title\" name=\"options\" ng-control=\"options\" type=\"radio\" [value]=\"col.fieldname\"  [(ngModel)]=\"column.widget.properties.autocompletefield\" >{{col.title}}<br/>\r\n      </ng-container>\r\n    </ng-container>\r\n\r\n  </div>\r\n  <div *ngSwitchDefault>{{widgetValue}}</div>\r\n\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-edit/w-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_shared_api_service__ = __webpack_require__("./src/app/shared/api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_widgets_w_edit_CompleterSearch__ = __webpack_require__("./src/app/widgets/w-edit/CompleterSearch.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WEditComponent = /** @class */ (function () {
    function WEditComponent(datatableService) {
        this.datatableService = datatableService;
        this.column = {
            fieldname: '',
            tablename: '',
            sqltablename: '',
            sqlname: '',
            widget: { name: '', properties: {} }
        };
        this.mode = 'input';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.refcolumns = [];
        this._widgetValue = '';
    }
    WEditComponent.prototype.ngOnInit = function () {
        if (!this.column) {
            this.column = {
                fieldname: '',
                tablename: '',
                sqltablename: '',
                sqlname: '',
                widget: { name: '', properties: {} }
            };
        }
        if (this.mode === 'properites') {
            this.changeautocompletedataset(this.column.widget.properties.dataset_id);
        }
        else if (this.mode === 'forminput') {
            if (this.column.widget.properties.autocomplete &&
                this.column.widget.properties.dataset_id) {
                this.customData = new __WEBPACK_IMPORTED_MODULE_2_app_widgets_w_edit_CompleterSearch__["a" /* CompleterSearch */](this.datatableService, this.column.widget.properties.dataset_id, this.column.widget.properties.autocompletefield);
            }
            else {
                this.customData = new __WEBPACK_IMPORTED_MODULE_2_app_widgets_w_edit_CompleterSearch__["a" /* CompleterSearch */](this.datatableService, 100, 'name');
            }
        }
    };
    WEditComponent.prototype.ngOnDestroy = function () { };
    Object.defineProperty(WEditComponent.prototype, "widgetValue", {
        get: function () {
            return this._widgetValue;
        },
        set: function (value) {
            this._widgetValue = value;
        },
        enumerable: true,
        configurable: true
    });
    WEditComponent.prototype.getValue = function () {
        return this._widgetValue;
    };
    WEditComponent.prototype.widgetValueSubmitted = function ($event) {
        if ($event) {
            this.widgetValue = $event.target.value;
        }
        this.widgetValueChange.emit(this.widgetValue);
    };
    WEditComponent.prototype.keyup = function ($event) {
        if ($event.keyCode === 13) {
            this.widgetValueChange.emit(this.widgetValue);
        }
    };
    WEditComponent.prototype.changeautocompletedataset = function (dataset_id) {
        var _this = this;
        this.column.widget.properties.dataset_id = dataset_id;
        this.refcolumns = [];
        this.datatableService.meta(dataset_id).then(function (serviceMeta) {
            _this.refcolumns = serviceMeta.columns;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WEditComponent.prototype, "column", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WEditComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WEditComponent.prototype, "widgetValueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], WEditComponent.prototype, "widgetValue", null);
    WEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-edit',
            template: __webpack_require__("./src/app/widgets/w-edit/w-edit.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-edit/w-edit.component.css")],
            providers: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_app_shared_api_service__["a" /* APIService */]])
    ], WEditComponent);
    return WEditComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-geometry/w-geometry.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"_mode\">\r\n  <div *ngSwitchCase=\"'forminput'\">\r\n    <div *ngIf=\"editingIsEnabled\">\r\n      <input class=\"form-control\" id=\"{{attr.fieldname}}\" type=\"text\" \r\n        name=\"{{attr.fieldname}}\" [(ngModel)]=\"_ownValue\" (blur)=\"onWidgetValueChanged();\"/>\r\n    </div>\r\n    <div *ngIf=\"!editingIsEnabled\">\r\n      <div placement=\"top\" ngbTooltip=\"{{ _ownValue }}\" (dblclick)=\"enableFormInput($event)\">\r\n        <span class=\"badge badge-secondary\" placement=\"top\" ngbTooltip=\"Double-click to edit on map\"><i class=\"fa fa-question\" aria-hidden=\"true\"></i></span>\r\n        {{ ownValue }}\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div *ngSwitchDefault placement=\"top\" ngbTooltip=\"{{ _ownValue }}\">{{ ownValue }}</div>\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-geometry/w-geometry.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WGeometryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__ = __webpack_require__("./src/app/doc-form/doc_service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_take__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/take.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var WGeometryComponent = /** @class */ (function () {
    function WGeometryComponent(docService, store, _el, actions) {
        this.docService = docService;
        this.store = store;
        this._el = _el;
        this.actions = actions;
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.mapId = '';
        this.refcolumns = [];
        this._ownValue = '';
        this._mode = '';
        // Specifies if the geomtery editing is enabled. The mode can be 'forminput',
        // but triggering the editing for geometry fields requires extra actions (double click)
        this.editingIsEnabled = false;
        this._widgetValue = '';
        this._ownValue = this.widgetValue;
    }
    Object.defineProperty(WGeometryComponent.prototype, "mode", {
        set: function (value) {
            this._mode = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WGeometryComponent.prototype, "widgetValue", {
        get: function () {
            return this._widgetValue;
        },
        set: function (value) {
            this._widgetValue = value;
        },
        enumerable: true,
        configurable: true
    });
    WGeometryComponent.prototype.getValue = function () {
        return this._widgetValue;
    };
    WGeometryComponent.prototype.onWidgetValueChanged = function () {
        if (this._ownValue) {
            this.widgetValueChange.emit(this._ownValue);
            this._ownValue = '';
        }
    };
    WGeometryComponent.prototype.ngOnInit = function () {
        this._ownValue = this.widgetValue;
        if (this.mapId === '') {
            throw 'The map identifier has to be specified for the point widget';
        }
    };
    WGeometryComponent.prototype.stopDrawing = function () {
        console.log('stopDrawing');
        this.actions.cancelDrawing({ mapId: this.mapId });
        if (this.subscription)
            this.subscription.unsubscribe();
        this.editingIsEnabled = false;
    };
    /**
     * Processes click in any arbitrary area of the page in order to disable the input.
     * Note: it does not happen when we finish the line or polygon drawing.
     */
    WGeometryComponent.prototype.onClick = function (event) {
        var _this = this;
        if (this.editingIsEnabled && this._mode === 'forminput' && !this._el.nativeElement.contains(event.target)) {
            this.store.take(1).subscribe(function (globalState) {
                var state = globalState.state;
                var mapElementIds = [];
                for (var key in state) {
                    if (key.substring(0, 5) === 'maps_') {
                        mapElementIds.push(key);
                    }
                }
                var eventHappenedInOneOFMaps = false;
                mapElementIds.map(function (elementId) {
                    var map = document.getElementById(elementId);
                    if (map.contains(event.target)) {
                        eventHappenedInOneOFMaps = true;
                        return false;
                    }
                });
                if (eventHappenedInOneOFMaps === false) {
                    _this.stopDrawing();
                }
            });
        }
    };
    WGeometryComponent.prototype.onKeyDown = function (event) {
        var e = event;
        if (e.keyCode === 13) {
            this.onWidgetValueChanged();
            return;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WGeometryComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WGeometryComponent.prototype, "widgetValueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], WGeometryComponent.prototype, "mapId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], WGeometryComponent.prototype, "mode", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], WGeometryComponent.prototype, "widgetValue", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* HostListener */])('keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], WGeometryComponent.prototype, "onKeyDown", null);
    WGeometryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            template: __webpack_require__("./src/app/widgets/w-geometry/w-geometry.component.html"),
            providers: [__WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__["a" /* DocService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__["a" /* DocService */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_2__store_app_actions__["a" /* AppActions */]])
    ], WGeometryComponent);
    return WGeometryComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-line/w-line.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WLineComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__ = __webpack_require__("./src/app/doc-form/doc_service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__w_geometry_w_geometry_component__ = __webpack_require__("./src/app/widgets/w-geometry/w-geometry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__map_map_component__ = __webpack_require__("./src/app/map/map.component.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var WLineComponent = /** @class */ (function (_super) {
    __extends(WLineComponent, _super);
    function WLineComponent(docService, store, _el, actions) {
        var _this = _super.call(this, docService, store, _el, actions) || this;
        _this.docService = docService;
        _this.store = store;
        _this._el = _el;
        _this.actions = actions;
        _this.featureName = 'lastLine';
        return _this;
    }
    WLineComponent.prototype.enableFormInput = function () {
        var _this = this;
        this.editingIsEnabled = true;
        this.subscription = this.store.select(Object(__WEBPACK_IMPORTED_MODULE_4__store_app_reducer__["b" /* stateGetter */])(this.mapId + '')).subscribe(function (mapData) {
            if (_this.editingIsEnabled && _this._mode === 'forminput' && mapData[_this.featureName]) {
                _this._ownValue = mapData[_this.featureName];
                _this.widgetValueChange.emit(_this._ownValue);
                _this.stopDrawing();
            }
        });
        this.actions.enableDrawingForMap({
            mapId: this.mapId,
            mode: __WEBPACK_IMPORTED_MODULE_6__map_map_component__["a" /* MapComponent */].MODE_LINE,
            existingFeature: ((this._ownValue) ? this._ownValue : false)
        });
    };
    Object.defineProperty(WLineComponent.prototype, "ownValue", {
        get: function () {
            if (this._ownValue) {
                return this._ownValue.substring(0, 8) + "...";
            }
            else {
                return 'Empty';
            }
        },
        enumerable: true,
        configurable: true
    });
    WLineComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'w-line',
            template: __webpack_require__("./src/app/widgets/w-geometry/w-geometry.component.html"),
            host: {
                '(document:click)': 'onClick($event)',
            },
            providers: [__WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__["a" /* DocService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__["a" /* DocService */], __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_3__store_app_actions__["a" /* AppActions */]])
    ], WLineComponent);
    return WLineComponent;
}(__WEBPACK_IMPORTED_MODULE_2__w_geometry_w_geometry_component__["a" /* WGeometryComponent */]));



/***/ }),

/***/ "./src/app/widgets/w-number/w-number.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">\r\n  <div *ngSwitchCase=\"'forminput'\">\r\n    <input \r\n      type=\"number\" \r\n      class=\"form-control\" \r\n      id=\"{{column.fieldname}}\" \r\n      name=\"{{column.fieldname}}\" \r\n      [(ngModel)]=\"widgetValue\" \r\n      (blur)=\"widgetValueSubmitted()\"\r\n      (keyup.enter)=\"widgetValueSubmitted()\"\r\n    />\r\n  </div>\r\n  <div *ngSwitchCase=\"'properites'\">\r\n    <div class=\"form-group\">\r\n      <label for=\"properties_before_{{column.fieldname}}\">digits before</label>\r\n      <input \r\n        type=\"number\" \r\n        class=\"form-control\" \r\n        name=\"properties_before_{{column.fieldname}}\" \r\n        [(ngModel)]=\"column.widget.properties.before\"\r\n      />\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <label for=\"properties_after_{{column.fieldname}}\">digits after</label>\r\n      <input \r\n        type=\"number\" \r\n        class=\"form-control\" \r\n        name=\"properties_after_{{column.fieldname}}\" \r\n        [(ngModel)]=\"column.widget.properties.after\"\r\n      />\r\n    </div>\r\n  </div>\r\n  <div *ngSwitchDefault>{{widgetValue}}</div>\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-number/w-number.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-number/w-number.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WNumberComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WNumberComponent = /** @class */ (function () {
    function WNumberComponent() {
        this.mode = 'input';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this._widgetValue = '';
    }
    Object.defineProperty(WNumberComponent.prototype, "widgetValue", {
        get: function () {
            return this._widgetValue;
        },
        set: function (value) {
            this._widgetValue = value;
        },
        enumerable: true,
        configurable: true
    });
    WNumberComponent.prototype.widgetValueSubmitted = function () {
        if (this.widgetValue) {
            this.widgetValueChange.emit(this.widgetValue);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WNumberComponent.prototype, "column", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WNumberComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WNumberComponent.prototype, "widgetValueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], WNumberComponent.prototype, "widgetValue", null);
    WNumberComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-number',
            template: __webpack_require__("./src/app/widgets/w-number/w-number.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-number/w-number.component.scss")]
        })
    ], WNumberComponent);
    return WNumberComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-password/w-password.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-password/w-password.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">    \r\n  <div *ngSwitchCase=\"'forminput'\">      \r\n    <div class=\"input-group\">\r\n      <input class=\"form-control\" [ngClass]=\"{'is-invalid': passwordsDoNotMatch || passwordHasWrongFormat}\" type=\"{{ inputType }}\" name=\"passwordTry1\" [(ngModel)]=\"passwordTry1\" (blur)=\"onBlur()\"/>\r\n      <input class=\"form-control\" [ngClass]=\"{'is-invalid': passwordsDoNotMatch || passwordHasWrongFormat}\" type=\"{{ inputType }}\" name=\"passwordTry2\" [(ngModel)]=\"passwordTry2\" (blur)=\"onBlur()\"/>\r\n      <div class=\"input-group-append\">\r\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"changeInputType($event)\">\r\n          <i *ngIf=\"inputType === 'password'\" aria-hidden=\"true\" class=\"fa fa-eye\" title=\"Show password\"></i>\r\n          <i *ngIf=\"inputType === 'text'\" aria-hidden=\"true\" class=\"fa fa-eye-slash\" title=\"Hide password\"></i>\r\n        </button>\r\n      </div>\r\n    </div>\r\n    <div>\r\n      <div *ngIf=\"passwordsDoNotMatch\" class=\"error-message\">Passwords do not match</div>\r\n      <div *ngIf=\"passwordHasWrongFormat\" class=\"error-message\">Password is too weak</div>\r\n    </div>\r\n  </div>\r\n  <div *ngSwitchCase=\"'properites'\">{{getvalue()}}</div>  \r\n  <div *ngSwitchDefault>{{getvalue()}}</div>\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-password/w-password.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WPasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WPasswordComponent = /** @class */ (function () {
    function WPasswordComponent() {
        this.attr = {};
        this.mode = "input";
        this.inputType = 'password';
        this.passwordTry1 = '';
        this.passwordTry2 = '';
        this.passwordsDoNotMatch = false;
        this.passwordHasWrongFormat = false;
        // @Input() onChange;
        this.widgetValue = '';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    WPasswordComponent.prototype.getvalue = function () {
        return (this.widgetValue);
    };
    WPasswordComponent.prototype.onBlur = function (event) {
        this.passwordsDoNotMatch = false;
        this.passwordHasWrongFormat = false;
        if (this.passwordTry1.length > 0 || this.passwordTry2.length > 0) {
            if (this.passwordTry1 === this.passwordTry2) {
                if (this.scorePassword(this.passwordTry1) > 60) {
                    this.widgetValue = this.passwordTry1;
                    this.widgetValueChange.emit(this.widgetValue);
                }
                else {
                    this.passwordHasWrongFormat = true;
                }
            }
            else {
                this.passwordsDoNotMatch = true;
            }
        }
    };
    WPasswordComponent.prototype.changeInputType = function (event) {
        if (this.inputType === 'password') {
            this.inputType = 'text';
        }
        else {
            this.inputType = 'password';
        }
    };
    /**
     * Scoring password (by https://stackoverflow.com/questions/948172/password-strength-meter)
     *
     * @param pass Password to check
     */
    WPasswordComponent.prototype.scorePassword = function (pass) {
        var score = 0;
        if (!pass)
            return score;
        var letters = new Object();
        for (var i = 0; i < pass.length; i++) {
            letters[pass[i]] = (letters[pass[i]] || 0) + 1;
            score += 5.0 / letters[pass[i]];
        }
        var variations = {
            digits: /\d/.test(pass),
            lower: /[a-z]/.test(pass),
            upper: /[A-Z]/.test(pass),
            nonWords: /\W/.test(pass),
        };
        var variationCount = 0;
        for (var check in variations) {
            variationCount += (variations[check] == true) ? 1 : 0;
        }
        score += (variationCount - 1) * 10;
        return score;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WPasswordComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WPasswordComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WPasswordComponent.prototype, "widgetValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WPasswordComponent.prototype, "widgetValueChange", void 0);
    WPasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-password',
            template: __webpack_require__("./src/app/widgets/w-password/w-password.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-password/w-password.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WPasswordComponent);
    return WPasswordComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-point/w-point.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WPointComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__ = __webpack_require__("./src/app/doc-form/doc_service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__w_geometry_w_geometry_component__ = __webpack_require__("./src/app/widgets/w-geometry/w-geometry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_conversions__ = __webpack_require__("./src/app/shared/conversions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__map_map_component__ = __webpack_require__("./src/app/map/map.component.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var WPointComponent = /** @class */ (function (_super) {
    __extends(WPointComponent, _super);
    function WPointComponent(docService, store, _el, actions) {
        var _this = _super.call(this, docService, store, _el, actions) || this;
        _this.docService = docService;
        _this.store = store;
        _this._el = _el;
        _this.actions = actions;
        return _this;
    }
    WPointComponent.prototype.enableFormInput = function () {
        var _this = this;
        this.editingIsEnabled = true;
        this.subscription = this.store.select(Object(__WEBPACK_IMPORTED_MODULE_5__store_app_reducer__["b" /* stateGetter */])(this.mapId + '')).subscribe(function (mapData) {
            if (_this.editingIsEnabled && _this._mode === 'forminput' && mapData['lastPoint']) {
                console.log('new value', mapData['lastPoint']);
                _this._ownValue = mapData['lastPoint'];
                _this.widgetValueChange.emit(_this._ownValue);
                _this.stopDrawing();
            }
        });
        this.actions.enableDrawingForMap({
            mapId: this.mapId,
            mode: __WEBPACK_IMPORTED_MODULE_7__map_map_component__["a" /* MapComponent */].MODE_POINT,
            existingFeature: ((this._ownValue) ? this._ownValue : false)
        });
    };
    Object.defineProperty(WPointComponent.prototype, "ownValue", {
        get: function () {
            var result = Object(__WEBPACK_IMPORTED_MODULE_3__shared_conversions__["b" /* wkt2decimal */])(this._ownValue);
            return result.lon.substring(0, 8) + " " + result.lat.substring(0, 8);
        },
        enumerable: true,
        configurable: true
    });
    WPointComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'w-point',
            template: __webpack_require__("./src/app/widgets/w-geometry/w-geometry.component.html"),
            host: {
                '(document:click)': 'onClick($event)',
            },
            providers: [__WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__["a" /* DocService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__doc_form_doc_service__["a" /* DocService */], __WEBPACK_IMPORTED_MODULE_6__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_4__store_app_actions__["a" /* AppActions */]])
    ], WPointComponent);
    return WPointComponent;
}(__WEBPACK_IMPORTED_MODULE_2__w_geometry_w_geometry_component__["a" /* WGeometryComponent */]));



/***/ }),

/***/ "./src/app/widgets/w-polygon/w-polygon.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WPolygonComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_doc_form_doc_service__ = __webpack_require__("./src/app/doc-form/doc_service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__w_geometry_w_geometry_component__ = __webpack_require__("./src/app/widgets/w-geometry/w-geometry.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_store_app_actions__ = __webpack_require__("./src/app/store/app.actions.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_store_app_reducer__ = __webpack_require__("./src/app/store/app.reducer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_map_map_component__ = __webpack_require__("./src/app/map/map.component.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var WPolygonComponent = /** @class */ (function (_super) {
    __extends(WPolygonComponent, _super);
    function WPolygonComponent(docService, store, _el, actions) {
        var _this = _super.call(this, docService, store, _el, actions) || this;
        _this.docService = docService;
        _this.store = store;
        _this._el = _el;
        _this.actions = actions;
        _this.featureName = 'lastPolygon';
        return _this;
    }
    WPolygonComponent.prototype.enableFormInput = function () {
        var _this = this;
        this.editingIsEnabled = true;
        this.subscription = this.store.select(Object(__WEBPACK_IMPORTED_MODULE_4_app_store_app_reducer__["b" /* stateGetter */])(this.mapId + '')).subscribe(function (mapData) {
            if (_this.editingIsEnabled && _this._mode === 'forminput' && mapData[_this.featureName]) {
                _this._ownValue = mapData[_this.featureName];
                _this.widgetValueChange.emit(_this._ownValue);
                _this.stopDrawing();
            }
        });
        this.actions.enableDrawingForMap({
            mapId: this.mapId,
            mode: __WEBPACK_IMPORTED_MODULE_6_app_map_map_component__["a" /* MapComponent */].MODE_POLYGON,
            existingFeature: ((this._ownValue) ? this._ownValue : false)
        });
    };
    Object.defineProperty(WPolygonComponent.prototype, "ownValue", {
        get: function () {
            if (this._ownValue) {
                return this._ownValue.substring(0, 8) + "...";
            }
            else {
                return 'Empty';
            }
        },
        enumerable: true,
        configurable: true
    });
    WPolygonComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'w-polygon',
            template: __webpack_require__("./src/app/widgets/w-geometry/w-geometry.component.html"),
            host: {
                '(document:click)': 'onClick($event)',
            },
            providers: [__WEBPACK_IMPORTED_MODULE_1_app_doc_form_doc_service__["a" /* DocService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_app_doc_form_doc_service__["a" /* DocService */], __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["h" /* Store */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_3_app_store_app_actions__["a" /* AppActions */]])
    ], WPolygonComponent);
    return WPolygonComponent;
}(__WEBPACK_IMPORTED_MODULE_2__w_geometry_w_geometry_component__["a" /* WGeometryComponent */]));



/***/ }),

/***/ "./src/app/widgets/w-select/w-select.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">    \r\n    <div *ngSwitchCase=\"'forminput'\"> \r\n        <select name=\"{{attr.fieldname}}\" class=\"form-control\" (change)=\"widgetValueSubmitted($event)\">\r\n          <ng-container *ngFor=\"let v of valueList\">\r\n            <option *ngIf=\"v.id==widgetValue\" selected value=\"v.id\"> {{v.name}} </option>\r\n            <option *ngIf=\"v.id!=widgetValue\" [value]=\"v.id\"> {{v.name}} </option>\r\n          </ng-container>\r\n        </select>\r\n        \r\n   \r\n    </div>\r\n    <div *ngSwitchCase=\"'properites'\">\r\n        <input \r\n        type=\"text\" \r\n        class=\"form-control\"         \r\n        name=\"{{attr.fieldname}}\" \r\n        [(ngModel)]=\"attr.widget.properties.options\"\r\n        />\r\n        \r\n\r\n    </div>  \r\n    <div *ngSwitchDefault>{{userValue}}</div>\r\n    \r\n  </ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-select/w-select.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-select/w-select.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WSelectComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WSelectComponent = /** @class */ (function () {
    function WSelectComponent() {
        this.mode = 'input';
        this.valueList = [];
        this.userValue = '';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this._widgetValue = '';
    }
    WSelectComponent.prototype.ngOnInit = function () {
        if (this.attr.widget &&
            this.attr.widget.properties &&
            'options' in this.attr.widget.properties) {
            var vals = this.attr.widget.properties['options'].split(';');
            for (var i = 0; i < vals.length; i++) {
                var v = vals[i].split(':');
                this.valueList.push({ id: v[0], name: v[1] });
                if (v[0] == this._widgetValue) {
                    this.userValue = v[1];
                }
            }
        }
    };
    Object.defineProperty(WSelectComponent.prototype, "widgetValue", {
        get: function () {
            return this._widgetValue;
        },
        set: function (value) {
            this._widgetValue = value;
        },
        enumerable: true,
        configurable: true
    });
    WSelectComponent.prototype.getValue = function () {
        return this._widgetValue;
    };
    WSelectComponent.prototype.widgetValueSubmitted = function ($event) {
        this._widgetValue = $event.target.value;
        this.widgetValueChange.emit(this._widgetValue);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WSelectComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WSelectComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WSelectComponent.prototype, "widgetValueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], WSelectComponent.prototype, "widgetValue", null);
    WSelectComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-select',
            template: __webpack_require__("./src/app/widgets/w-select/w-select.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-select/w-select.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], WSelectComponent);
    return WSelectComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-table/w-table.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">    \r\n  <div *ngSwitchCase=\"'forminput'\"> \r\n    <datatable [mode]=\"'readonly'\" [datasetId]=\"100\" (curenttable)=\"changetab($event)\"></datatable>    \r\n  </div>\r\n  <div *ngSwitchCase=\"'properites'\">{{widgetValue}}</div>  \r\n  <div *ngSwitchDefault>{{widgetValue}}</div>\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-table/w-table.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-table/w-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WTableComponent = /** @class */ (function () {
    function WTableComponent() {
        //@Input() attr: MetadataColumns;
        this.mode = 'forminput';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this._widgetValue = '';
    }
    Object.defineProperty(WTableComponent.prototype, "widgetValue", {
        get: function () {
            return this._widgetValue;
        },
        set: function (value) {
            this._widgetValue = value;
        },
        enumerable: true,
        configurable: true
    });
    WTableComponent.prototype.ngOnInit = function () { };
    WTableComponent.prototype.changetab = function (tab) {
        this.widgetValueChange.emit(String(tab.id));
        console.log(tab);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WTableComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WTableComponent.prototype, "widgetValueChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], WTableComponent.prototype, "widgetValue", null);
    WTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-table',
            template: __webpack_require__("./src/app/widgets/w-table/w-table.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-table/w-table.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], WTableComponent);
    return WTableComponent;
}());



/***/ }),

/***/ "./src/app/widgets/w-textarea/w-textarea.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/widgets/w-textarea/w-textarea.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"mode\">    \r\n  <div *ngSwitchCase=\"'forminput'\">      \r\n      <textarea class=\"form-control\" id=\"{{attr.fieldname}}\" type=\"text\" name=\"{{attr.fieldname}}\" [(ngModel)]=\"widgetValue\" (blur)=\"onBlur()\"></textarea>\r\n  </div>\r\n  <div *ngSwitchCase=\"'properites'\">{{getvalue()}}</div>  \r\n  <div *ngSwitchDefault>{{getvalue()}}</div>\r\n</ng-container>"

/***/ }),

/***/ "./src/app/widgets/w-textarea/w-textarea.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WTextareaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WTextareaComponent = /** @class */ (function () {
    function WTextareaComponent() {
        this.attr = {};
        this.mode = "input";
        // @Input() onChange;
        this.widgetValue = '';
        this.widgetValueChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    WTextareaComponent.prototype.getvalue = function () {
        return (this.widgetValue);
    };
    WTextareaComponent.prototype.onBlur = function (event) {
        // this.widgetValue = this.getvalue();
        this.widgetValueChange.emit([event, this.widgetValue, this]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WTextareaComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WTextareaComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WTextareaComponent.prototype, "widgetValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WTextareaComponent.prototype, "widgetValueChange", void 0);
    WTextareaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'w-textarea',
            template: __webpack_require__("./src/app/widgets/w-textarea/w-textarea.component.html"),
            styles: [__webpack_require__("./src/app/widgets/w-textarea/w-textarea.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WTextareaComponent);
    return WTextareaComponent;
}());



/***/ }),

/***/ "./src/app/widgets/widget/widget.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container [ngSwitch]=\"attr.widget.name\">\r\n  <w-edit \r\n    *ngSwitchCase=\"'tableaccess'\"\r\n    [column]=\"attr\"\r\n    [widgetValue]=\"widgetValue\"  \r\n    [mode]=\"mode\" \r\n    (widgetValueChange)=\"changeval($event)\">\r\n  </w-edit>\r\n  <w-edit *ngSwitchCase=\"'edit'\"  [column]=\"attr\" [widgetValue]=\"widgetValue\"  [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\"></w-edit>\r\n  <w-password *ngSwitchCase=\"'password'\"  [widgetValue]=\"widgetValue\"  [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\"></w-password>\r\n  <w-date *ngSwitchCase=\"'date'\"  [attr]=\"attr\" [widgetValue]=\"widgetValue\" [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\"></w-date>\r\n  <w-textarea *ngSwitchCase=\"'textarea'\"  [attr]=\"attr\" [widgetValue]=\"widgetValue\" [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\"></w-textarea>\r\n  <w-number \r\n    *ngSwitchCase=\"'number'\"\r\n    [column]=\"attr\"\r\n    [widgetValue]=\"widgetValue\"\r\n    [mode]=\"mode\" \r\n    (widgetValueChange)=\"changeval($event)\">\r\n  </w-number>\r\n  <w-boolean *ngSwitchCase=\"'boolean'\"  [attr]=\"attr\" [widgetValue]=\"widgetValue\"  [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\"></w-boolean>\r\n  <w-point *ngSwitchCase=\"'point'\"  [attr]=\"attr\" [widgetValue]=\"widgetValue\"  [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\" [mapId]=\"'maps_front-page-map'\"></w-point>\r\n  <w-line *ngSwitchCase=\"'line'\"  [attr]=\"attr\" [widgetValue]=\"widgetValue\"  [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\" [mapId]=\"'maps_front-page-map'\"></w-line>\r\n  <w-polygon *ngSwitchCase=\"'polygon'\"  [attr]=\"attr\" [widgetValue]=\"widgetValue\"  [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\" [mapId]=\"'maps_front-page-map'\"></w-polygon>\r\n  <w-select *ngSwitchCase=\"'select'\"  [attr]=\"attr\" [widgetValue]=\"widgetValue\"  [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\"></w-select>\r\n  <w-classify *ngSwitchCase=\"'classify'\"  [attr]=\"attr\" [widgetValue]=\"widgetValue\"  [mode]=\"mode\" (widgetValueChange)=\"changeval($event)\"></w-classify>\r\n  <span *ngSwitchDefault>Default</span>\r\n</ng-container>\r\n"

/***/ }),

/***/ "./src/app/widgets/widget/widget.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WidgetComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WidgetComponent = /** @class */ (function () {
    function WidgetComponent() {
        this.attr = {};
        this.widgetValue = '';
        this.mode = {};
    }
    WidgetComponent.prototype.ngOnInit = function () { };
    WidgetComponent.prototype.getValue = function () {
        return this.widgetValue;
    };
    WidgetComponent.prototype.changeval = function (mas) {
        this.widgetValue = mas;
        /*
        this.valueChange.emit(this.widgetValue);*/
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WidgetComponent.prototype, "attr", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WidgetComponent.prototype, "widgetValue", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], WidgetComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Q" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */])
    ], WidgetComponent.prototype, "valueChange", void 0);
    WidgetComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'widget',
            template: __webpack_require__("./src/app/widgets/widget/widget.component.html"),
        })
    ], WidgetComponent);
    return WidgetComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    APIPath: 'http://84.237.16.39:3000/api'
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map